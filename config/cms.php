<?php

return array(
	//mode
	'need_chg'=>array(
		'Fan_Speed',
		'Operating_Mode',
		'Swing',
		'Sleep',
		'Error'
		),
	'Error'=>array(
		'',
		'Indoor Room Thermistor Error'
		,
		'Indoor Coil Thermistor Error / Pipe Water Thermistor Error'
		,
		'Outdoor Coil Thermistor Error'
		,
		'Compressor Overload / Pipe Water Temperature Poor'
		,
		'Outdoor Abnormal / Pipe Water Temperature Bad'
		,
		'Water Pump Fault'
		,
		'Outdoor Coil Thermistor Exist (MS model)'
		,
		'Hardware Error'
		,
		'Indoor Fan Feedback Error'
		,
		''
		,
		''
		,
		''
		,
		''
		,
		'EEPROM Error'
		,
		''
		,
		'Protection equipment operation (All)'
		,
		'Indoor PCB failure'
		,
		'Fan motor lock'
		,
		'Drain water level to high'
		,
		'Heat exchanger (1) temperature abnormal'
		,
		'Heat exchanger (2) temperature abnormal'
		,
		'Fan motor overloaded,overcurrent and locked'
		,
		'Louver motor locked'
		,
		'AC input overcurrent'
		,
		'Expansion valve abnormal'
		,
		'Heater overheat'
		,
		'Electrical air cleaner abnormal (dusty)'
		,
		'Overheat'
		,
		'Improper indoor unit power setting'
		,
		'Insufficient water supply'
		,
		'Humidity system abnormal'
		,
		'Sensor abnormal (All)'
		,
		''
		,
		''
		,
		'Drain water level sensor abnormal'
		,
		'Indoor heat exchanger sensor (1) abnormal'
		,
		'Indoor heat exchanger sensor (2) abnormal'
		,
		'Fan motor overload,overcurrent and lock'
		,
		'Louver motor sensor abnormal'
		,
		'AC input sensor abnormal'
		,
		'Indoor room sensor abnormal'
		,
		'Outdoor room sensor abnormal'
		,
		'Dust sensor abnormal'
		,
		'Humidity sensor abnormal'
		,
		'Temperature sensor abnormal (handset etc)'
		,
		'Temperature sensor abnormal (handset etc)'
		,
		'High pressure switch failure'
		,
		'Protection system operation (All)'
		,
		'Outdoor PCB failure'
		,
		''
		,
		'High pressure switch error (HPS)'
		,
		'Low pressure switch error (LPS)'
		,
		'Compressor motor overload'
		,
		'Compressor motor overcurrent & lock'
		,
		'Fan motor overload,overcurrent,lock'
		,
		'AC input overcurrent'
		,
		'Expansion valve abnormal'
		,
		'Mode switching abnormal/4 way valve change abnormal'
		,
		'Pump overcurrent & lock'
		,
		'Water temperature abnormal'
		,
		'Additional protection equipment abnormal'
		,
		'Drain water level abnormal'
		,
		'Extra heat source unit abnormal'
		,
		'Sensor abnormal (All)'
		,
		'Air temperature sensor abnormal'
		,
		'Power source sensor abnormal'
		,
		'High pressure switch failure'
		,
		'Low pressure switch failure'
		,
		'Compressor motor overload/sensor abnormal'
		,
		'Compressor motor overcurrent/locked/sensor abnormal'
		,
		'Fan motor overloaded/overcurrent/sensor abnormal'
		,
		'AC input current sensor abnormal'
		,
		'Outdoor temperature sensor abnormal'
		,
		'Discharge temperature sensor abnormal'
		,
		'Pump overcurrent/lock/sensor abnormal'
		,
		'Water temperature/hot water temperature sensor abnormal'
		,
		'Water system failure
		'
		,
		'Drain water level sensor abnormal'
		,
		'Extra heat source unit/controller warning/underchecking'
		,
		'No 1&2 system communication equipment operate'
		,
		'No 1. system communication equipment operate'
		,
		'No 2. system communication equipment operate'
		,
		'Compressor discharge temperature abnormal'
		,
		'Compressor suction temperature abnormal
		'
		,
		''
		,
		'Heat exchanger (1) temperature abnormal(high pressure control in cooling)'
		,
		''
		,
		''
		,
		''
		,
		'Discharge pressure abnormal'
		,
		'High temperature oil abnormal'
		,
		'Suction pressure abnormal'
		,
		'Exhaust temperature abnormal
		'
		,
		'Oil pressure abnormal'
		,
		'Oil level abnormal/refrigerant oil flow is light'
		,
		'Refrigerant temperature sensor abnormal'
		,
		'Pressure sensor system abnormal (All)'
		,
		'Current sensor system abnormal'
		,
		'Outdoor discharge pipe sensor abnormal'
		,
		'Low pressure equivalent saturated temperature sensor ab.'
		,
		'Suction tube temperature sensor abnormal'
		,
		'Heat exchanger temperature system abnormal (1)'
		,
		'Heat exchanger temperature system abnormal (2)'
		,
		'Liquid tube temperature system abnormal'
		,
		'Gas tube temperature system abnormal'
		,
		'Discharge pressure sensor system abnormal'
		,
		'Oil temperature sensor system abnormal'
		,
		'Suction pressure sensor system abnormal'
		,
		'Exhaust temperature sensor abnormal
		'
		,
		'Oil pressure sensor system abnormal'
		,
		'Oil level sensor system abnormal'
		,
		'Inverter system abnormal'
		,
		'Inverter PCB failure
		'
		,
		''
		,
		'Switch box (inside) temperature increase'
		,
		'Heatsink fin for transistor temperature increase'
		,
		'Inverter compressor overcurrent'
		,
		'AC output overcurrent (shortime)'
		,
		'Air conditioner total input overcurrent'
		,
		'Inverter compressor overcurrent'
		,
		'Inverter compressor startup failure'
		,
		'Power transistor abnormal'
		,
		'Converter generation abnormal
		'
		,
		'Communication between indoor & outdoor unit'
		,
		'Engine stop
		'
		,
		'Ignitor open
		'
		,
		'Engine cannot start
		'
		,
		'No gas for heat source'
		,
		'Short of one phase (in case 3 phase)'
		,
		''
		,
		'Switch box (inside) temperature sensor increase'
		,
		'Heat sink for power transistor temperature sensor system ab.'
		,
		'DC current sensor system abnormal'
		,
		'AC/DC output current sensor system abnormal'
		,
		'Air conditioner (long time)power input sensor system abnormal'
		,
		'Engine revolution failure
		'
		,
		'Additional equipment increase humidity fan motor abnormal
		'
		,
		'Additional equipment increase humidity heater abnormal
		'
		,
		'Additional equipment increase humidity sensor abnormal
		'
		,
		'Generated signal (when power on) abnormal
		'
		,
		'Capacity setting failure for outdoor unit'
		,
		'Fuel gas valve output abnormal
		'
		,
		'Starter (engine) misoperation
		'
		,
		'No gas'
		,
		'Opposite phase'
		,
		'Power line voltage system abnormal'
		,
		'Communication error (All)'
		,
		'Communication error between indoor & outdoor'
		,
		'Communication error between indoor & handset failure'
		,
		'Communication error between indoor unit'
		,
		'Communication error between outdoor unit'
		,
		'Communication error between handset'
		,
		'Communication error between system'
		,
		'Installation error'
		,
		'Refrigerant system error'
		,
		'Address setting failure'
		,
		'Additional equipment communication failure'
		,
		'Communication error between indoor unit (main)'
		,
		'Wrong connection'
		,
		),
	'Swing'=>array(
		'Remain as previous',
		'ON',
		'OFF',
		),
	'Sleep'=>array(
		'Remain as previous',
		'ON',
		'OFF',
	),
	'Fan_Speed'=>array(
		'',
		'Auto',
		'High',
		'Medium',
		'Low',
		),
	'Operating_Mode'=>array(
		'0'=>'Remain as previous',
		'1'=>'Fan',
		'2'=>'Cool',
		'3'=>'Heat',
		'4'=>'Auto',
		'5'=>'OFF',
		'6'=>'Dry',
		'7'=>'HTR 1',
		'8'=>'HTR 2',
		'9'=>'Turbo (Cool Mode)',
		'10'=>'Turbo (Heat Mode)',
		'11'=>'Quiet (Cool Mode)',
		'12'=>'Quiet (Heat Mode)',
		)
	);



































