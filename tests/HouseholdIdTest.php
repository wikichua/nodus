<?php

use App\Lib\HouseholdIdGenerator\HouseholdIdGenerator;

class HouseholdIdTest extends TestCase
{
    /**
     * Test with HouseholdIdGenerator
     */
    public function testHouseHoldIdGenerator()
    {
        $email = 'kevinkhew@convep.com';

        $HouseholdIdGenerator = new HouseholdIdGenerator();
        $household_id = $HouseholdIdGenerator->generate($email);

        if ($household_id === 59404) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
