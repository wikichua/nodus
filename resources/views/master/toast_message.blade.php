@if (Session::has('success-message') || count($errors) > 0||Session::has('warning-message') )
    <script type="text/javascript" src="{{ url('assets/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/pages/scripts/ui-bootstrap-growl.js') }}"></script>
    <script type="text/javascript">
        @if (Session::has('success-message'))
            Bs_growl_show('{{ session('success-message') }}', 'success');
        @endif
        @if (Session::has('warning-message'))
           Bs_growl_show('{{ session('warning-message') }}', 'warning');
        @endif
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                Bs_growl_show('{{ $error }}', 'danger');
            @endforeach
        @endif
    </script>
@endif
