<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>@yield('title', 'Nodus-CMS')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ url('assets/global/css/components-md.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/global/css/plugins-md.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/admin/layout4/css/layout.css') }}" rel="stylesheet" type="text/css" />
    <link id="style_color" href="{{ url('assets/admin/layout4/css/themes/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/admin/layout4/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- END THEME STYLES -->
    <link href="{{ url('style/style.css') }}" rel="stylesheet" type="text/css" />
    @yield('style')
    
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-md page-header-fixed page-sidebar-closed-hide-logo">
    <!-- BEGIN HEADER -->
    <div class="page-header md-shadow-z-1-i navbar navbar-fixed-top " >
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo col-md-12 " style="background-color:{{ isset(App\CustomerIdList::find(auth()->user()->app_id)->background_color)?App\CustomerIdList::find(auth()->user()->app_id)->background_color:'' }}">
                @if(App\CustomerIdList::find(auth()->user()->app_id)&&App\CustomerIdList::find(auth()->user()->app_id)->app_icon!=NULL)
                    <a href="{{ route('home') }}"class="col-md-10 text-center"  >
                    <img src="{{ asset('uploads/'.imgTagShow(App\CustomerIdList::find(auth()->user()->app_id)->app_icon)) }}" alt="logo" class="logo-default " style="max-width:180px;  max-height: 70px; padding-top: 10%; padding-right:10px" />
                    </a>
                 @else
                    <a href="{{ route('home') }}"class=""  >
                        <img src="{{ url('images/logo-big-black.png') }}" alt="logo" class="logo-default" />
                    </a>
                @endif
                 <div class="menu-toggler sidebar-toggler">

                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="separator hide">
                        </li>
                        <li class="separator hide">
                        </li>
                        <li class="separator hide">
                        </li>
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user dropdown-dark">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="username username-hide-on-mobile">
                                    {{ auth()->user()->name }}
                                </span>
                                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                <img alt="" class="img-circle" src="{{ asset('uploads/'.imgTagShow(Auth::user()->picture)) }}" />
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ route('home.profile') }}"><i class="icon-user"></i> My Profile </a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>

                                    @if(Session::has('original_user_id'))
                                         <a href="{{ route('home.switch',auth()->user()->id)}}"><i class="icon-key"></i> Switch Back</a>

                                    @else
                                        <a href="{{ route('auth.logout') }}"><i class="icon-key"></i> Log Out </a>
                                    @endif


                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        @include('master.sidebar_menu')
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
                @yield('page_title')
                @yield('breadcrumb')
                <!-- BEGIN PAGE CONTENT-->
                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>

        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="">
        <div class="container">
        
            <p class="text-muted">{{ Carbon\Carbon::now()->year }} &copy; Convep Mobilogy Sdn. Bhd</p>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="{{ url('assets/global/plugins/respond.min.js') }}"></script>
    <script src="{{ url('assets/global/plugins/excanvas.min.js') }}"></script>
    <![endif]-->
    <script src="{{ url('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/jquery.datetimepicker.full.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/Chart.bundle.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/admin/layout4/scripts/layout.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/admin/layout4/scripts/demo.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('script/number.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/select2.min.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            Demo.init(); // init demo features
            highlightAccessKeys();
            hookUpOnInput();

            var textarea = $('textarea.resizable-s');
            if (textarea.resizable) textarea.resizable({
                handles: 's'
            });

                
                $("#cms").focusin(function(){
                $('#cms1').attr("class","glyphicon glyphicon-folder-open");
                });
                $("#cms").focusout(function(){
                $('#cms1').attr("class","glyphicon glyphicon-folder-close");
                 });
            
        });
    </script>
    @yield('script')
    <!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->

</html>
