<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="start">
                <a href="{{ route('home',1) }}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="arrow "></span>
                </a>
                
                @can('admin',"Users")
                <ul class="sub-menu">
                    @foreach(App\CustomerIdList::all() as $customer)
                        <li>
                            <a href="{{ route('home.ga',$customer->id) }}">
                                <i class="icon-puzzle"></i>{{ $customer->name}}
                            </a>
                        </li>
                    @endforeach
                </ul>
                @endcan
                @can('manager',"Users")
                <ul class="sub-menu">
                    @foreach(App\CustomerIdList::whereIn('id', json_decode(auth()->user()->app_ids,true))->get() as $customer)
                        <li>
                            <a href="{{ route('home.ga',$customer->id) }}">
                                <i class="icon-puzzle"></i>{{ $customer->name}}
                            </a>
                        </li>
                    @endforeach
                </ul>
                @endcan
            </li>
            <li>
                <a href="#" id='cms'>
                    <i class="glyphicon glyphicon-folder-close " id='cms1'></i>
                    <span class="title cms">CMS</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('cms.inventory.index') }}">
                            <i class="icon-rocket"></i> Inventory
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('cms.feedbacks.index') }}">
                            <i class="fa fa-comment"></i> Feedback
                        </a>
                    </li>
                    @can('sud_customer',"Users")

                    @else
                    <li>
                        <a href="{{ route('users.index') }}">
                            <i class="icon-user"></i> System Users
                        </a>
                    </li>
                    @endcan
                    <li>
                        <a href="{{ route('cms.mobile_users.index') }}">

                             <i class="fa fa-user "></i> Mobile Users
                        </a>
                    </li>
                </ul>
            </li>
            @can('admin',"Users")
                <li>
                    <a href="#">
                        <i class="icon-settings"></i>
                        <span class="title">Settings</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('cms.customer_id_lists.index') }}">
                                <i class="icon-puzzle"></i> Licensee
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cms.ieee_lists.index') }}">
                                <i class="icon-puzzle"></i> IEEE
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cms.customer_sub_id_lists.index') }}">
                                <i class="icon-puzzle"></i> Batch
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cms.device_type_lists.index') }}">
                                <i class="icon-puzzle"></i> Device Type
                            </a>



                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('app.version') }}">
                            <i class="icon-puzzle"></i> App Version
                    </a>
                </li>
                <li>
                    <a href="{{ route('app.firmware') }}">
                            <i class="fa fa-building-o "></i> Firmware
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-puzzle"></i>
                        <span class="title">Number Convertor</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <table class="horizontal">
                            <tr>
                                <th scope="row" style="text-align: right">
                                    <label for="base10">&nbsp;&nbsp;Decimal <sub>10</sub>&nbsp;</label>
                                </th>
                                <td>
                                    <input type="text" name="base10" id="base10" accesskey="D" size="10" oninput="numberOnInput(this)">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="text-align: right">
                                    <label for="base16">&nbsp;&nbsp;Hex <sub>16</sub>&nbsp;</label>
                                </th>
                                <td>
                                    <input type="text" name="base16" id="base16" accesskey="H" size="10" oninput="numberOnInput(this)">
                                </td>
                            </tr>
                            <!-- hide this Octal -->
                            <tr style="display: none;">
                                <th scope="row" style="text-align: right">
                                    <label for="base8">&nbsp;&nbsp;Octal <sub>8</sub>&nbsp;</label>
                                </th>
                                <td>
                                    <input type="text" name="base8" id="base8" accesskey="O" size="10" oninput="numberOnInput(this)">
                                </td>
                            </tr>
                            <tr>
                                <th scope="row" style="text-align: right">
                                    <label for="base2">&nbsp;&nbsp;Binary <sub>2</sub>&nbsp;</label>
                                </th>
                                <td>
                                    <input type="text" name="base2" id="base2" accesskey="B" size="16" oninput="numberOnInput(this)">
                                </td>
                            </tr>
                        </table>
                    </ul>
                </li>
            @endcan
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
