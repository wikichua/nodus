@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('page_title')
<div class="page-head">
<div class="col-md-12">
    <div class="page-title" style="width: 100%;">
        <h1 style="display: inline-block;">Dashboard</h1>
        <div class="pull-right well well-sm">

            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Charts <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                <li><a href="{{ route('home.ga',$CustomerIdLists_id) }}">Chart1</a></li>
                <li><a href="{{  route('home.chartjs',$CustomerIdLists_id) }}">Chart1</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<br />
@endsection

@section('content')


<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Chart
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <canvas id="canvas" height="280" width="600"></canvas>
    </div>
</div>

@endsection

@section('script')
<script src="{{ url('assets/Chart.bundle.js') }}" type="text/javascript"></script>
<script>


 var year = <?php echo $GA_CHART_START_DATE; ?>;
    var power = <?php echo $power; ?>;
    var data_viewer = <?php echo $DeviceAuditTrail; ?>;

    var barChartData = {
        labels: year,
        datasets: [{
            label: 'Total Power(kilowatts)',
            backgroundColor: "rgba(220,220,220,0.5)",
            data: power
        }, 
        {
            label: 'average temperature',
            backgroundColor: "rgba(20,20,205,0.5)",
            data: data_viewer
        }
        ]
    };

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            
            options: {
                tooltips:{
                    
                },
                scales: {

                xAxes: [{
                    type: 'time',
                    time: {
                        min:year[0],
                        unit: 'month'
                    }
            }]
        },
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: 'rgb(0, 255, 0)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly Power Usage'
                }
            }
        });

    };
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection



