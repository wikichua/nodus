@extends('master.master')

@section('page_title')
<div class="page-head">
<div class="col-md-12">
    <div class="page-title" style="width: 100%;">
        <h1 style="display: inline-block;">Dashboard</h1>
        <div class="pull-right well well-sm">
            {!! Form::open(array(route('home.ga',$CustomerIdLists->first()->id) ,'method'=>'get','name'=>'theform','files'=>true,'class'=>'form-inline','style'=>'display:inline-block;')) !!}
            <div class="form-group">
                {!! Form::label('ga_from','From') !!}
                {!! Form::text('ga_from',$GA_CHART_START_DATE,array('class'=>'form-control datepicker')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('ga_to','To') !!}
                {!! Form::text('ga_to',$GA_CHART_END_DATE,array('class'=>'form-control datepicker')) !!}
            </div>
            <button type="submit" class="btn btn-default"><i class="fa fa-search-plus"></i></button>
            {!! Form::close() !!}
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Charts <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                <li><a href="#">Chart1</a></li>
                {{-- <li><a href="{{  route('home.chartjs',$CustomerIdLists->first()->id) }}">Chart2</a></li> --}}
                </ul>
            </div>
        </div>
    </div>
</div>
<br />
@endsection

@section('content')

    @foreach ($CustomerIdLists as $key => $CustomerIdList)
        @if($CustomerIdList->ga_ids != '')
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>{{ $CustomerIdList->name }}</h3>
                        <div class="panel panel-default panel-ga panel-ga-{{ $key }}-1">
                            <div class="panel-heading">
                                <i class="fa fa-user"> | </i> Users
                            </div>
                            <div class="panel-body">
                                <div id="chart-{{ $key }}-1-container" class="ga1"></div>
                            </div>
                            <div class="chart-divbtm"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6">
                        @if (count($CustomerIdLists) > 1)
                            {{ $CustomerIdLists->name }}
                        @endif
                        <div class="panel panel-default panel-ga panel-ga-{{ $key }}-3">
                            <div class="panel-heading">
                                <i class="fa fa-tachometer "> | </i> Malaysia Usage
                            </div>
                            <div class="panel-body">
                                <div id="chart-{{ $key }}-3-container"></div>
                            </div>
                            <div class="chart-divbtm"></div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        @if (count($CustomerIdLists) > 1)
                            &nbsp;
                        @endif
                        <div class="panel panel-default panel-ga panel-ga-{{ $key }}-4">
                            <div class="panel-heading">
                                <i class="fa fa-globe "> | </i> Global Usage 
                            </div>
                            <div class="panel-body">
                                <div id="chart-{{ $key }}-4-container"></div>
                            </div>
                            <div class="chart-divbtm"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6">
                        @if (count($CustomerIdLists) > 1)
                            {{ $CustomerIdLists->name }}
                        @endif
                        <div class="panel panel-default panel-ga panel-ga-{{ $key }}-2">
                            <div class="panel-heading">
                                <i class="fa fa-tachometer "> | </i> Screen Views
                            </div>
                            <div class="panel-body">
                                <div id="chart-{{ $key }}-2-container"></div>
                            </div>
                            <div class="chart-divbtm"></div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        @if (count($CustomerIdLists) > 1)
                            &nbsp;
                        @endif
                        <div class="panel panel-default panel-ga panel-ga-{{ $key }}-5">
                            <div class="panel-heading">
                                <i class="fa fa-globe "> | </i> User Device Operating System
                            </div>
                            <div class="panel-body">
                                <div id="chart-{{ $key }}-5-container"></div>
                            </div>
                            <div class="chart-divbtm"></div>
                        </div>
                    </div>

                </div>
            </div>

            
            



        @endif
    @endforeach
@endsection

@section('script')

    <script type = "text/javascript" >
   
    if($('#ga_to').val()=='')
            var GA_CHART_END_DATE ='yesterday';
        else{
            var GA_CHART_END_DATE =$('#ga_to').val();
        }

        if($('#ga_from').val()=='')
            var GA_CHART_START_DATE ='30daysAgo';
        else{
            var GA_CHART_START_DATE =$('#ga_from').val();
        }
        $(document).ready(function(){
        $('#ga_from').change(function(){
        });
    });

        (function (w, d, s, g, js, fs) {
            g = w.gapi || (w.gapi = {});
            g.analytics = {
                q: [],
                ready: function(f) {
                    this.q.push(f);
                }
            };
            js = d.createElement(s);
            fs = d.getElementsByTagName(s)[0];
            js.src = 'https://apis.google.com/js/platform.js';
            fs.parentNode.insertBefore(js, fs);
            js.onload = function() {
                g.load('analytics');
            };
        }(window, document, 'script'));

        gapi.analytics.ready(function() {

            gapi.analytics.auth.authorize({
                serverAuth: {
                    access_token: '{{ $gapi_access_token or 0 }}'
                }
            });

            @foreach ($CustomerIdLists as $key => $CustomerIdList)
                @if($CustomerIdList->ga_ids != '')
                    var IDS{{ $key }} = '{{ $CustomerIdList->ga_ids }}';

                    // Handle the Google Analytics Chart Date Range
                
                    var dataChart_{{ $key }}_1 = new gapi.analytics.googleCharts.DataChart({
                        query: {
                            'ids': IDS{{ $key }},
                            'start-date': GA_CHART_START_DATE,
                            'end-date': GA_CHART_END_DATE,
                            'metrics': 'ga:users,ga:newUsers',
                            'dimensions': 'ga:date',
                        },
                        chart: {
                            'container': 'chart-{{ $key }}-1-container',
                            'type': 'LINE',
                            'options': {
                                'width': '100%',
                                'height': '250',
                            }
                        }
                    }).execute();


                    var dataChart_{{ $key }}_2 = new gapi.analytics.googleCharts.DataChart({
                        query: {
                            'ids': IDS{{ $key }},
                            'start-date': GA_CHART_START_DATE,
                            'end-date': GA_CHART_END_DATE,
                            'metrics': 'ga:screenViews',
                            'dimensions': 'ga:screenName',
                            'sort': '-ga:screenViews',
                            'filters': 'ga:screenName!=Home',
                            'max-results': 7,
                        },
                        chart: {
                            'container': 'chart-{{ $key }}-2-container',
                            'type': 'PIE',
                            'options': {
                                'width': '100%',
                                'height': '250',
                                'legend': {
                                    'position': 'labeled'
                                },
                                'pieSliceText': 'none',
                                'sliceVisibilityThreshold': 0,
                                'pieHole': 0.25
                            }
                        }
                    }).execute();

                    var dataChart_{{ $key }}_3 = new gapi.analytics.googleCharts.DataChart({
                        query: {
                            'ids': IDS{{ $key }},
                            'start-date': GA_CHART_START_DATE,
                            'end-date': GA_CHART_END_DATE,
                            'metrics': 'ga:Sessions',
                            'dimensions': 'ga:city',
                            'sort': '-ga:Sessions',
                            'filters': 'ga:country==Malaysia',
                            'max-results': 8,
                        },
                        chart: {
                            'container': 'chart-{{ $key }}-3-container',
                            'type': 'COLUMN',
                            'options': {
                                'width': '100%',
                                'height': '100%',
                            }
                        }
                    }).execute();

                    var dataChart_{{ $key }}_4 = new gapi.analytics.googleCharts.DataChart({
                        query: {
                            'ids': IDS{{ $key }},
                            'start-date': GA_CHART_START_DATE,
                            'end-date': GA_CHART_END_DATE,
                            'metrics': 'ga:Sessions',
                            'dimensions': 'ga:country',
                            'filters': 'ga:country!=Malaysia',
                            'max-results': 8,
                        },
                        chart: {
                            'container': 'chart-{{ $key }}-4-container',
                            'type': 'COLUMN',
                            'options': {
                                'width': '100%',
                                'height': '100%',
                                'legend': {
                                    'position': 'labeled'
                                },
                                'pieSliceText': 'none',
                                'sliceVisibilityThreshold': 0,
                            }

                        }
                    }).execute();

                    var dataChart_{{ $key }}_5 = new gapi.analytics.googleCharts.DataChart({
                        query: {
                            'ids': IDS{{ $key }},
                            'start-date': GA_CHART_START_DATE,
                            'end-date': GA_CHART_END_DATE,
                            'metrics': 'ga:Sessions',
                            'dimensions': 'ga:operatingSystem',
                            'sort': '-ga:Sessions',
                            'filters': 'ga:operatingSystem!=-10',
                            'max-results': 8                        },
                        chart: {
                            'container': 'chart-{{ $key }}-5-container',
                            'type': 'PIE',
                            'options': {
                                'width': '100%',
                                'height': '100%',
                            }
                        }
                    }).execute();

                    


                    
                @endif
            @endforeach
        });







    </script>
@include('master.toast_message')

@endsection
