@extends('master.master')
@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('breadcrumb')


<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#">Profile</a>
    </li>
</ul>
@stop
@section('content')
<div class="row">
	<div class="col-lg-3 col-md-4 col-sm-4">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar col-lg-16 col-md-16 col-sm-16" >
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="{{ asset('uploads/'.imgTagShow(Auth::user()->picture)) }}" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<strong>{{ auth()->user()->name }}</strong>
					</div>
					<div class="">
						{{ auth()->user()->email }}
					</div>
					
					
				
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				{{-- <div class="profile-userbuttons">
					<button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>
					<button type="button" class="btn btn-circle btn-danger btn-sm">Message</button>
				</div> --}}
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				{{-- <div class="profile-usermenu">
					<ul class="nav">
						<li>
							<a href="#">
							<i class="icon-home"></i>
							Overview </a>
						</li>
						<li class="active">
							<a href="#">
							<i class="icon-settings"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="icon-check"></i>
							Tasks </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-info"></i>
							Help </a>
						</li>
					</ul>
				</div> --}}
				<!-- END MENU -->
			</div>
		</div>
			<!-- END PORTLET MAIN -->
	</div>


<div class="profile-content">
	<div class="row">
		<div class="col-lg-8 col-md-7 col-sm-7">
			<div class="portlet light">
				<div class="portlet-title tabbable-line">
					<div class="caption caption-md">
						<i class="icon-globe theme-font hide"></i>
						<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
					</div>
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
						</li>
						<li>
							<a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
						</li>
						<li>
							<a href="#tab_1_3" data-toggle="tab">Change Password</a>
						</li>
						{{-- <li>
							<a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
						</li> --}}
					</ul>
				</div>
				<div class="portlet-body">
					<div class="tab-content">
						<!-- PERSONAL INFO TAB -->
						<div class="tab-pane active" id="tab_1_1">
							{!! Form::open(array('route'=>array('home.update'),'method'=>'put','name'=>'theform','files'=>true,'class'=>'')) !!}
								<div class="">
									{!! Form::label('name', 'Name',array('class'=>'control-label')) !!}
									<br>
									<div class="">
										{!! Form::text('name', auth()->user()->name ,array('class'=>'form-control')) !!}
									</div>
								</div>
								<br>

								<div class="">
										{!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
									<br>
									<div class="">
	                            		<input type="text" name="email" readonly value= " {{  auth()->user()->email }} "  class="form-control" id="form_control_1" placeholder="Enter serial number">
	                 				</div>
								</div>
								<br>

								<!-- <div class="">
									{!! Form::label('gender', 'Gender',array('class'=>'control-label')) !!}
									<br>
									<div class="">
	                            		{!! Form::select('gender',array('female'=>'Female','male'=>'Male'),auth()->user()->gender, ['class' => 'form-control']) !!}
	                 				</div>
								</div>
								<br>

								<div class="">
									{!! Form::label('dob', 'Day of birth',array('class'=>'control-label')) !!}
									<br>
									<div class="">
	                            		{!! Form::text('dob', auth()->user()->dob ,array('class'=>'form-control')) !!}
	                 				</div>
								</div> -->
								<br>

								<div class="">
									<button type="submit" class="btn btn-info ">Save</button>
								</div>
							{!! Form::close() !!}
						</div>
						<!-- END PERSONAL INFO TAB -->
						<!-- CHANGE AVATAR TAB -->
						<div class="tab-pane" id="tab_1_2">
							{!! Form::open(array('route'=>array('home.updateimage'),'method'=>'put','name'=>'theform','files'=>true,'class'=>'')) !!}
								<div class="">
									{!! Form::label('picture','Photo',array('class'=>'col-sm-2 control-label')) !!}
									<div class = "">
										<div class = "fileinput-new thumbnail" style="width: 200px; height: 210px;">
											<img id = "previewHere" src="{{ asset('uploads/'.imgTagShow(Auth::user()->picture)) }}" class="thumbnail col-sm-3 " style="width: 190px; height: 180px;" >
										</div>
										{!! Form::file('picture',array('class'=>'form-control')) !!}
									</div>
								</div>
								<br>
								<div class="">
									<button type="submit" class="btn btn-info ">Save</button>
								</div>


							{!! Form::close() !!}


						</div>
						<!-- END CHANGE AVATAR TAB -->
						<!-- CHANGE PASSWORD TAB -->
						<div class="tab-pane" id="tab_1_3">
							{!! Form::open(array('route'=>array('home.updatepassword'),'method'=>'put','name'=>'theform','files'=>true,'class'=>'form-title')) !!}
									{!! Form::hidden('name', auth()->user()->name ,array('class'=>'form-control')) !!}
									{!! Form::hidden('email', auth()->user()->email ,array('class'=>'form-control')) !!}
								<div class="">
									{!! Form::label('old_password', 'Current Password',array('class'=>'control-label')) !!}
								<br>
								<div class="">
									{!! Form::password('old_password',array('class'=>'form-control')) !!}
									</div>
								</div>
								<br>
								<div class="">
									{!! Form::label('password', 'New Password',array('class'=>'control-label')) !!}
								<br>
								<div class="">
									{!! Form::password('password',array('class'=>'form-control')) !!}
									</div>
								</div>
								<br>
								<div class="">
									{!! Form::label('password_confirmation', 'Re-type New Password',array('class'=>'control-label ')) !!}
								<br>
								<div class="">
									{!! Form::password('password_confirmation',array('class'=>' form-control placeholder-no-fix')) !!}
									</div>
								</div>
								<br>
								<div class="">
									<button type="submit" class="btn btn-info ">Save</button>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@stop

@section('script')

<script type="text/javascript">
	$(document).ready(function(){
	$("#picture").change(function(){
	    previewImg(this,'previewHere');
	    var fname=$('#picture').val();
	    var filepath=fname.split('.');
		previewImg(this,'previewHere');

	});
});
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
 <script src="{{ asset('assets/script.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
    });
</script>


@include('master.toast_message')

@stop
