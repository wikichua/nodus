<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>@yield('title', 'Nodus-CMS')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ url('assets/global/css/components-md.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/global/css/plugins-md.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/admin/layout4/css/layout.css') }}" rel="stylesheet" type="text/css" />
    <link id="style_color" href="{{ url('assets/admin/layout4/css/themes/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/admin/layout4/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- END THEME STYLES -->
    <link href="{{ url('style/style.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
    
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body >

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(array(route('chartjs',$CustomerIdLists_id) ,'method'=>'get','name'=>'theform','files'=>true,'class'=>'form-inline')) !!}
                <div class="form-group col-sm-5 pull-left" style="display: inline-block;">
                    {!! Form::label('form_date','From') !!}
                    {!! Form::date('form_date',Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $form_date),array('class'=>'form-control ')) !!}
                </div>
                <div class="form-group col-sm-5 pull-left " style="display: inline-block;">
                    {!! Form::label('to_date','To Date') !!}
                    {!! Form::select('to_date',array(1=>"Hours Of Day",365=>"Days Of Month"),$to_date,array('class'=>'form-control ')) !!}
                </div>
                <div class="form-group col-sm-5 pull-left " style="display: inline-block;">
                    {!! Form::label('chart','Chart') !!}
                    {!! Form::select('chart',array(1=>"Power Consumption",2=>"Power Consumption Accumulation",3=>" Temperature"),$chart,array('class'=>'form-control ')) !!}
                </div>
                <button type="submit" class="btn btn-default btn-md pull-left" style="margin-top: 25px"><i class="fa fa-search-plus"></i></button>
        {!! Form::close() !!}
        </div>
    </div>

    <div class="row container " >
        <div class="col-md-12 pre-scrollable" >
            <canvas id="canvas" height="300px" width="600px"></canvas>
            
        </div>
    </div>
                <!-- END PAGE CONTENT-->
           
    <!-- END FOOTER -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="{{ url('assets/global/plugins/respond.min.js') }}"></script>
    <script src="{{ url('assets/global/plugins/excanvas.min.js') }}"></script>
    <![endif]-->
    <script src="{{ url('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/jquery.datetimepicker.full.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/Chart.bundle.js') }}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/admin/layout4/scripts/layout.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/admin/layout4/scripts/demo.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('script/number.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/select2.min.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            Demo.init(); // init demo features
            highlightAccessKeys();
            hookUpOnInput();

            var textarea = $('textarea.resizable-s');
            if (textarea.resizable) textarea.resizable({
                handles: 's'
            });

                
                $("#cms").focusin(function(){
                $('#cms1').attr("class","glyphicon glyphicon-folder-open");
                });
                $("#cms").focusout(function(){
                $('#cms1').attr("class","glyphicon glyphicon-folder-close");
                 });
            
        });
    </script>
    <script src="{{ url('assets/Chart.bundle.js') }}" type="text/javascript"></script>
    <script>


        var unit =  "<?php echo $unit; ?>";
        var time =  <?php echo $time; ?>;
        var Power = <?php echo $Power; ?>;

        var barChartData = {
            labels:time,
             datasets: [

            {
                label: 'Power(Kilowatts)',
                backgroundColor: "rgba(151,187,205,0.5)",
                data: Power
            }]
        };


        window.onload = function() {

            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'line',
                data: barChartData,
                options: {
                    responsive: false,
                    tooltips: {
                        // callbacks:{
                        //     label: function(tooltipItem, data) {
                                
                        //         return  data.datasets[tooltipItem.datasetIndex].label+" "+JSON.stringify((data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]["z"]) ||data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]["y"]) ;

                        //         // return  data.datasets[tooltipItem.datasetIndex].label+" "+JSON.stringify(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]["z"] ||data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]["y"]) ;
                               
                        //     }
                        // }
                    },

                    scales: {
                        yAxes: [{
                    ticks: {
                        min: 0
                    }
                }],
                    xAxes: [{
                        // type: 'time',
                        // time: {
                        //     max:time[1],
                        //     unit:unit,
                        // }
                }]
            },
                    title: {
                        display: true,
                        text: 'Device Power Consumption 2'
                    }
                }
            });

           

        };
        

    </script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#date').change(function(){
                
                $('#form_date').val( $('#date').val());
            });
           

        });
    </script>
    @include('master.toast_message')


</body>
<!-- END BODY -->

</html>
