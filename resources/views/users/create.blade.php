@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
     <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('users.index') }}">Users</a>
         <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#">Create</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="icon-user font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Add user</span>
				</div>
			</div>
			<div class="portlet-body">
    			{!! Form::open(['url' => route('users.store'), 'method' => 'POST', 'role' => 'form', 'class' => 'form-horizontal margin-bottom-40']) !!}
                <div class="form-group form-md-line-input">
                    <label for="name" class="col-md-3 control-label">Name</label>
                    <div class="col-md-6">
                        {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>

                <div class="form-group form-md-line-input">
                    <label for="email" class="col-md-3 control-label">Email</label>
                    <div class="col-md-6">
                        {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>


                <div class="form-group form-md-line-input">
                    <label for="password" class="col-md-3 control-label">Password</label>
                    <div class="col-md-6">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>

                <div class="form-group form-md-line-input">
                    <label for="password" class="col-md-3 control-label">Re-type New Password</label>
                    <div class="col-md-6">
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>

                @can('admin','user')
                <div class="form-group form-md-line-input">
                    {!! Form::label('Role', 'Role', array('class'=>'col-md-3 control-label')) !!}
                    <div class="col-md-6">
                        {!! Form::select('Role', array('manager'=>'Manager','customer'=>'Licensee','admin'=>'Admin'), old('group'), ['class' => 'form-control']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>

                
                 <div class="form-group form-md-line-input app">
                    <label for="app" class="col-md-3 control-label">Licensee</label>
                    <div class="col-md-6">
                        {!! Form::select('App_id',$app_id,'', ['class' => 'form-control',  'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>

                 <div class="form-group form-md-line-input apps">
                    <label for="app" class="col-md-3 control-label">Licensee</label>
                    <div class="col-md-6">
                        {!! Form::select('app_ids[]',$app_id,'', ['class' => 'form-control', 'multiple'=>'multiple']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                @else
                {!! Form::hidden('parent_id', Auth::user()->id, ['class' => 'form-control']) !!}
                @endcan
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ route('users.index') }}" type="button" class="btn blue">Back</a>
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
		</div>
	</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
                    $('.app').hide();
            
            $('#Role').change(function(){
                $('.app').show();
                if($(this).val() == 'admin')
                {
                    $('#App_id').val('');
                     $('#app_ids').val('');
                    $('.app').hide();
                    $('.apps').hide();
                }
                if($(this).val() == 'customer')
                {
                    $('#app_ids').val('');
                    $('.apps').hide();
                }
                if($(this).val() == 'manager')
                {
                    $('#App_id').val('');
                    $('.apps').show();
                    $('.app').hide();
                }
            });
            $("select[multiple=multiple]").select2();
        });
    </script>
     @include('master.toast_message');
@endsection
