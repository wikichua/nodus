@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#">User</a>
    </li>
</ul>
@endsection

@section('content')

<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user"></i>User
        </div>
        <div class="actions">
            @can('admin',$Users) 
                <a href="{{ route('users.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add </a>
            @endcan 
        </div>
    </div>
 
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Name</th>
                    <th>Email</th>
                    
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Users as $key => $user)

                <tr class="odd gradeX">
                    <td class="text-right clickable-row" data-href="{{ route('users.show', $user->id)}}" style="width: 35px">{{ $key + 1 }}</td>
                    <td >{{ $user->name }}</td>
                    <td >{{ $user->email }}</td>
                    <td class="text-center" style="width: 180px">

                        <a href="{{ route('users.show', $user->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Show">
                            <i class="fa fa-list"></i>
                        </a>

                        <a href="{{ route('users.edit', $user->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Edit">
                            <i class="fa fa-pencil"></i>
                        </a>

                        @can('su',$user)
                        @if(Session::has('original_user_id'))
                                 <a href="{{ route('home.switch', $user->id) }}" disabled class="tooltips btn btn-icon-only default " data-container="body" data-placement="top" data-html="true" data-original-title="Switch" ><span style="font-size: 8px" class="glyphicon glyphicon-arrow-right"><span class="glyphicon glyphicon-user"  style="font-size: 9px"></span></span></a>
                                 
                        @else
                            @can('manager',$user)
                            @else
                                 <button data-href="{{ route('home.switch', $user->id) }}" class="tooltips btn btn-icon-only default confirm-switchUser-dialog" disable="true" data-toggle="modal" data-container="body" data-placement="top" data-html="true" data-original-title="Switch" ><span style="font-size: 8px" class="glyphicon glyphicon-arrow-right"><span class="glyphicon glyphicon-user"  style="font-size: 9px"></span></span></button>
                            @endcan                    

                        @endif
                        @endcan
                        @can('manager',$user)
                        @else
                            <button data-href="{{ route('users.destroy', $user->id) }}" data-token="{{ csrf_token() }}" class="confirm-delete-dialog tooltips btn btn-icon-only default" data-toggle="modal" data-container="body" data-placement="top" data-html="true" data-original-title="Delete">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        @endcan                    
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>    

<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
 <script type="text/javascript" src="{{ url('assets/admin/pages/scripts/ui-bootstrap-growl.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection