@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('users.index') }}">User</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('users.edit') }}">Edit</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Update User</span>
                </div>
            </div>
            <div class="portlet-body">
                {!! Form::open(['url' => route('users.update',$Users->id), 'method' => 'PUT', 'role' => 'form', 'class' => 'form-horizontal margin-bottom-40']) !!}
                    <div class="form-group form-md-line-input">
                        {!! Form::label('name', 'Name', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', $Users->name, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        {!! Form::label('email', 'Email', ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-6">
                            <input type="text" name="email" readonly value= " {{  $Users->email }} "  class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="{{ route('users.index') }}" type="button" class="btn blue">Back</a>
                            <button type="submit" class="btn blue">Submit</button>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    @include('master.toast_message')
@endsection
