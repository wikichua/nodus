@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('users.index') }}">Users</a>
         <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#">Show</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="icon-user font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Show user</span>
				</div>
			</div>
			<div class="portlet-body">
                <div class="form-horizontal margin-bottom-40">
                    <div class="form-group form-md-line-input">

                        <label for="name" class="col-md-3 control-label">Profile Picture</label>
                        <div class="col-md-6">
                            <div class = " fileinput-new thumbnail" style="width: 200px; height: 210px;">
                                <img id = "previewHere" src="{{ asset('uploads/'.imgTagShow($Users->picture)) }}" class="thumbnail col-sm-3 " style="width: 190px; height: 180px;" >
                            </div>

                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-horizontal margin-bottom-40">
                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" name="name" readonly value=" {{  $Users->name }} " class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label for="email" class="col-md-3 control-label">Email</label>
                            <div class="col-md-6">
                                <input type="text" name="email" readonly value= " {{  $Users->email }} "  class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label for="serial_number" class="col-md-3 control-label">App ID</label>
                            <div class="col-md-6">
                                <input type="text" name="App" readonly value= " {{  isset($CustomerIdList->name) ? $CustomerIdList->name : '' }} "  class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="{{ route('users.index') }}" type="button" class="btn blue">Back</a>
                            </div>
                        </div>
                    </div>
		  	    </div>
		    </div>
	   </div>
    </div>
</div>
@endsection
