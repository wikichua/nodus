@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#">app Version</a>
    </li>
</ul>
@endsection

@section('content')

<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user"></i>User
        </div>
        <div class="actions">
            @can('admin',auth()->user()) 
                <a href="{{ route('app.firmware.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add </a>
            @endcan 
        </div>
    </div>
 
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
				<thead>
					<tr>
						<th>Licensee</th>
						<th>Version</th>
						<th>Description</th>
						<th class="text-center"><i class="fa fa-cogs"></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($Firmwares as $Firmware)
					<tr>
						<td>{{ ($Firmware->app_id==0)?'':$app_ids[$Firmware->app_id] }}</td>
						<td>{{ $Firmware->version }}</td>
						<td>{{ str_limit($Firmware->description,20) }}</td>
						<td class="table-btn-action text-center">

							@can('admin', auth()->user())
				            	<a href="{{ route('app.firmware.edit',$Firmware->id) }}" class="btn btn-info btn-sm"data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>

				            	<a href="{{ route('app.firmware.destroy',$Firmware->id) }}" class="btn btn-info btn-sm"data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>


				            @endcan


						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
    </div>
</div>    

<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
 <script type="text/javascript" src="{{ url('assets/admin/pages/scripts/ui-bootstrap-growl.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection



