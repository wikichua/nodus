@extends('master.master')

@section('page-header')
	App Versioning
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Add Inventory</span>
				</div>
			</div>
			<div class="portlet-body">
		        {!! Form::open(array('route' => 'app.firmware.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
				<div class="form-group">
					{!! Form::label('app_id', 'Licensee', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::select('app_id',$app_ids , old('app_id'), array('class'=>"form-control",'placeholder'=>"Select One")) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('version', 'Version', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('version', old('version'), array('class'=>"form-control",'placeholder'=>"e.g. 1.0.1 or X.x.x")) !!}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('file', 'File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::file('file', array('class'=>"form-control")) !!}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::textarea('description', old('description'), array('class'=>"form-control",'placeholder'=>"e.g. Critical update", 'rows'=>3)) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						<a href="{{ route('app.firmware') }}" class='btn btn-back'>Back</a>
						{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
					</div>
				</div>
				{!! Form::close() !!}
		    </div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
$(function(){

});
</script>
@stop