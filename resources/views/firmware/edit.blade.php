@extends('master.master')

@section('page-header')
	App Versioning
@stop

@section('content')


<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Firmware</span>
				</div>
			</div>
			<div class="portlet-body">
		        {!! Form::open(array('route' => array('app.firmware.update',$Firmware->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}

		        <h3 class=" col-sm-12 text-center" >{{ ($Firmware->app_id==0)?'':$app_ids[$Firmware->app_id] }}</h3>

				<div class="form-group">
					{!! Form::label('version', 'Version', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::text('version', $Firmware->version, array('class'=>"form-control",'placeholder'=>"e.g. 1.0.1 or X.x.x")) !!}
					</div>
				</div>
				<a class=" col-sm-12 text-center" href={{ URL::to('uploads/'.$Firmware->file) }}><h4>{{ $Firmware->file }}</h4></a>

				<div class="form-group">
					{!! Form::label('file', 'File', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::file('file', array('class'=>"form-control")) !!}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label')) !!}
					<div class="col-sm-7">
						{!! Form::textarea('description', $Firmware->description, array('class'=>"form-control",'placeholder'=>"e.g. Critical update",'rows'=>3)) !!}
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7">
						<a href="{{ route('app.firmware') }}" class='btn btn-back'>Back</a>
						{!! Form::submit('Save', array('class'=>"btn btn-submit")) !!}
					</div>
				</div>
				{!! Form::close() !!}
		    </div>
		</div>
	</div>
</div>
@stop

@section('scripts')
@include('master.toast_message')

<script>

$(function(){

});

</script>
@stop