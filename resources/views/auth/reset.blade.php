@extends('master.auth')

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ url('images/logo-big-white.png') }}" style="height: 17px;" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<div class="content">
    <!-- BEGIN PASSWORD RESET FORM -->
    {!! Form::open(['url' => route('password.reset_email'), 'method' => 'POST']) !!}
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="form-title">
			<span class="form-title">Password reset</span>
			<span class="form-subtitle">Enter your new password to reset it.</span>
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<button class="close" data-close="alert"></button>
        		@foreach ($errors->all() as $error)
					<span>{{ $error }} </span>
        		@endforeach
			</div>
        @endif
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email"/>
		</div>
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control placeholder-no-fix" type="password" id="register_password" placeholder="1Password" name="password"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
			<input class="form-control placeholder-no-fix" type="password" placeholder="Re-type Your Password" name="new_password"/>
		</div>
		<div class="form-actions">
            <a href="{{ route('auth.login') }}" class="btn btn-default">Back</a>
			<button type="submit" class="btn btn-primary uppercase pull-right">Submit</button>
		</div>
    {!! Form::close() !!}
	<!-- END PASSWORD RESET FORM -->
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@endsection
