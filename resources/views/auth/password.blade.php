@extends('master.auth')

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ url('images/logo-big-white.png') }}" style="height: 17px;" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<div class="content">
    <!-- BEGIN FORGOT PASSWORD FORM -->
    {!! Form::open(['url' => route('password.post_email'), 'method' => 'POST']) !!}
		<div class="form-title">
			<span class="form-title">Forget Password ?</span>
			<span class="form-subtitle">Enter your e-mail to reset it.</span>
		</div>
		@if (Session::has('status'))
			<div class="alert alert-success">
				<span>{{ session('status') }} </span>
			</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<button class="close" data-close="alert"></button>
        		@foreach ($errors->all() as $error)
					<span>{{ $error }} </span>
        		@endforeach
			</div>
        @endif
        <div class="form-group ">
                        {!! Form::select('App_id',$app_id,'', ['class' => 'form-control',  'placeholder' => 'Licensee',]) !!}

                </div>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email"/>
		</div>
		<div class="form-actions">
            <a href="{{ route('auth.login') }}" class="btn btn-default">Back</a>
			<button type="submit" style="color:#fff " class="btn btn-primary uppercase pull-right">Submit</button>
		</div>
    {!! Form::close() !!}
	<!-- END FORGOT PASSWORD FORM -->
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@endsection
