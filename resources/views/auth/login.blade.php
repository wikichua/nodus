@extends('master.auth')

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ url('images/logo-big-white.png') }}" style="height: 17px;" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
    {!! Form::open(['url' => route('auth.post_login'), 'method' => 'POST']) !!}
		<div class="form-title">
			<span class="form-title">Welcome.</span>
			<span class="form-subtitle">Please login.</span>
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<button class="close" data-close="alert"></button>
        		@foreach ($errors->all() as $error)
					<span>{{ $error }} </span>
        		@endforeach
			</div>
        @endif
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="email" placeholder="Email" name="email" value="{{ old('email') }}"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" placeholder="Password" name="password"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary btn-block uppercase">Login</button>
		</div>
		<div class="form-actions">
			<div class="pull-left">
				<label class="rememberme check">
				<input type="checkbox" name="remember" checked/>Remember me </label>
			</div>
			<div class="pull-right forget-password-block">
				<a href="{{ route('password.email') }}" id="forget-password" class="forget-password">Forgot Password?</a>
			</div>
		</div>
		<div class="create-account">
			<p>
				{{-- disable register link --}}
				{{-- <a href="{{ route('auth.register') }}" id="register-btn">Create an account</a> --}}
			</p>
		</div>
    {!! Form::close() !!}
	<!-- END LOGIN FORM -->
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@endsection
