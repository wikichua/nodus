@extends('master.auth')

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ url('images/logo-big-white.png') }}" style="height: 17px;" alt=""/>
	</a>
</div>
<!-- END LOGO -->
<div class="content">
    <!-- BEGIN REGISTRATION FORM -->
    {!! Form::open(['url' => route('users.store'), 'method' => 'POST']) !!}
		<div class="form-title">
			<span class="form-title">Sign Up</span>
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<button class="close" data-close="alert"></button>
        		@foreach ($errors->all() as $error)
					<span>{{ $error }} </span>
        		@endforeach
			</div>
        @endif
		<p class="hint">
			 Enter your details below:
		</p>
		<div class="form-group">
			{!! Form::label('name', 'Name',array('class'=>'control-label visible-ie8 visible-ie9')) !!}
			{!! Form::text('name', "",array('class'=>'form-control','placeholder'=>'Name')) !!}
			
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			{!! Form::label('email', 'Email',array('class'=>'control-label visible-ie8 visible-ie9')) !!}
			{!! Form::text('email', "",array('class'=>'form-control','placeholder'=>'Email')) !!}
			
		</div>
		<div class="form-group">
				{!! Form::label('password', 'Password',array('class'=>'control-label visible-ie8 visible-ie9')) !!}
				{!! Form::password('password',array('class'=>'form-control','placeholder'=>'Password','autocomplete'=>'off')) !!}
			
			{{-- <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password"/> --}}
		</div>
		<div class="form-group">
			{!! Form::label('password_confirmation', 'Re-type Your Password',array('class'=>'control-label visible-ie8 visible-ie9')) !!}
			{!! Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Re-type Your Password','autocomplete'=>'off')) !!}

		</div>
		<div class="form-actions">
            <a href="{{ route('auth.login') }}" class="btn btn-default">Back</a>
			<button type="submit" id="register-submit-btn" class="btn btn-primary uppercase pull-right">Submit</button>
		</div>
    {!! Form::close() !!}
	<!-- END REGISTRATION FORM -->
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
 @include('master.toast_message');
@endsection
