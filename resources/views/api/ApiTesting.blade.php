<!DOCTYPE html>
<html>
<head>
	<title>NODUS - Mobile API Poster</title>
</head>
<body>
    <h2><a href="{{ route('api.poster') }}">NODUS - Mobile API Poster</a></h2>


	{{ route('ApiTesting_device_login') }}
	<form action="{{ route('ApiTesting_device_login') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"imei"=>"","email"=>"tester@convep.com","password"=>"tester")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('ApiTesting_device_check_requests') }}
	<form action="{{ route('ApiTesting_device_check_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('apitesting_app_id',''),"token"=>session('apitesting_token',''))) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('ApiTesting_device_complete_requests') }}
	<form action="{{ route('ApiTesting_device_complete_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('apitesting_app_id',''),"token"=>session('apitesting_token',''),"POC_OffAirRequest_id"=>"1","info"=>"info")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />




</body>
</html>