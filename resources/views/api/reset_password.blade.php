@extends('master.auth')

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ isset($CustomerIdList->app_icon) ? url('/uploads/' . $CustomerIdList->app_icon) : url('images/logo-big-white.png') }}" alt="app_icon" height="30px" />
	</a>
</div>
<!-- END LOGO -->
<div class="content">
    <!-- BEGIN PASSWORD RESET FORM -->
    {!! Form::open(['url' => route('api.reset_password.post',array($token,$type)), 'method' => 'POST']) !!}
		<input type="hidden" name="app_id" value="{{ $app_id }}">
		<input type="hidden" name="token" value="{{ $token }}">
		<div class="form-title">
			<span class="form-title">Password reset</span>
			<span class="form-subtitle">Enter your new password to reset it.</span>
		</div>
		@if (Session::has('error-message'))
			<div class="alert alert-danger">
				<span>{{ session('error-message') }} </span>
			</div>
		@endif
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9 ">New Password</label>
			<input class="form-control placeholder-no-fix" type="password" placeholder="Password" name="new_password"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password Confirmation</label>
			<input class="form-control placeholder-no-fix" type="password" placeholder="Password Confirmation" name="password_confirmation"/>
		</div>
		<div class="form-actions">
            <a href="{{ route('auth.login') }}" class="btn btn-default">Back</a>
			<button type="submit" class="btn btn-primary uppercase pull-right">Submit</button>
		</div>
    {!! Form::close() !!}
	<!-- END PASSWORD RESET FORM -->
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@endsection
