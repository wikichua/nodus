@extends('master.auth')

@section('title')
{{ isset($CustomerIdList->name)?$CustomerIdList->name: ''}}
@stop

@section('style')
<link href="{{ url('assets/admin/pages/css/login2.css') }}" rel="stylesheet" type="text/css"/>
@endsection


@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="{{ route('home') }}">
	<img src="{{ isset($CustomerIdList->app_icon) ? url('/uploads/' . $CustomerIdList->app_icon) : url('images/logo-big-white.png') }}" alt="app_icon" height="30px" />
	</a>
</div>
<!-- END LOGO -->
<div class="content">
	<div class="alert alert-success">
		<span>Account Activated Successfully </span>
	</div>
</div>
@endsection

@section('script')
<script src="{{ url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
@endsection
