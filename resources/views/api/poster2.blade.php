<!DOCTYPE html>
<html>
<head>
	<title>NODUS - Mobile API Poster</title>
</head>
<body>
    <h2><a href="{{ route('api.poster') }}">NODUS - Mobile API Poster</a></h2>

	{{ route('api.register') }}
	<form action="{{ route('api.register') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"name"=>"Tester","email"=>"tester@convep.com","gender"=>"MALE","dob"=>"1988-08-08","password"=>"tester")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.confirm.send') }}
	<form action="{{ route('api.confirm.send') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"email"=>"tester@convep.com")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.login') }}
	<form action="{{ route('api.login') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"email"=>"tester@convep.com","password"=>"tester")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.fogot_password.send') }}
	<form action="{{ route('api.fogot_password.send') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"email"=>"tester@convep.com")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.change_password') }}
	<form action="{{ route('api.change_password') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"email"=>"tester@convep.com","password"=>"tester","new_password"=>"tester")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.update_profile') }}
	<form action="{{ route('api.update_profile') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>1,"token"=>session('api_token',''),"name"=>"Tester2","gender"=>"MALE","dob"=>"1988-08-08")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.pt') }}
	<form action="{{ route('api.pt') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("width"=>1920,"height"=>1080,"app_id"=>1,"imei"=>"AND:353687062158081","token"=>session('api_token',''),"os_version"=>"5.0","settingIndex"=>"0000-00-00%252000:00:00","version"=>"1.0","push_token"=>"APA91bGyV3MnV3W9qOfxmgPkQvzbn5OrlegIkZU6xuJbbpgWo0h9cKK_LMpMBKUt1vHHRmIrMOtWLlOFZuoyom4K8HQEXwziDEU0Y0lipWrKfw532JGpB2GRc3XbRJu9aqAp9HYABQOY")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />
		
	{{ route('api2.setting') }} 
	<form action="{{ route('api2.setting') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array(
				'app_id' => session('app_id',''),
				'token' => session('api_token'),
			)) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.create_setting') }}
	<form action="{{ route('api2.create_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"zone"=>"zone1","scene"=>"scene1","timer"=>"timer1","manual_light"=>"manual_light1","setting"=>"setting1","location"=>"location1","location_type"=>"location_type1","sense"=>"sense1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.update_setting') }}
	<form action="{{ route('api2.update_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"zone"=>"zone1","scene"=>"scene1","timer"=>"timer1","manual_light"=>"manual_light1","setting"=>"setting1","location"=>"location1","setting_id"=>1,"location_type"=>"location_type1","sense"=>"sense1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.pair_device') }}
	<form action="{{ route('api2.pair_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"customer_id"=>161,"device_type"=>[83,83],"customer_sub_headerblock_id"=>[1,1],"customer_device_serial_id"=>[28,27],"lat"=>"12","long"=>"23","setting_id"=>"")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.unpair_device') }}
	<form action="{{ route('api2.unpair_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"customer_id"=>161,"device_type"=>[83,83],"customer_sub_headerblock_id"=>[1,1],"customer_device_serial_id"=>[28,27])) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.share_setting') }}
	<form action="{{ route('api2.share_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"setting_id"=>'1',"user2_email"=>"mobile@convep.com")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.unshare_setting') }}
	<form action="{{ route('api2.unshare_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"setting_id"=>'1',"user2_email"=>"mobile@convep.com")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.pending_setting') }} 
	<form action="{{ route('api2.pending_setting') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array(
				'app_id' => session('app_id',''),
				'token' => session('api_token'),
			)) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />


	{{ route('api2.accept_setting') }}
	<form action="{{ route('api2.accept_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"setting_id"=>"1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.make_admin') }}
	<form action="{{ route('api2.make_admin') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"user2_email"=>"mobile@convep.com","setting_id"=>"1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.unmake_admin') }}
	<form action="{{ route('api2.unmake_admin') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"user2_email"=>"mobile@convep.com","setting_id"=>"1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.leave_setting') }}
	<form action="{{ route('api2.leave_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"setting_id"=>"1")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.pairing_logs') }} 
	<form action="{{ route('api2.pairing_logs') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array(
				'app_id' => session('app_id',''),
				'token' => session('api_token'),
				'serial_num' => [28,27],
			)) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.feedback') }}
	<form action="{{ route('api.feedback') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"subject"=>"Cool","message"=>"This is very cool")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.poc_issue_requests') }}
	<form action="{{ route('api2.poc_issue_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"request"=>"","command"=>"","setting_id"=>"1","imei"=>"")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.poc_followup_requests') }}
	<form action="{{ route('api2.poc_followup_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"POC_OffAirRequest_id"=>"1","imei"=>"")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.poc_device_login') }}
	<form action="{{ route('api2.poc_device_login') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"imei"=>"","email"=>"tester@convep.com","password"=>"tester")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.poc_device_check_requests') }}
	<form action="{{ route('api2.poc_device_check_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''))) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.poc_device_complete_requests') }}
	<form action="{{ route('api2.poc_device_complete_requests') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"POC_OffAirRequest_id"=>"1","info"=>"info")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption') }}
	<form action="{{ route('api2.power_consumption') }}" method="POST">
		{{-- <textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>["1"],"setting_id"=>["1"],"power"=>["100"],"time"=>[Carbon\Carbon::now()->format('Y-m-d H:i:s')])) }}</textarea> --}}
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device"=>[array("device_id"=>"1","setting_id"=>"1","batch_id"=>"1","time"=>Carbon\Carbon::now()->format('Y-m-d H:').'00:00',"power"=>rand(0,100) ),array("device_id"=>"1","setting_id"=>"1","batch_id"=>"1","time"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00',"power"=>rand(0,100) )])) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption_get_webview') }}
	<form action="{{ route('api2.power_consumption_get_webview') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1"))}}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />
		{{ route('api2.power_consumption_get_webview2') }}
	<form action="{{ route('api2.power_consumption_get_webview2') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1"))}}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption_get_daily') }}
	<form action="{{ route('api2.power_consumption_get_daily') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1","from"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00')) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption_get_weekly') }}
	<form action="{{ route('api2.power_consumption_get_weekly') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1","from"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00')) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption_get_monthly') }}
	<form action="{{ route('api2.power_consumption_get_monthly') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1","from"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00')) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.power_consumption_get_yearly') }}
	<form action="{{ route('api2.power_consumption_get_yearly') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1","from"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00')) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.device_audit_trail') }}
	<form action="{{ route('api2.device_audit_trail') }}" method="POST">

	<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device"=>[array("device_id"=>"1","setting_id"=>"1","batch_id"=>"1","time"=>Carbon\Carbon::now()->format('Y-m-d H:').'00:00','mode'=>array('temp'=>rand(16,30),'mode'=>'on') ),array("device_id"=>"1","setting_id"=>"1","batch_id"=>"1","time"=>Carbon\Carbon::now()->addSeconds(5)->format('Y-m-d H:').'00:00','mode'=>array('temp'=>rand(16,30),'mode'=>'off') )])) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.device_audit_trail_get_report') }}
	<form action="{{ route('api2.device_audit_trail_get_report') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),"device_id"=>"1","setting_id"=>"1","batch_id"=>"1" )) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api2.firmware') }}
	<form action="{{ route('api2.firmware') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"token"=>session('api_token',''),)) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.logout') }}
	<form action="{{ route('api.logout') }}" method="POST">
		<textarea cols="100" name="data">{{ json_encode(array("app_id"=>session('app_id',''),"email"=>"tester@convep.com")) }}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

</body>
</html>
