<!DOCTYPE html>
<html>
<head>
	<title>NODUS - Mobile API Poster</title>
</head>
<body>
    <h2><a href="{{ route('api.poster') }}">NODUS - Mobile API Poster</a></h2>


	{{ route('api.register') }}
	<form action="{{ route('api.register') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","name":"Tester","email":"tester@convep.com","gender":"MALE","dob":"1988-08-08","password":"tester"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.confirm.send') }}
	<form action="{{ route('api.confirm.send') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","email":"tester@convep.com"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.login') }}
	<form action="{{ route('api.login') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","email":"tester@convep.com","password":"tester"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.fogot_password.send') }}
	<form action="{{ route('api.fogot_password.send') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","email":"tester@convep.com"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.change_password') }}
	<form action="{{ route('api.change_password') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","email":"tester@convep.com","password":"tester","new_password":"tester"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.update_profile') }}
	<form action="{{ route('api.update_profile') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","name":"Tester2","gender":"MALE","dob":"1988-08-08"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.pt') }}
	<form action="{{ route('api.pt') }}" method="POST">
		<textarea cols="100" name="data">{"width":1920,"height":1080,"app_id":"1","imei":"AND:353687062158081","token":"{{ session('api_token','') }}","os_version":"5.0","settingIndex":"0000-00-00%252000:00:00","version":"1.0","push_token":"APA91bGyV3MnV3W9qOfxmgPkQvzbn5OrlegIkZU6xuJbbpgWo0h9cKK_LMpMBKUt1vHHRmIrMOtWLlOFZuoyom4K8HQEXwziDEU0Y0lipWrKfw532JGpB2GRc3XbRJu9aqAp9HYABQOY"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.setting') }} 
	<form action="{{ route('api.setting') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.submit_setting') }}
	<form action="{{ route('api.submit_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","zone":"zone1","scene":"scene1","timer":"timer1","manual_light":"manual_light1","setting":"setting1"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.create_setting') }}
	<form action="{{ route('api.create_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","zone":"zone1","scene":"scene1","timer":"timer1","manual_light":"manual_light1","setting":"setting1","location":"location1"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.update_setting') }}
	<form action="{{ route('api.update_setting') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","zone":"zone1","scene":"scene1","timer":"timer1","manual_light":"manual_light1","setting":"setting1","location":"location1","setting_id":"1"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.check_device') }}
	<form action="{{ route('api.check_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.pair_device') }}
	<form action="{{ route('api.pair_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","customer_id":["161","161"],"device_type":["83","83"],"customer_sub_headerblock_id":["1","1"],"customer_device_serial_id":["28","27"],"lat":"12","long":"23","setting_id":""}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.unpair_device') }}
	<form action="{{ route('api.unpair_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","customer_id":["161","161"],"device_type":["83","83"],"customer_sub_headerblock_id":["1","1"],"customer_device_serial_id":["28","27"]}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.share_device') }}
	<form action="{{ route('api.share_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","customer_id":["161","161"],"device_type":["83","83"],"customer_sub_headerblock_id":["1","1"],"customer_device_serial_id":["28","27"],"user2_email":"mobile@convep.com"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.unshare_device') }}
	<form action="{{ route('api.unshare_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","customer_id":["161","161"],"device_type":["83","83"],"customer_sub_headerblock_id":["1","1"],"customer_device_serial_id":["28","27"],"user2_email":"mobile@convep.com"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.accept_device') }}
	<form action="{{ route('api.accept_device') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","device_id":"2"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.feedback') }}
	<form action="{{ route('api.feedback') }}" method="POST">
		<textarea rows="6" cols="100" name="data">{"app_id":"1","token":"{{ session('api_token','') }}","subject":"Cool","message":"This is very cool"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	{{ route('api.logout') }}
	<form action="{{ route('api.logout') }}" method="POST">
		<textarea cols="100" name="data">{"app_id":"1","email":"tester@convep.com"}</textarea>
	    <button type="submit">Submit</button>
	</form>
	<br />

	

</body>
</html>
