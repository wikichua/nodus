@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.feedbacks.index') }}">Feedback</a>
    </li>
</ul>
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-comment font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Show Feedback</span>
                </div>
            </div>
            <div class="portlet-body">
               
                <div class="form-horizontal margin-bottom-40">
                    <div class="form-horizontal margin-bottom-40">

                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">User Name</label>
                            <div class="col-md-6">
                                <input type="text" name="subject" readonly value="{{ $Feedbacks->user()->first()->name}}" class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">Date</label>
                            <div class="col-md-6">
                                <input type="text" name="subject" readonly value="{{ $Feedbacks->created_at }}" class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">Licensee</label>
                            <div class="col-md-6">
                                <input type="text" name="subject" readonly value="{{ isset($Feedbacks->licensee()->first()->name)?$Feedbacks->licensee()->first()->name:''}}" class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">Email</label>
                            <div class="col-md-6">
                                <input type="text" name="subject" readonly value="{{ $Feedbacks->user()->first()->email}}" class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label for="name" class="col-md-3 control-label">Subject</label>
                            <div class="col-md-6">
                                <input type="text" name="subject" readonly value=" {{  $Feedbacks->subject }} " class="form-control" id="form_control_1" >
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label for="email" class="col-md-3 control-label">Message</label>
                            <div class="col-md-6">
                                {!! Form::textarea('message', $Feedbacks->message, ['class' => 'form-control', 'placeholder' => 'Please fill in the value','readonly']) !!}
                                
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="{{ route('cms.feedbacks.index') }}" type="button" class="btn blue">Back</a>
                            </div>
                        </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
