@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.feedbacks.index') }}">Feedback</a>
    </li>
</ul>
@endsection

@section('content')
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-comment"></i>Feedback
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th>Mobile User Name</th>
                    
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Licensee</th>
                    <th>Date</th>
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Feedbacks as $Feedback)
                <tr class="odd gradeX">
                    <td>{{ isset($Feedback->user()->first()->name)?$Feedback->user()->first()->name:'' }}</td>
                    <td>{{ $Feedback->subject }}</td>
                    <td style="width:45%"><p style="text-overflow: clip ellipsis; width:100%" >{{ $Feedback->message }}</p></td>
                    <td >{{ isset($Feedback->licensee()->first()->name)?$Feedback->licensee()->first()->name:'' }}</td>
                    <td>{{ $Feedback->created_at }}</td>
                    <td>
                        <a href="{{ route('cms.feedbacks.show',$Feedback->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Show">
                            <i class="fa fa-list"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
    });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
