@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.device_type_lists.index') }}">Device Type</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Add device type</span>
				</div>
			</div>
			<div class="portlet-body">
			{!! Form::open(['url' => route('cms.device_type_lists.store'), 'role' => 'form', 'class' => 'form-horizontal margin-bottom-40']) !!}
                <div class="form-group form-md-line-input">
                    <label for="name" class="col-md-3 control-label">Name</label>
                    <div class="col-md-6">
                        {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="code" class="col-md-3 control-label">Code<sub>10</sub></label>
                    <div class="col-md-6">
                        {!! Form::text('code', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <p style="font-size:11px">* decimal format</p>
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ route('cms.device_type_lists.index') }}" type="button" class="btn blue">Back</a>
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
    @include('master.toast_message')
@endsection
