@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.customer_id_lists.index') }}">Licensee</a>
    </li>
</ul>
@endsection

@section('content')
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Licensee
        </div>
        <div class="actions">
            @can('admin','user')
            <a href="{{ route('cms.customer_id_lists.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add </a>
            @endcan
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th>App Id</th>
                    <th>Name</th>
                    <th class="text-center">App Icon</th>
                    <th class="text-center">GA Ids</th>
                    <th class="text-center">Background Color</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Code<sub>10</sub></th>
                    <th class="text-center">Code<sub>16</sub></th>
                    <th class="text-center">Website</th>
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($CustomerIdLists as $CustomerIdList)
                <tr class="odd gradeX">
                    <td class="text-right" style="width: 30px">{{ $CustomerIdList->id }}</td>
                    <td>{{ $CustomerIdList->name }}</td>
                    <td class="text-center" style="width: 80px"><img src="{{ isset($CustomerIdList->app_icon) ? url('/uploads/' . $CustomerIdList->app_icon) : url('images/default.png') }}" alt="app_icon" width="50" height="50" /></td>
                    <td class="text-center" style="width: 120px">{{ $CustomerIdList->ga_ids }}</td>
                    <td class="text-center" style="width: 120px">{{ $CustomerIdList->background_color }}</td>
                    <td class="text-center" style="width: 120px">{{ $CustomerIdList->email }}</td>
                    <td class="text-center" style="width: 70px">{{ $CustomerIdList->code }}</td>
                    <td class="text-center" style="width: 70px">0x{{ str_pad(dechex($CustomerIdList->code), 2, '0', STR_PAD_LEFT) }}</td>
                    <td class="text-center" style="width: 70px">{{ $CustomerIdList->website }}</td>
                    <td class="text-center" style="width: 60px">
                        @can('admin','user')
                            <a href="{{ route('cms.customer_id_lists.edit', $CustomerIdList->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
