@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.customer_id_lists.index') }}">Licensee</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Update licensee</span>
				</div>
			</div>
			<div class="portlet-body">
			{!! Form::open(['url' => route('cms.customer_id_lists.update', $CustomerIdList->id), 'method' => 'PUT', 'role' => 'form', 'files' => 'true', 'class' => 'form-horizontal margin-bottom-40']) !!}
                <div class="form-group form-md-line-input">
                    {!! Form::label('name', 'Name', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', $CustomerIdList->name, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('app_icon', 'App Icon', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        <img id="previewHere" src="{{ url('uploads/' . imgTagShow($CustomerIdList->app_icon)) }}" class="thumbnail col-sm-3 " width="100px" height="100%" >
                        {!! Form::file('app_icon', ['class' => 'form-control']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('ga_ids', 'GA Ids', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('ga_ids', $CustomerIdList->ga_ids, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('background_color', 'Background Color', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('background_color', $CustomerIdList->background_color, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('email', 'Email', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('email', $CustomerIdList->email, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="code" class="col-md-3 control-label">Code<sub>10</sub></label>
                    <div class="col-md-6">
                        {!! Form::text('code', $CustomerIdList->code, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <p style="font-size:11px">* decimal format</p>
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="code" class="col-md-3 control-label">Website</label>
                    <div class="col-md-6">
                        {!! Form::text('website', $CustomerIdList->website, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ route('cms.customer_id_lists.index') }}" type="button" class="btn blue">Back</a>
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#app_icon").change(function() {
                previewImg(this, 'previewHere');
                var fname = $('#app_icon').val();
                var filepath = fname.split('.');
                previewImg(this, 'previewHere');
            });

            function previewImg(input, previewBox) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#' + previewBox).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>
    @include('master.toast_message')
@endsection
