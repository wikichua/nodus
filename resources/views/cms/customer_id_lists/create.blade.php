@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.customer_id_lists.index') }}">Licensee</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Add licensee</span>
				</div>
			</div>
			<div class="portlet-body">
			{!! Form::open(['url' => route('cms.customer_id_lists.store'), 'role' => 'form', 'files' => 'true', 'class' => 'form-horizontal margin-bottom-40']) !!}
                <div class="form-group form-md-line-input">
                    <label for="name" class="col-md-3 control-label">Name</label>
                    <div class="col-md-6">
                        {!! Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('app_icon', 'App Icon', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        <img id="previewHere" src="{{ url('default.png') }}" class="thumbnail col-sm-3 " width="100px" height="100%" >
                        {!! Form::file('app_icon', ['class' => 'form-control']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="ga_ids" class="col-md-3 control-label">GA Ids</label>
                    <div class="col-md-6">
                        {!! Form::text('ga_ids', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="background_color" class="col-md-3 control-label">Background Color</label>
                    <div class="col-md-6">
                        {!! Form::text('background_color', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="email" class="col-md-3 control-label">Email</label>
                    <div class="col-md-6">
                        {!! Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="code" class="col-md-3 control-label">Code<sub>10</sub></label>
                    <div class="col-md-6">
                        {!! Form::text('code', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <p style="font-size:11px">* decimal format</p>
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="code" class="col-md-3 control-label">Website</label>
                    <div class="col-md-6">
                        {!! Form::text('website', '', ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ route('cms.customer_id_lists.index') }}" type="button" class="btn blue">Back</a>
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#app_icon").change(function() {
                previewImg(this, 'previewHere');
                var fname = $('#app_icon').val();
                var filepath = fname.split('.');
                previewImg(this, 'previewHere');
            });

            function previewImg(input, previewBox) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#' + previewBox).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>
    @include('master.toast_message')
@endsection
