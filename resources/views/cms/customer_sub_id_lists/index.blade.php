@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.customer_sub_id_lists.index') }}">Batch</a>
    </li>
</ul>
@endsection

@section('content')
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Batch
        </div>
        <div class="actions">
            @can('admin','user')
            <a href="{{ route('cms.customer_sub_id_lists.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add </a>
            @endcan
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center">Vendor</th>
                    <th class="text-center">IEEE</th>
                    <th class="text-center">Code<sub>10</sub></th>
                    <th class="text-center">Code<sub>16</sub></th>
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($CustomerSubHeaderblockIdLists as $CustomerSubHeaderblockIdList)
                <tr class="odd gradeX">
                    <td>{{ $CustomerSubHeaderblockIdList->name }}</td>
                    <td class="text-center" style="width: 120px">{{ isset($CustomerSubHeaderblockIdList->customer->name)?$CustomerSubHeaderblockIdList->customer->name:'' }}</td>
                    <td class="text-center" style="width: 120px">{{ isset($CustomerSubHeaderblockIdList->ieee->name)?$CustomerSubHeaderblockIdList->ieee->name:'' }}</td>
                    <td class="text-center" style="width: 120px">{{ $CustomerSubHeaderblockIdList->code }}</td>
                    <td class="text-center" style="width: 120px">0x{{ str_pad(dechex($CustomerSubHeaderblockIdList->code), 2, '0', STR_PAD_LEFT) }}</td>
                    <td class="text-center" style="width: 130px">
                         @can('admin','user')
                        <a href="{{ route('cms.customer_sub_id_lists.edit', $CustomerSubHeaderblockIdList->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <button data-href="{{ route('cms.customer_sub_id_lists.destroy', $CustomerSubHeaderblockIdList->id) }}" data-token="{{ csrf_token() }}" class="confirm-delete-dialog tooltips btn btn-icon-only default" data-toggle="modal" data-container="body" data-placement="top" data-html="true" data-original-title="Delete">
                            <i class="fa fa-trash-o"></i>
                        </button>
                         @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
