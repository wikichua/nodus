@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.inventory.index') }}">Inventory</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Show Inventory</span>
				</div>
			</div>
			<div class="portlet-body">
                <div class="form-horizontal margin-bottom-40">
                    <div class="form-group form-md-line-input">
                        <label for="name" class="col-md-3 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" name="name" readonly value="{{ $Device->name }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="serial_number" class="col-md-3 control-label">Serial Number<sub>16</sub></label>
                        <div class="col-md-6">
                            <input type="text" name="serial_number" readonly value="{{ $Device->serial_number }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="customer_id" class="col-md-3 control-label">Licensee</label>
                        <div class="col-md-6">
                            <input type="text" name="customer_id" readonly value="{{ $Device->customer_id }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="device_type" class="col-md-3 control-label">Device Type</label>
                        <div class="col-md-6">
                            <input type="text" name="device_type" readonly value="{{ $Device->device_type }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="ieee" class="col-md-3 control-label">IEEE</label>
                        <div class="col-md-6">
                            <input type="text" name="ieee" readonly value="{{ $Device->ieee }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="customer_sub_headerblock_id" class="col-md-3 control-label">Customer Sub Headerblock Id</label>
                        <div class="col-md-6">
                            <input type="text" name="customer_sub_headerblock_id" readonly value="{{ $Device->customer_sub_headerblock_id }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label for="customer_device_serial_id" class="col-md-3 control-label">Device Serial Id<sub>16</sub></label>
                        <div class="col-md-6">
                            <input type="text" name="customer_device_serial_id" readonly value="{{ $Device->customer_device_serial_id }}" class="form-control" id="form_control_1" placeholder="Enter serial number">
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue">Back</a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection
