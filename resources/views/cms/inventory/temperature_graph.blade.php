@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.inventory.index') }}">Inventory</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
     <a href="#"> Temperature ({{ $Device_name }})</a>
    </li>

</ul>
@endsection
@section('page_title')
<div class="page-head">
    <div class="col-md-12">
        <div class="page-title" style="width: 100%;">
            <div class="pull-right well well-sm">
                {!! Form::open(array(route('chartjs',$CustomerIdLists_id) ,'method'=>'get','name'=>'theform','files'=>true,'class'=>'form-inline','style'=>'display:inline-block;')) !!}
<!--                         <div class="form-group">
                            {!! Form::label('form_date','From') !!}
                            {!! Form::date('form_date',Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $form_date),array('class'=>'form-control ')) !!}
                        </div> -->
                        <div class="form-group">
                            {!! Form::label('to_date','Type') !!}
                            {!! Form::select('to_date',array(1=>"Temperature",7=>"Indoor Temperature",365=>"outdoor Temperature"),$to_date,array('class'=>'form-control ')) !!}
                        </div>
                        <button type="submit" class="btn btn-default"><i class="fa fa-search-plus"></i></button>
                {!! Form::close() !!}
                <!-- <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Charts <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Temperature</a></li>
                            <li><a href="{{  route('temperature_graph',array($CustomerIdLists_id,$batch_id))."?form_date=".Carbon\Carbon::now()->format('Y-m-d')."&to_date=1" }}">Temperature<br/>Indoor</a></li>
                            <li><a href="{{  route('temperature_graph',array($CustomerIdLists_id,$batch_id)) }}">Temperature<br/>Outdoor</a></li>
                        </ul>
                </div>
 -->
            </div>
        </div>
    </div>
</div>

@stop

@section('content')
<!-- BEGIN EXAMPLE TABLE PORTLET-->


<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Device Temperature
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <canvas id="canvas" height="280" width="600"></canvas>
        <div class="text-center">
            <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue">Back</a>
        </div>
    </div>
</div>

{{-- <div class="portlet box grey-cascade chart2"hidden>
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Device Temperature
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <canvas id="canvas3" height="280" width="600"></canvas>
        <div class="text-center">
            <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue">Back</a>
        </div>
    </div>
</div> --}}

<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
<script src="{{ url('assets/Chart.bundle.js') }}" type="text/javascript"></script>
<script>


    var unit =  "<?php echo $unit; ?>";
    var Device_name ="<?php echo $Device_name; ?>";
    var last = <?php echo $to_date-1; ?>;
    var time =  <?php echo $time; ?>;
    var Temperature = <?php echo $Temperature; ?>;

    var barChartData = {
        labels:time,
         datasets: [
        {
            label: 'Temperature',
            backgroundColor: "rgba(151,187,205,0.5)",
            data: Temperature
        }]
    };


    window.onload = function() {

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                tooltips: {

                },
                scales: {
                    yAxes: [{
                ticks: {
                    min: 0
                }
            }],
                xAxes: [{
                    // type: 'time',
                    // time: {
                    //     max:time[last],
                    //     unit:unit,
                    // }
            }]
        },
                title: {
                    display: true,
                    text: Device_name+' Temperature'
                }
            }
        });

       

    };
    

</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection






