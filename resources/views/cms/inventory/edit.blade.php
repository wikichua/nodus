@extends('master.master')

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.inventory.index') }}">Inventory</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption font-green-haze">
					<i class="fa fa-rocket font-green-haze"></i>
					<span class="caption-subject bold uppercase"> Update Inventory</span>
				</div>
			</div>
			<div class="portlet-body">
			{!! Form::open(['url' => route('cms.inventory.update', $Device->id), 'method' => 'PUT', 'role' => 'form', 'class' => 'form-horizontal margin-bottom-40']) !!}
                <div class="form-group form-md-line-input">
                    {!! Form::label('name', 'Name', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', $Device->name, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('customer_id', 'Licensee', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('customer_id', $customer_id_lists, $Device->customer_id, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('customer_sub_headerblock_id', 'Batch', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" id="customer_sub_headerblock_id" name="customer_sub_headerblock_id">
                            <option value="">Please fill in the value</option>
                            @foreach ($customer_sub_headerblock_id_lists as $key => $customer_sub_headerblock_id_list)
                                <option @if ($Device->customer_sub_headerblock_id === $customer_sub_headerblock_id_list['batch_code']) selected="selected" @endif value="{{ $customer_sub_headerblock_id_list['batch_code'] }}" class="{{ $customer_sub_headerblock_id_list['customer_code'] }}">{{ $customer_sub_headerblock_id_list['batch_code'] }} - {{ $customer_sub_headerblock_id_list['batch_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('ieee', 'IEEE', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        <select class="form-control" id="ieee" name="ieee">
                            <option value="">Please fill in the value</option>
                            @foreach ($ieee_lists as $key => $ieee_list)
                                <option @if ($Device->ieee === $ieee_list['ieee_code']) selected="selected" @endif value="{{ $ieee_list['ieee_code'] }}" class="{{ $ieee_list['customer_code'] }}">{{ $ieee_list['ieee_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    {!! Form::label('device_type', 'Device Type', ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('device_type', $device_type_lists, $Device->device_type, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="customer_device_serial_id" class="col-md-3 control-label">Device Serial Id<sub>10</sub></label>
                    <div class="col-md-6">
                        {!! Form::text('customer_device_serial_id', $Device->customer_device_serial_id, ['class' => 'form-control', 'placeholder' => 'Please fill in the value']) !!}
                        <div class="form-control-focus"></div>
                        <p style="font-size:11px">* decimal format</p>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label for="status" class="col-md-3 control-label">Status</label>
                    <div class="col-md-6">
                        {!! Form::select('status', array('Pending'=>'Pending','Approved'=>'Approved'), $Device->status, ['class' => 'form-control']) !!}
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                 <div class="form-group form-md-line-input">
                    <label for="Leadsource" class="col-md-3 control-label">Leadsource</label>
                    <div class="col-md-6">
                        <input type="text" name="Leadsource" readonly value= " {{  $Device->leadsource }} "  class="form-control" id="form_control_1" >
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue">Back</a>
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
    @include('master.toast_message')
    <script src="{{ url('bower_components/chained/jquery.chained.min.js') }}"></script>
    <script type="text/javascript">
        $("#customer_sub_headerblock_id").chained("#customer_id");
        $("#ieee").chained("#customer_id");
    </script>
@endsection
