@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.inventory.index') }}">Inventory</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="#"> Device Audit Trail ({{ $Device_name }})</a>
    </li>

</ul>
@endsection
@section('page_title')
<div class="page-head">
    <div class="col-md-12">
        <div class="page-title" style="width: 100%;">
            <div class="pull-right well well-sm">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Charts <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{  route('chartjs',array($CustomerIdLists_id,$batch_id))."?form_date=".Carbon\Carbon::now()->format('Y-m-d')."&to_date=1" }}">    Power<br/>Consumption</a></li>
                            <li><a href="{{  route('chartjs2',array($CustomerIdLists_id,$batch_id))."?form_date=".Carbon\Carbon::now()->format('Y-m-d')."&to_date=1" }}">Power<br/>Consumption 2</a></li>
                            <li><a href="{{  route('grid',array($CustomerIdLists_id,$batch_id)) }}">Audit Trail</a></li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('content')


<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Device Audit Trail
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    @foreach( $TH as $th )
                        <th>
                            {{ $th  }}
                        </th>
                    @endforeach
            <th>
                Time
            </th>
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($TD as $key => $Td)    
                    <tr>
                        @foreach( $Td as $td )
                            <td>
                                {{ $td  }}
                            </td>
                        @endforeach
                        <td>
                            {{ $time[$key] }}
                        </td>

                        <td>
                        </td>

                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
            <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue ">Back</a>
        </div>
        

    </div>
</div>



<!-- BEGIN EXAMPLE TABLE PORTLET-->



{{-- <div class="portlet box grey-cascade chart2"hidden>
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Device Power Consumption
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <canvas id="canvas3" height="280" width="600"></canvas>
        <div class="text-center">
            <a href="{{ route('cms.inventory.index') }}" type="button" class="btn blue">Back</a>
        </div>
    </div>
</div> --}}

<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')

</script>

</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')    
<!-- END PAGE LEVEL SCRIPTS -->
@endsection






