@extends('master.master')

@section('style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/select2/select2.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="{{ route('home') }}">Home</a>
        <i class="fa fa-arrow-right"></i>
    </li>
    <li>
        <a href="{{ route('cms.inventory.index') }}">Inventory</a>
    </li>
</ul>
@endsection

@section('content')
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-rocket"></i>Inventory
        </div>
        <div class="actions">
            @can('admin', auth()->user())
            <a href="{{ route('cms.inventory.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Add </a>
            @endcan
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Serial Number<sub>16</sub></th>
                    <th>Licensee</th>
                    <th>Status</th>
                    <th>Leadsource</th>
                    <th class="text-center"><i class="fa fa-cogs"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($Devices as $Device)

                <tr class="odd gradeX ">

                    <td >{{ $Device->name }}</td>
                    <td >{{ $Device->serial_number }}</td>
                    <td>{{ $Device->customer_id }}</td>
                    <td>{{ $Device->status }}</td>
                    <td>{{ $Device->leadsource }}</td>
                    <td class="text-center" style="width: 170px">

                        <a href="{{ route('temperature_graph', array($Device->id,$Device->customer_sub_headerblock_id))."?form_date=".Carbon\Carbon::now()->format('Y-m-d')."&to_date=1" }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Temperature">
                            <i class="fa fa-tasks "></i>
                        </a>
                        
                        <a href="{{ route('chartjs', array($Device->id,$Device->customer_sub_headerblock_id))."?form_date=".Carbon\Carbon::now()->format('Y-m-d')."&to_date=1" }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Chart">
                            <i class="fa fa-area-chart"></i>
                        </a>
                        <a href="{{ route('cms.inventory.show', $Device->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Show">
                            <i class="fa fa-list"></i>
                        </a>
                        @can('admin','user')
                        <a href="{{ route('cms.inventory.edit', $Device->id) }}" class="tooltips btn btn-icon-only default" data-container="body" data-placement="top" data-html="true" data-original-title="Edit">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <button data-href="{{ route('cms.inventory.destroy', $Device->id) }}" data-token="{{ csrf_token() }}" class="confirm-delete-dialog tooltips btn btn-icon-only default" data-toggle="modal" data-container="body" data-placement="top" data-html="true" data-original-title="Delete">
                            <i class="fa fa-trash-o"></i>
                        </button>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
@endsection

@section('script')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ url('assets/global/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('assets/admin/pages/scripts/table-managed.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('script/confirm-delete-dialog.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        TableManaged.init();
        $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
    });
</script>
@include('master.toast_message')
<!-- END PAGE LEVEL SCRIPTS -->
@endsection
