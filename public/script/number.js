/**
 * Credits
 * =======
 * http://coderstoolbox.net/number/
 */

var regexps = {
    2: /^([-+]?[01]*)(\.[01]*)?$/,
    8: /^([-+]?[0-7]*)(\.[0-7]*)?$/,
    10: /^([-+]?\d*)(\.\d*)?$/,
    16: /^([-+]?[0-9a-f]*)(\.[0-9a-f]*)?$/i
};

function numberOnInput(input) {
    var base = parseInt(input.name.substr(4));
    var s = input.value;
    if (base == 16 && s.substr(0, 2) == '0x') {
        s = s.substr(2);
    }
    s = s.replace(/^ +| +$/g, '');
    var n;
    var matches = s.match(regexps[base]);
    if (!matches) {
        n = NaN;
    } else if (!matches[2] || matches[2].length < 2) {
        n = parseInt(matches[1], base);
    } else {
        n = parseInt(matches[1], base);
        n += (n > 0 ? 1 : -1) * parseInt(matches[2].substr(1), base) / Math.pow(base, matches[2].length - 1);
    }
    var bases = [2, 8, 10, 16];
    for (var i = 0; i < bases.length; i++) {
        if (bases[i] != base) {
            var output;
            if (isNaN(n)) {
                output = '';
            } else if (16.25.toString(16) == '10.4') {
                output = n.toString(bases[i]);
            } else {
                output = (n > 0 ? Math.floor(n) : Math.ceil(n)).toString(bases[i]);
                if (n % 1) {
                    output += '.' + Math.round((Math.abs(n) % 1) * Math.pow(bases[i], 8)).toString(bases[i]);
                    output = output.replace(/0+$/, '');
                }
            }
            document.getElementById('base' + bases[i]).value = output.toUpperCase();
        }
    }
}

window.onload = function() {
    var base10 = document.getElementById('base10');
    base10.value = 161;
    numberOnInput(base10);
};

function highlightAccessKeys() {
    if (!document.getElementsByTagName) return;
    var labels = document.getElementsByTagName('LABEL');
    for (var i = 0; i < labels.length; i++) {
        var control = document.getElementById(labels[i].htmlFor);
        if (control && control.accessKey) {
            highlightAccessKey(labels[i], control.accessKey);
        } else if (labels[i].accessKey) {
            highlightAccessKey(labels[i], labels[i].accessKey);
        }
    }
    var tagNames = new Array('A', 'BUTTON', 'LEGEND');
    for (var j = 0; j < tagNames.length; j++) {
        var elements = document.getElementsByTagName(tagNames[j]);
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].accessKey) {
                highlightAccessKey(elements[i], elements[i].accessKey);
            }
        }
    }
}

function highlightAccessKey(e, accessKey) {
    if (e.hasChildNodes()) {
        var childNode, txt;
        for (var i = 0; i < e.childNodes.length; i++) {
            txt = e.childNodes[i].nodeValue;
            if (e.childNodes[i].nodeType == 3 && txt.toLowerCase().indexOf(accessKey.toLowerCase()) != -1) {
                childNode = e.childNodes[i];
                break;
            }
        }
        if (!childNode) {
            return;
        }
        var pos = txt.toLowerCase().indexOf(accessKey.toLowerCase());
        var span = document.createElement('SPAN');
        var spanText = document.createTextNode(txt.substr(pos, 1));
        span.className = 'accesskey';
        span.appendChild(spanText);
        var text1 = document.createTextNode(txt.substr(0, pos));
        var text2 = document.createTextNode(txt.substr(pos + 1));
        if (text1.length > 0) e.insertBefore(text1, childNode);
        e.insertBefore(span, childNode);
        if (text2.length > 0) e.insertBefore(text2, childNode);
        e.removeChild(childNode);
    }
}

function hookUpOnInput() {
    var tagNames = ['INPUT', 'TEXTAREA'];
    for (var j = 0; j < tagNames.length; j++) {
        var elements = document.getElementsByTagName(tagNames[j]);
        for (var i = 0; i < elements.length; i++) {
            var e = elements[i];
            var oninput = e.getAttribute('ONINPUT');
            if (oninput) {
                e.prevValue = e.value;
                e.onkeypress = e.onkeyup = e.onchange = onPerhapsChanged;
                e.onmouseup = function(event) {
                    window.setTimeout(function() {
                        onPerhapsChanged.call(event.target);
                    }, 1);
                    (event.target);
                };
                e.onfocus = onFocus;
                e.onChanged = new Function(oninput);
                e.removeAttribute('ONINPUT');
            }
        }
    }
}

function onFocus() {
    this.prevValue = this.value;
}

function onPerhapsChanged() {
    if (this.value != this.prevValue) {
        this.onChanged();
        this.prevValue = this.value;
    }
}

String.prototype.right = function(i) {
    return this.substr(this.length - i);
};

Function.prototype.applyDelayed = function(thisArg, args) {
    if (typeof this.delayedTimeout != 'undefined') {
        window.clearTimeout(this.delayedTimeout);
    }
    var method = this;
    this.delayedTimeout = window.setTimeout(function() {
        method.apply(thisArg, !args ? [] : args);
        method.delayedTimeout = undefined;
    }, 750);
};

function callServerFunction(methodName, args, callback) {
    var xmlHttp = null; /*@cc_on @*/
    /*@if (@_jscript_version >= 5)
    		var ids = ['Msxml2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0',
    			'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP'];
    		for (var i = 0; !xmlHttp && i < ids.length; i++) {
    			try {
    				xmlHttp = new ActiveXObject(ids[i]);
    			} catch(e) {}
    		}
    	@end @*/
    if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
        xmlHttp = new XMLHttpRequest();
    }
    if (!xmlHttp || typeof xmlHttp.send == 'undefined') {
        return false;
    }
    var url = 'rpc.php?method=' + methodName;
    for (var i = 0; i < args.length; i++) {
        url += '&args[' + i + ']=' + encodeURIComponent(args[i]);
    }
    xmlHttp.open('GET', url, true);
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4) {
            output = eval(xmlHttp.responseText);
            callback.apply(null, output);
        }
    };
    xmlHttp.send(null);
    return true;
}

function hasSearchWord(s) {
    var matches = /\?.*\bq=([^&]+)/.exec(document.referrer);
    return matches && decodeURIComponent(matches[1].toLowerCase()).indexOf(s.toLowerCase()) != -1;
}
