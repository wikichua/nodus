$(document).ready(function() {

    $('.confirm-delete-dialog').click(function() {
        $self = $(this);
        bootbox.confirm('Are you sure you want to delete that?', function(confirmed) {
            if (confirmed) {
                $.ajax({
                    url: $self.data('href'),
                    type: 'POST',
                    data: {
                        _method: 'delete',
                        _token: $self.data('token')
                    },
                }).done(function(data) {
                    location.reload();
                });
            }
        });
    });
        
        $('.confirm-switchUser-dialog').click(function() {
        $self = $(this);
        bootbox.confirm('Are you sure you want switch to this user?', function(confirmed) {
            if (confirmed) {
                $.ajax({
                    //url: $self.data('href'),
                    type: 'GET',
                    // data: {
                    //     //_method: 'switchUser',
                    //      // _token: $self.data('token')
                    // },
                }).done(function(data) {
                    window.document.location = $self.data("href");
                });
            }
        });
    });

    function previewImg(input,previewBox) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+previewBox).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

});
