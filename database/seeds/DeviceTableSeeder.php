<?php

use Illuminate\Database\Seeder;
use App\Device;

class DeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Device::create([
            'name'                        => 'device 1',
            'customer_id'                 => hexdec("A1"),
            'device_type'                 => hexdec("53"),
            'ieee'                        => hexdec("A082AC"),
            'customer_sub_headerblock_id' => hexdec("02"),
            'customer_device_serial_id'   => hexdec("0001"),
            'gps'                         => '{"lat":[0],"long":[0]}',
        ]);

        Device::create([
            'name'                        => 'device 2',
            'customer_id'                 => hexdec("B2"),
            'device_type'                 => hexdec("65"),
            'ieee'                        => hexdec("B082AC"),
            'customer_sub_headerblock_id' => hexdec("03"),
            'customer_device_serial_id'   => hexdec("0002"),
        ]);
    }
}
