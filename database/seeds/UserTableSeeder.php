<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Super User',
            'email'     => 'su@convep.com',
            'role'      =>'su',
            'app_id' => '0',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
        ]);

        User::create([
            'name'      => 'Kevin Khew',
            'email'     => 'kevinkhew@convep.com',
            'role'      => 'admin',
            'app_id' => '0',
            'password'  => bcrypt('kevinkhew'),
            'confirmed' => 1,
        ]);

        User::create([
            'name'      => 'Panasonic',
            'email'     => 'panasonic@convep.com',
            'role'      => 'customer',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
            'app_id'    => 1,
        ]);

        User::create([

            'name'      => 'Panasonic Sub Customer',
            'email'     => 'panasonic_sub@convep.com',
            'role'      => 'sub_customer',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
            'app_id'    => 1,
        ]);

        User::create([
            'name'      => 'Pensonic',
            'email'     => 'pensonic@convep.com',
            'role'      => 'customer',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
            'app_id'    => 2,
        ]);

        User::create([

            'name'      => 'Pensonic Sub Customer',
            'email'     => 'pensonic_sub@convep.com',
            'role'      => 'sub_customer',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
            'app_id'    => 2,
        ]);

        User::create([
            'name'      => 'mobile',
            'email'     => 'mobile@convep.com',
            'role'      => 'mobile',
            'password'  => bcrypt('ABC123abc'),
            'confirmed' => 1,
            'app_id'    => 1,
        ]);
    }
}
