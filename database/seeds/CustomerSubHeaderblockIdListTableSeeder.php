<?php

use Illuminate\Database\Seeder;
use App\CustomerSubHeaderblockIdList;

class CustomerSubHeaderblockIdListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerSubHeaderblockIdList::create([
            'name' => 'Panasonic',
            'code' => hexdec("02"),
            'ieee_list_id' => 1,
            'customer_id_list_id' => 1,
        ]);

        CustomerSubHeaderblockIdList::create([
            'name' => 'Pensonic',
            'code' => hexdec("03"),
            'ieee_list_id' => 2,
            'customer_id_list_id' => 2,
        ]);
    }
}
