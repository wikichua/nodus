<?php

use Illuminate\Database\Seeder;
use App\CustomerIdList;

class CustomerIdListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerIdList::create([
            'name' => 'Panasonic',
            'code' => hexdec("A1"),
        ]);

        CustomerIdList::create([
            'name' => 'Pensonic',
            'code' => hexdec("B2"),
        ]);
    }
}
