<?php

use Illuminate\Database\Seeder;
use App\IeeeList;

class IeeeListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IeeeList::create([
            'name' => 'IEEE A',
            'code' => hexdec("A082AC"),
        ]);

        IeeeList::create([
            'name' => 'IEEE B',
            'code' => hexdec("B082AC"),
        ]);
    }
}
