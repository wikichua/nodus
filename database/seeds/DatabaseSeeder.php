<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(DeviceTableSeeder::class);
        $this->call(CustomerIdListTableSeeder::class);
        $this->call(CustomerSubHeaderblockIdListTableSeeder::class);
        $this->call(DeviceTypeListTableSeeder::class);
        $this->call(IeeeListTableSeeder::class);

        Model::reguard();
    }
}
