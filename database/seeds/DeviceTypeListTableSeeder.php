<?php

use Illuminate\Database\Seeder;
use App\DeviceTypeList;

class DeviceTypeListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeviceTypeList::create([
            'name' => '@Nodus-MotionSensor',
            'code' => hexdec("53"),
        ]);

        DeviceTypeList::create([
            'name' => '@Nodus-MotionSensor2',
            'code' => hexdec("65"),
        ]);
    }
}
