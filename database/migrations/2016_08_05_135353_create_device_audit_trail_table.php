<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceAuditTrailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_audit_trails', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('device_id');
            $table->longtext('setting_id');
            $table->longtext('mode');
            $table->datetime('time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_audit_trails');
    }
}
