<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password', 60);
            $table->string('picture')->nullable()->default('');
            $table->string('fb_id');
            $table->string('role');
            $table->string('parent_id')->nullable()->default(0);
            $table->smallInteger('household_id')->unsigned();
            $table->smallInteger('gender_age')->unsigned();
            $table->integer('country_code');
            $table->string('state');
            $table->string('suburb');
            $table->string('app_passcode', 4)->default('0000');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
