<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGaIdsToCustomerIdListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_id_lists', function (Blueprint $table) {
            $table->string('ga_ids');
            $table->string('app_icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_id_lists', function (Blueprint $table) {
            $table->dropColumn('ga_ids');
            $table->dropColumn('app_icon');
        });
    }
}
