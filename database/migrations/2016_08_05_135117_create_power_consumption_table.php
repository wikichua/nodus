<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerConsumptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_consumptions', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('device_id');
            $table->longtext('setting_id');
            $table->longtext('power');
            $table->datetime('time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('power_consumptions');
    }
}
