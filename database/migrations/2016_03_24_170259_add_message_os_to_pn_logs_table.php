<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageOsToPnLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pn_logs', function (Blueprint $table) {
            $table->string('os');
            $table->longText('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pn_logs', function (Blueprint $table) {
            $table->dropColumn('os');
            $table->dropColumn('message');
        });
    }
}
