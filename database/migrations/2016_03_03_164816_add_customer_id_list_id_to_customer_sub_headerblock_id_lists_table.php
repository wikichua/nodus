<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerIdListIdToCustomerSubHeaderblockIdListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_sub_headerblock_id_lists', function (Blueprint $table) {
            $table->integer('customer_id_list_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_sub_headerblock_id_lists', function (Blueprint $table) {
            $table->dropColumn('customer_id_list_id');
        });
    }
}
