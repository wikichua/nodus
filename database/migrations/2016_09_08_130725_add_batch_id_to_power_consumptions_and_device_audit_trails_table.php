<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBatchIdToPowerConsumptionsAndDeviceAuditTrailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('power_consumptions', function (Blueprint $table) {
            $table->integer('batch_id');
        });
        Schema::table('device_audit_trails', function (Blueprint $table) {
            $table->integer('batch_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('power_consumptions', function (Blueprint $table) {
            $table->dropColumn('batch_id');
        });

        Schema::table('device_audit_trails', function (Blueprint $table) {
            $table->dropColumn('batch_id');
        });
    }
}
