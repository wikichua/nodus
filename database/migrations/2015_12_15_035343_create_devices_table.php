<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('customer_id')->unsigned();
            $table->tinyInteger('device_type')->unsigned();
            $table->mediumInteger('ieee')->unsigned();
            $table->tinyInteger('customer_sub_headerblock_id')->unsigned();
            $table->smallInteger('customer_device_serial_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
