<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailToCustomerIdLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_id_lists', function (Blueprint $table) {
            $table->string('background_color');
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_id_lists', function (Blueprint $table) {
            $table->dropColumn('background_color');
            $table->dropColumn('email');
        });
    }
}
