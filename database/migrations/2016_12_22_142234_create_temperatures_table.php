<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemperaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temperatures', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('device_id');
            $table->integer('batch_id');
            $table->longtext('setting_id');
            $table->longtext('temperature');
            $table->longtext('room_temperature');
            $table->longtext('outdoor_temperature');
            $table->datetime('time');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temperatures');
    }
}
