<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSubHeaderblockIdListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_sub_headerblock_id_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('code')->unsigned();
            $table->integer('ieee_list_id')->default(0)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_sub_headerblock_id_lists');
    }
}
