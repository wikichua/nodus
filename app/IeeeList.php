<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IeeeList extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'code'];

    public function batch()
    {
        return $this->hasMany('App\CustomerSubHeaderblockIdList');
    }
}
