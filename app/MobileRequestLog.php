<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileRequestLog extends Model
{
    protected $fillable = [
        'name',
        'info',
    ];
}
