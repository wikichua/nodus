<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MobileUser extends Model
{
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'imei',
        'width',
        'height',
        'os',
        'os_version',
        'app_version',
        'push_token',
        'user_id',
    ];
}
