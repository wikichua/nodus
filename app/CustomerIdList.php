<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerIdList extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'code',
        'ga_ids',
        'app_icon',
        'background_color',
        'email',
        'app_id'
    ];

    public function batch()
    {
        return $this->hasMany('App\CustomerSubHeaderblockIdList', 'customer_id_list_id');
    }
}
