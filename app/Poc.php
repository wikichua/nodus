<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poc extends Model
{
    use SoftDeletes;
	
    protected $dates = ['deleted_at'];
    protected $guarded = [];

 public function setting()
    {
        return $this->hasOne('App\Setting','id','setting_id');
    }
  
}
