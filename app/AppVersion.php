<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AppVersion extends Model
{
    use SoftDeletes;

    protected $table = 'app_versions';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
