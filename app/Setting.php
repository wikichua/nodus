<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function devices()
    {
        return $this->hasMany('App\Device','setting_id','id');
    }

    public function getUserIdsAttribute($value)
    {
        return json_decode($value)? json_decode($value,true):array();
    }

    public function setUserIdsAttribute($value)
    {
        $this->attributes['user_ids'] = json_encode($value);
    }

    public function getAdminIdsAttribute($value)
    {
        return json_decode($value)? json_decode($value,true):array();
    }

    public function setAdminIdsAttribute($value)
    {
        $this->attributes['admin_ids'] = json_encode($value);
    }
}
