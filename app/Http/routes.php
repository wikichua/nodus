<?php

get('/',  ['as' => 'home', 'uses' => 'HomeController@index']);
get('/chart1/{id}',  ['as' => 'home.ga', 'uses' => 'HomeController@ga1']);

get('auth/login',             ['as' => 'auth.login',           'uses' => 'Auth\AuthController@getLogin']);
post('auth/login',            ['as' => 'auth.post_login',      'uses' => 'Auth\AuthController@postLogin']);
get('auth/logout',            ['as' => 'auth.logout',          'uses' => 'Auth\AuthController@getLogout']);
get('password/email',         ['as' => 'password.email',       'uses' => 'Auth\PasswordController@getEmail']);
post('password/email1',        ['as' => 'password.post_email',  'uses' => 'Auth\PasswordController@postEmail']);
get('password/reset/{token}', ['as' => 'password.reset',       'uses' => 'Auth\PasswordController@getReset']);
post('password/reset',        ['as' => 'password.reset_email', 'uses' => 'Auth\PasswordController@postReset']);

Route::put('/update', array('as'=>'home.update','uses'=>'HomeController@update'));
Route::get('/profile', array('as'=>'home.profile','uses'=>'HomeController@profile'));
Route::get('/switch/{id}', array('as'=>'home.switch','uses'=>'HomeController@switch_user'));
Route::put('/updateimage', array('as' =>'home.updateimage','uses'=>'HomeController@updateimage'));
Route::put('/updatepassword',array('as' =>'home.updatepassword',	'uses'=>'HomeController@updatepassword'));
Route::get('cms/chartjs/{id}/{batch_id}', array('as' =>'chartjs','uses'=>'Cms\DeviceController@chartjs'));
Route::get('cms/chartjs2/{id}/{batch_id}', array('as' =>'chartjs2','uses'=>'Cms\DeviceController@chartjs2'));
Route::get('cms/temperature_graph/{id}/{batch_id}', array('as' =>'temperature_graph','uses'=>'Cms\DeviceController@temperature_graph'));
Route::get('/chart2/{id}', array('as' =>'home.chartjs','uses'=>'HomeController@chartjs'));
Route::get('cms/webview/{id}/{setting_id}/{batch_id}', array('as' =>'webview','uses'=>'Cms\DeviceController@webview'));
Route::get('cms/webview2/{id}/{setting_id}/{batch_id}', array('as' =>'webview2','uses'=>'Cms\DeviceController@webview2'));
Route::get('cms/webview3/{id}/{setting_id}/{batch_id}', array('as' =>'webview3','uses'=>'Cms\DeviceController@webview3'));
Route::get('cms/grid/{id}/{batch_id}', array('as' =>'grid','uses'=>'Cms\DeviceController@grid'));
Route::get('/app/version', array('as'=>'app.version','uses'=>'AppVersionController@index'));
Route::get('/app/version/create', array('as'=>'app.version.create','uses'=>'AppVersionController@create'));
Route::post('/app/version/store', array('as'=>'app.version.store','uses'=>'AppVersionController@store'));
Route::get('/app/version/{id}/edit', array('as'=>'app.version.edit','uses'=>'AppVersionController@edit'));
Route::put('/app/version/{id}/update', array('as'=>'app.version.update','uses'=>'AppVersionController@update'));
Route::get('/app/version/{id}/destroy', array('as'=>'app.version.destroy','uses'=>'AppVersionController@destroy'));

Route::get('/app/firmware/', array('as'=>'app.firmware','uses'=>'FirmwareController@index'));
Route::get('/app/firmware/create', array('as'=>'app.firmware.create','uses'=>'FirmwareController@create'));
Route::post('/app/firmware/store', array('as'=>'app.firmware.store','uses'=>'FirmwareController@store'));
Route::get('/app/firmware/{id}/edit', array('as'=>'app.firmware.edit','uses'=>'FirmwareController@edit'));
Route::put('/app/firmware/{id}/update', array('as'=>'app.firmware.update','uses'=>'FirmwareController@update'));
Route::get('/app/firmware/{id}/destroy', array('as'=>'app.firmware.destroy','uses'=>'FirmwareController@destroy'));

// Route::get('change_id', array('as' =>'change_id',function(){

// $devices = App\Device::where('status','Approve')->get();
// foreach ($devices as $key => $device) {
// 	$device->status = 'Approved';
// 	$device->save();
// }
// 		dd($devices);

// }));



get('api/poster',                    ['as' => 'api.poster',              'uses' => 'HomeController@poster']);
post('api/register',                 ['as' => 'api.register',            'uses' => 'Api\AuthController@register']);
post('api/confirm/send',             ['as' => 'api.confirm.send',        'uses' => 'Api\AuthController@sendConfirm']);
get('api/confirm/{app_id}/{token}',  ['as' => 'api.confirm_email',       'uses' => 'Api\AuthController@confirmEmail']);
post('api/login',                    ['as' => 'api.login',               'uses' => 'Api\AuthController@login']);
post('api/fogot_password/send',      ['as' => 'api.fogot_password.send', 'uses' => 'Api\AuthController@sendPassword']);
get('api/password/{app_id}/{token}/{type}', ['as' => 'api.reset_password',      'uses' => 'Api\AuthController@resetPassword']);
post('api/password/reset/{token}/{type}',   ['as' => 'api.reset_password.post', 'uses' => 'Api\AuthController@postResetPassword']);
post('api/update_profile',           ['as' => 'api.update_profile',      'uses' => 'Api\ApiController@updateProfile']);
post('api/change_password',          ['as' => 'api.change_password',     'uses' => 'Api\AuthController@changePassword']);
post('api/logout',                   ['as' => 'api.logout',              'uses' => 'Api\AuthController@logout']);

post('api/pt',                       ['as' => 'api.pt',                  'uses' => 'Api\ApiController@pt']);
post('api/setting',                  ['as' => 'api.setting',             'uses' => 'Api\ApiController@setting']);
post('api/submit_setting',           ['as' => 'api.submit_setting',      'uses' => 'Api\ApiController@submitSetting']);
post('api/create_setting',           ['as' => 'api.create_setting',      'uses' => 'Api\ApiController@create_setting']);
post('api/update_setting',           ['as' => 'api.update_setting',      'uses' => 'Api\ApiController@update_setting']);
post('api/check_device',             ['as' => 'api.check_device',        'uses' => 'Api\ApiController@checkDevice']);
post('api/pair_device',              ['as' => 'api.pair_device',         'uses' => 'Api\ApiController@pairDevice']);
post('api/unpair_device',            ['as' => 'api.unpair_device',       'uses' => 'Api\ApiController@unpairDevice']);
post('api/share_device',             ['as' => 'api.share_device',        'uses' => 'Api\ApiController@shareDevice']);
post('api/unshare_device',           ['as' => 'api.unshare_device',      'uses' => 'Api\ApiController@unshareDevice']);
post('api/accept_device',            ['as' => 'api.accept_device',       'uses' => 'Api\ApiController@acceptDevice']);
post('api/feedback',                 ['as' => 'api.feedback',            'uses' => 'Api\ApiController@feedback']);

get('api/v2/poster',                    ['as' => 'api.poster2',              'uses' => 'HomeController@poster2']);
post('api/v2/setting',                  ['as' => 'api2.setting',             'uses' => 'Api\Api2Controller@setting']);
post('api/v2/create_setting',           ['as' => 'api2.create_setting',      'uses' => 'Api\Api2Controller@create_setting']);
post('api/v2/update_setting',           ['as' => 'api2.update_setting',      'uses' => 'Api\Api2Controller@update_setting']);
post('api/v2/pair_device',              ['as' => 'api2.pair_device',         'uses' => 'Api\Api2Controller@pairDevice']);
post('api/v2/unpair_device',            ['as' => 'api2.unpair_device',       'uses' => 'Api\Api2Controller@unpairDevice']);
post('api/v2/share_setting',             ['as' => 'api2.share_setting',        'uses' => 'Api\Api2Controller@shareSetting']);
post('api/v2/unshare_setting',           ['as' => 'api2.unshare_setting',      'uses' => 'Api\Api2Controller@unshareSetting']);
post('api/v2/accept_setting',            ['as' => 'api2.accept_setting',       'uses' => 'Api\Api2Controller@acceptSetting']);
post('api/v2/make_admin',            	['as' => 'api2.make_admin',       	'uses' => 'Api\Api2Controller@make_admin']);
post('api/v2/unmake_admin',            	['as' => 'api2.unmake_admin',      	 'uses' => 'Api\Api2Controller@unmake_admin']);
post('api/v2/leave_setting',           	['as' => 'api2.leave_setting',     	 'uses' => 'Api\Api2Controller@leave_setting']);
post('api/v2/pending_setting',          ['as' => 'api2.pending_setting',       'uses' => 'Api\Api2Controller@pending_setting']);
post('api/v2/pairing_logs',          	['as' => 'api2.pairing_logs',       'uses' => 'Api\Api2Controller@pairing_logs']);
post('api/v2/poc',          	['as' => 'api2.poc',       'uses' => 'Api\Api3Controller@poc']);
post('api/v2/poc_issue_requests',          	['as' => 'api2.poc_issue_requests',       'uses' => 'Api\Api3Controller@poc_issue_requests']);
post('api/v2/poc_followup_requests',          	['as' => 'api2.poc_followup_requests',       'uses' => 'Api\Api3Controller@poc_followup_requests']);
post('api/v2/poc_device_check_requests',          	['as' => 'api2.poc_device_check_requests',       'uses' => 'Api\Api3Controller@poc_device_check_requests']);
post('api/v2/poc_device_complete_requests',          	['as' => 'api2.poc_device_complete_requests',       'uses' => 'Api\Api3Controller@poc_device_complete_requests']);
post('api/v2/poc_device_login',          	['as' => 'api2.poc_device_login',       'uses' => 'Api\AuthController@poc_device_login']);

get('ApiTesting',                    ['as' => 'api.ApiTesting',              'uses' => 'HomeController@ApiTesting']);

post('api/ApiTesting/device_check_requests',          	['as' => 'ApiTesting_device_check_requests',       'uses' => 'Api\Api3Controller@poc_device_check_requests']);

post('api/ApiTesting/device_complete_requests',          	['as' => 'ApiTesting_device_complete_requests',       'uses' => 'Api\Api3Controller@poc_device_complete_requests']);

post('api/ApiTesting/device_login',          	['as' => 'ApiTesting_device_login',       'uses' => 'Api\AuthController@poc_device_login']);


post('api/v2/power_consumption',          	['as' => 'api2.power_consumption',       'uses' => 'Api\Api2Controller@power_consumption']);

post('api/v2/power_consumption_get_webview',          	['as' => 'api2.power_consumption_get_webview',       'uses' => 'Api\Api2Controller@power_consumption_get_webview']);
post('api/v2/power_consumption_get_webview2',          	['as' => 'api2.power_consumption_get_webview2',       'uses' => 'Api\Api2Controller@power_consumption_get_webview2']);
post('api/v2/power_consumption_get_daily',          	['as' => 'api2.power_consumption_get_daily',       'uses' => 'Api\Api2Controller@power_consumption_get_daily']);
post('api/v2/power_consumption_get_weekly',          	['as' => 'api2.power_consumption_get_weekly',       'uses' => 'Api\Api2Controller@power_consumption_get_weekly']);
post('api/v2/power_consumption_get_monthly',          	['as' => 'api2.power_consumption_get_monthly',       'uses' => 'Api\Api2Controller@power_consumption_get_monthly']);
post('api/v2/power_consumption_get_yearly',          	['as' => 'api2.power_consumption_get_yearly',       'uses' => 'Api\Api2Controller@power_consumption_get_yearly']);

post('api/v2/device_audit_trail',          	['as' => 'api2.device_audit_trail',       'uses' => 'Api\Api2Controller@device_audit_trail']);
post('api/v2/device_audit_trail_get_report',          	['as' => 'api2.device_audit_trail_get_report',       'uses' => 'Api\Api2Controller@device_audit_trail_get_report']);
post('api/v2/firmware',          	['as' => 'api2.firmware',       'uses' => 'Api\Api2Controller@firmware']);


resource('users',                     'Cms\UserController');
resource('cms/inventory',             'Cms\DeviceController');
resource('cms/customer_id_lists',     'Cms\CustomerIdListController');
resource('cms/customer_sub_id_lists', 'Cms\CustomerSubIdListController');
resource('cms/device_type_lists',     'Cms\DeviceTypeListController');
resource('cms/ieee_lists',            'Cms\IeeeListController');	
get('cms/feedbacks', ['as' => 'cms.feedbacks.index', 'uses' => 'Cms\FeedbackController@index']);
get('cms/show/{id}/feedbacks', ['as' => 'cms.feedbacks.show', 'uses' => 'Cms\FeedbackController@show']);
get('cms/mobile_users', ['as' => 'cms.mobile_users.index', 'uses' => 'Cms\MobileUserController@index']);

get('testmail',function(){
	Mail::queue('emails.testmail', array(), function ($message) {
            $message->to('chuawiki@gmail.com', 'wikichua')->subject('nodus-cms');
        });
});