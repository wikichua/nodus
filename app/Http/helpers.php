<?php

function uploadMultipleImage($inputKey,$directory='')
{
    $uploaded = array();
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $Files = Input::file($inputKey);
        $i = 0;
        foreach ($Files as $File) {
            $originName = $File->getClientOriginalName();
            $originExtension = $File->getClientOriginalExtension();
            $fileName = $inputKey.'_'.time() . '_'.(++$i).'.' . $originExtension;
            Image::make($File)->save($destinationPath.'/'.$fileName);
            chmod($destinationPath.'/'.$fileName, 0777);
            
            $uploaded[] = $time .'/'. $fileName;
        }
    }
    
    return $uploaded;
}

function uploadImage($inputKey,$existFile='',$directory='')
{
    
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = Input::file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'.time() . '.' . $originExtension;
        Image::make($File)->save($destinationPath.'/'.$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }
        return $time .'/'. $fileName;
    }else
       return $existFile;
}

function removeUploadFile($file)
{
    $uploads_path = public_path().'/uploads';
    if($file != '' && file_exists($uploads_path .'/'.$file))
    {
        chmod($uploads_path .'/'.$file, 0777);
        return unlink($uploads_path .'/'.$file);
    }
    return;
}

function uploadFile($inputKey,$existFile='',$directory='')
{
    if (Input::hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = Input::file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'.time() . '.' . $originExtension;
        $result = $File->move($destinationPath,$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }

        return $time .'/'. $fileName;
    }

    return $existFile;
}

function imgTagShow($filename,$type='default')
{
    $nopic = array(
            'default'=>'../default.png',
            'logo'=>'../images/logo-big-black.png',
        );
    $pic = $nopic[strtolower($type)];
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
        $pic = $filename;

    return $pic;
}

function sortTableHeaderSnippet($field='',$fieldname = '')
{
    $fullUrla = $fullUrld = isset(parse_url(Request::fullUrl())['query'])? parse_url(Request::fullUrl())['query']:'';
    parse_str($fullUrla, $fullUrla);
    parse_str($fullUrld, $fullUrld);
    unset($fullUrld['a'],$fullUrla['d']);

    $fullUrla['a'] = $fieldname; 
    $fullUrla = http_build_query($fullUrla);
    $fullUrld['d'] = $fieldname; 
    $fullUrld = http_build_query($fullUrld);
    return $field.' <div class="btn-group">
            <a href="'.Request::url().'?'.$fullUrla.'" style="position:absolute;top:-12px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="glyphicon glyphicon-chevron-up icon-sort"></i></a>
            <a href="'.Request::url().'?'.$fullUrld.'" style="position:absolute;top:-3px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="glyphicon glyphicon-chevron-down icon-sort"></i></a>
            </div>';
}

function searchTableHeaderSnippet($field='')
{

    return Form::text('s-'.$field, Input::get('s-'.$field), array('class'=>"input-sm form-control"));
}

function isValidDateTimeString($str_dt, $str_dateformat, $str_timezone) {
    $date = DateTime::createFromFormat($str_dateformat, $str_dt, new DateTimeZone($str_timezone));
    return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;
}