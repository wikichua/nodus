<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Middleware\GetUserFromToken;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Http\Controllers\Api\ApiTrait;

class JwtAuth extends GetUserFromToken
{
    use ApiTrait;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $inputs = json_decode($request->all()['data'], true);

        if (!$token = isset($inputs['token']) ? $inputs['token'] : '') {
            return $this->returnData([], 'auth', 'error', 'Token not provided',$no_app_data = true);
        }

        try {
            $User = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return $this->returnData([], 'auth', 'error', 'Token expired',$no_app_data = true);
        } catch (JWTException $e) {
            return $this->returnData([], 'auth', 'error', 'Login expired. Please logout and login again.',$no_app_data = true); // originally Token Invalid
        }

        if (!$User) {
            return $this->returnData([], 'auth', 'error', 'User not found',$no_app_data = true);
        }

        $this->events->fire('tymon.jwt.valid', $User);

        return $next($request);
    }
}
