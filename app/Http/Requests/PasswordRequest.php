<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'confirmed|required',
            'old_password'=>'required|check_password',
        ];
    }

    public function messages()
    {
        return array(
            'password.required' => 'confirmed|',
            'old_password.check_password'=>'Current Password Not Match',
            );
    }
}
