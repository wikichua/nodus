<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DeviceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required',
            'customer_id'               => 'required',
            'device_type'               => 'required',
            'customer_device_serial_id' => 'required|integer|min:0|max:65535',
        ];
    }
}
