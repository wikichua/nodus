<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerSubIdListRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                => 'required',
            'customer_id_list_id' => 'required',
            'ieee_list_id'        => 'required',
            'code'                => 'required|integer|min:0|max:255',
        ];
    }
}
