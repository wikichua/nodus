<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\CustomerIdList;
use Mail;
use App\User;
use Carbon\Carbon;
use DB;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/';
    protected $subject = 'Nodus - Change Password Link';

    /**
     * (Override the original vendor file)
     * Create a new password controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $app_id='';
        if($request->get('App_id')==''){
             $app_id='0';
        }else{
            $app_id=$request->get('App_id');
        }
        $User = User::where('email', $request->get('email'))->where('role','!=','mobile')->where('app_id',  $app_id)->first();
        if (!$User) {
            return redirect()->back()->withErrors(['email' => trans('Email not Exists')]);
        }

        $token = hash_hmac('sha256', str_random(40), config('app.key'));

        $confirm_datas = [
            'email'      => $User->email,
            'token'      => $token,
            'created_at' => Carbon::now(),
            'app_id'     =>  $app_id
        ];

        DB::table('password_resets')->insert($confirm_datas);

        $CustomerIdList = CustomerIdList::find( $app_id);
        $background_color = '#71bc37';
        $image_link = url('images/logo-big-black.png');
        if( isset($CustomerIdList)){
            if ($CustomerIdList->background_color && $CustomerIdList->background_color != '') {
                $background_color = $CustomerIdList->background_color;
            }
            if ($CustomerIdList->app_icon && $CustomerIdList->app_icon != '') {
                $image_link = url('/uploads/' . $CustomerIdList->app_icon);
            }
        }
        $datas = [
            'token' => $token,
            'user_name' => $User->name,
            'app_id' =>  $app_id,
            'link' => route('api.reset_password', [ $app_id, $token,'web']),
            'background_color' => $background_color,
            'image_link' => $image_link
        ];
        if( isset($CustomerIdList)){
            Mail::send('emails.mobile_password', $datas, function ($message) use ($User, $CustomerIdList) {
                $CustomerIdList_email = isset($CustomerIdList->email)?$CustomerIdList->email:'support@convep.com';
                $message->from($CustomerIdList_email, $CustomerIdList->name);
                $message->to($User->email, $User->name)->subject($CustomerIdList->name . ' - Reset Password');
            });
        }else{
            Mail::send('emails.mobile_password', $datas, function ($message) use ($User) {
                $CustomerIdList_email ='support@convep.com';
                $message->from($CustomerIdList_email, 'Admin');
                $message->to($User->email, $User->name)->subject('Admin - Reset Password');
            });
        }
         
          return redirect()->back()->with('status', trans('sent'));

        
        
    }

    /**
     * (Override the original vendor file)
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $credentials['app_id'] = null;

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect($this->redirectPath())->with('status', trans($response));

            default:
                return redirect()->back()
                            ->withInput($request->only('email'))
                            ->withErrors(['email' => trans($response)]);
        }
    }

    public function getEmail()
    {
        $Appid = CustomerIdList::all();

        foreach ($Appid as $key => $appid) {
            $app_id[$appid->id] = $appid->name;
        }
        return view('auth.password', compact('app_id'));
    }
}
