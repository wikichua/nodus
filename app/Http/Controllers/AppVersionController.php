<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AppVersion;

class AppVersionController extends Controller
{

    private $OSs = array('IOS'=>'IOS','APK'=>'APK');
    private $types = array('Optional'=>'Optional','Critical'=>'Critical');
    private $statuses = array('Active'=>'Active','Inactive'=>'Inactive');

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $AppVersions = AppVersion::orderBy('version','desc')->paginate(25);
        return view('app_version.index')->with(compact('AppVersions'));
    }

    public function create()
    {
        $OSs = $this->OSs;
        $types = $this->types;
        $statuses = $this->statuses;
        return view('app_version.create')->with(compact('OSs','types','statuses'));
    }

    public function store(Request $request)
    {
        if($request->get('type') == 'Active')
        {
            AppVersion::where('os',$request->get('os'))->where('type',$request->get('type'))->update(array('status' => 'Inactive'));
        }
        $AppVersion = AppVersion::create(
                array(
                        'os' => $request->get('os'),
                        'version' => $request->get('version'),
                        'type' => $request->get('type'),
                        'status' => $request->get('status'),
                        'description' => $request->get('description'),
                    )
            );

        return redirect()->route('app.version')->with('success','Record created.');
    }

    public function edit($id)
    {
        $AppVersion = AppVersion::find($id);
        $OSs = $this->OSs;
        $types = $this->types;
        $statuses = $this->statuses;
        return view('app_version.edit')->with(compact('AppVersion','OSs','types','statuses'));
    }

    public function update(Request $request, $id)
    {
        if($request->get('type') == 'Active')
        {
            AppVersion::where('os',$request->get('os'))->where('type',$request->get('type'))->update(array('status' => 'Inactive'));
        }
        $AppVersion = AppVersion::find($id);
        $AppVersion->os = $request->get('os',$AppVersion->os);
        $AppVersion->version = $request->get('version',$AppVersion->version);
        $AppVersion->type = $request->get('type',$AppVersion->type);
        $AppVersion->status = $request->get('status',$AppVersion->status);
        $AppVersion->description = $request->get('description',$AppVersion->description);
        $AppVersion->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
         AppVersion::destroy($id);

        return redirect()->route('app.version');
        
    }
}