<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth,Redirect;
use Mail;
use DB;
use Carbon\Carbon;
use App\User;
use App\CustomerIdList;
use App\Setting;
use App\Lib\HouseholdIdGenerator\HouseholdIdGenerator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\MobileDevice;

class AuthController extends Controller
{
    use ApiTrait;
 
    public function __construct(Request $request)
    {
        // $this->log_mobile_request(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'', $request->all());
    }

    private function compileUsersInSetting(&$Setting)
    {
        $user_ids = $Setting->user_ids;
        unset($Setting->user_ids,$ids);
        $user_id_keys = array_keys($user_ids);
        foreach ($user_ids as $id => $status) {
            $ids[] = array(
                'id'=>$id,
                'name'=>isset(User::find($id)->name)?User::find($id)->name:'',
                'email'=>isset(User::find($id)->email)?User::find($id)->email:'',
                'status'=>$status
                );
        }
        $Setting->user_ids = $ids;
        
        $admin_ids = $Setting->admin_ids;
        unset($Setting->admin_ids,$ids);
        $Users = User::whereIn('id',$admin_ids)->get();
        foreach ($admin_ids as $id) {
            $ids[] = array(
                'id'=>$id,
                'name'=>User::find($id)->name,
                'email'=>User::find($id)->email,
                );
        }
        $Setting->admin_ids = $ids;
    }
    private function compileSetting(&$Settings)
    {
        foreach ($Settings as $Setting) {
            if(isset($Setting->user_ids)){
                $this->compileUsersInSetting($Setting);
            }
        }
    }
    public function register(Request $request, HouseholdIdGenerator $HouseholdIdGenerator)
    {
        $inputs = json_decode($request->all()['data'], true);

        $validator = \Validator::make($inputs, [
            'name'     => 'required',
            'email'    => 'required|email',
            'password' => 'required',
            'app_id'   => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnData([], __FUNCTION__, 'error', $validator->errors()->all());
        }

        $ExistingUser = User::where('email', $inputs['email'])->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first();

        if ($ExistingUser) {
            return $this->returnData([], __FUNCTION__, 'error', 'Email has already been registered.');
        }

        try {
            $inputs['dob'] = isset($inputs['dob'])?$inputs['dob']:'1970-01-01';
            $inputs['dob'] = date('Y-m-d', strtotime($inputs['dob']));
        } catch (\Exception $e) {
            return $this->returnData([], __FUNCTION__, 'error', 'Error input Date of Birth');
        }

        try {
            // $household_id = $HouseholdIdGenerator->generate($inputs['email']);
            $User = User::create([
                'name'         => $inputs['name'],
                'email'        => $inputs['email'],
                'password'     => bcrypt($inputs['password']),
                'role'         => 'mobile',
                // 'household_id' => $household_id,
                'app_id'       => $inputs['app_id'],
                'gender'       => isset($inputs['gender'])?$inputs['gender']:'',
                'dob'          => $inputs['dob'],
            ]);
            $datas[] = $User->toArray();
        } catch (\Exception $e) {
            return $this->returnData([], __FUNCTION__, 'error', 'Error registering');
        }

        $this->sendConfirm($request);

        return $this->returnData($datas, __FUNCTION__);
    }

    /**
     * Send email for user to confirm registration with their email
     * @param  mixed $User
     */
    public function sendConfirm(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);

        $User = User::where('email', $inputs['email'])->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first();

        if (!$User) {
            return $this->returnData([], __FUNCTION__, 'error', 'Invalid email');
        }

        $token = hash_hmac('sha256', str_random(40), config('app.key'));

        $confirm_datas = [
            'email'      => $User->email,
            'token'      => $token,
            'created_at' => Carbon::now(),
            'app_id'     => $inputs['app_id']
        ];

        DB::table('password_resets')->insert($confirm_datas);

        $CustomerIdList = CustomerIdList::find($inputs['app_id']);
        $background_color = '#71BC37';
        $image_link = url('images/logo-big-black.png');
        if ($CustomerIdList->background_color && $CustomerIdList->background_color != '') {
            $background_color = $CustomerIdList->background_color;
        }
        if ($CustomerIdList->app_icon && $CustomerIdList->app_icon != '') {
            $image_link = asset('uploads/'.imgTagShow($CustomerIdList->app_icon));
        }

        $datas = [
            'token' => $token,
            'user_name' => $User->name,
            'app_id' => $inputs['app_id'],
            'link' => route('api.confirm_email', [$inputs['app_id'], $token]),
            'background_color' => $background_color,
            'image_link' => $image_link
        ];

        Mail::send('emails.confirm', $datas, function ($message) use ($User, $CustomerIdList) {
            if (!isset($CustomerIdList->email) || $CustomerIdList->email == '') {
                $CustomerIdList->email = 'support@convep.com';
            }
            $message->from($CustomerIdList->email, $CustomerIdList->name);
            $message->to($User->email, $User->name)->subject($CustomerIdList->name . ' - Activate Registration');
        });

        return $this->returnData([], __FUNCTION__);
    }

    public function confirmEmail($app_id, $token)
    {
        // delete expired token
        $expire = config('auth.password.expire', 60);
        DB::table('password_resets')->where('created_at', '<', Carbon::now()->subMinutes($expire))->delete();

        if (!isset($token) || !isset($app_id)) {
            return redirect()->back()->with('error-message', 'Invalid or expired token');
        }

        $email = DB::table('password_resets')->where('token', $token)->where('app_id', $app_id)->orderBy('created_at', 'desc')->first()->email;

        if (!$email) {
            return redirect()->back()->with('error-message', 'Invalid or expired token');
        }

        $User = User::where('email', $email)->where('role', 'mobile')->where('app_id', $app_id)->first();
        $User->confirmed = 1;
        $User->confirmed_at = \Carbon\Carbon::now();
        $User->save();
        $CustomerIdList = CustomerIdList::find($app_id);


        if($CustomerIdList->website!=""){
            return Redirect::to($CustomerIdList->website);
        }
        return view('api.confirm_success',compact('CustomerIdList'));
    }

    public function login(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);

        $User = User::where('email', trim($inputs['email']))->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first();

        if (!$User) {
            return $this->returnData([], __FUNCTION__, 'error', 'User not yet register');
        }

        if ($User->confirmed == 0) {
            return $this->returnData([], __FUNCTION__, 'error', 'Please validate your email that have been send to your email account');
        }

        $credentials = ['email' => trim($inputs['email']), 'password' => $inputs['password'], 'role' => 'mobile', 'app_id' => $inputs['app_id'], 'confirmed' => 1];

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->returnData([], __FUNCTION__, 'error', 'Your login information is invalid. Please try with the correct information');
            }
        } catch (JWTException $e) {
            return $this->returnData([], __FUNCTION__, 'error', 'could_not_create_token');
        }
        
        // $settings = \App\Setting::where('user_ids','like' ,'%"'.$User->id.'":"Accepted"%')->orderBy('created_at', 'desc')->get()->toArray();
        $settings = \App\Setting::with('devices')->where('user_ids', 'like','%"'.$User->id.'":"Accepted"%')->orderBy('created_at', 'desc')->get();
         $this->compileSetting($settings);

        $datas = $User->toArray();
        $datas['token'] = $token;
        session()->put('api_token',$token);
        session()->put('app_id',$inputs['app_id']);
        $datas['settings'] = $settings;

        return $this->returnData($datas, __FUNCTION__);
    }

    public function poc_device_login(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = User::where('email', trim($inputs['email']))->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first();

        if (!$User) {
            return $this->returnData([], __FUNCTION__, 'error', 'User not yet register',$no_app_data = true);
        }

        if ($User->confirmed == 0) {
            return $this->returnData([], __FUNCTION__, 'error', 'Please validate your email that have been send to your email account',$no_app_data = true);
        }

        $credentials = ['email' => trim($inputs['email']), 'password' => $inputs['password'], 'role' => 'mobile', 'app_id' => $inputs['app_id'], 'confirmed' => 1];

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->returnData([], __FUNCTION__, 'error', 'Your login information is invalid. Please try with the correct information');
            }
        } catch (JWTException $e) {
            return $this->returnData([], __FUNCTION__, 'error', 'could_not_create_token',$no_app_data = true);
        }
        
        // $settings = \App\Setting::where('user_ids','like' ,'%"'.$User->id.'":"Accepted"%')->orderBy('created_at', 'desc')->get()->toArray();
        $setting_ids = Setting::where('user_ids', 'like','%"'.$User->id.'":"Accepted"%')->where('app_id',$inputs['app_id'])->lists('id')->toArray();

       
        $datas['token'] = $token;
        session()->put('apitesting_token',$token);
        session()->put('apitesting_app_id',$inputs['app_id']);
       
        return $this->returnData($datas, __FUNCTION__,'success','',$no_app_data = true);
    }


    public function sendPassword(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);

        $User = User::where('email', $inputs['email'])->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first();

        if (!$User) {
            return $this->returnData([], __FUNCTION__, 'error', 'Invalid email');
        }

        $token = hash_hmac('sha256', str_random(40), config('app.key'));

        $confirm_datas = [
            'email'      => $User->email,
            'token'      => $token,
            'created_at' => Carbon::now(),
            'app_id'     => $inputs['app_id']
        ];

        DB::table('password_resets')->insert($confirm_datas);

        $CustomerIdList = CustomerIdList::find($inputs['app_id']);
        $background_color = '#71bc37';
        $image_link = url('images/logo-big-black.png');
        if ($CustomerIdList->background_color && $CustomerIdList->background_color != '') {
            $background_color = $CustomerIdList->background_color;
        }
        if ($CustomerIdList->app_icon && $CustomerIdList->app_icon != '') {
            $image_link = url('/uploads/' . $CustomerIdList->app_icon);
        }

        $datas = [
            'token' => $token,
            'user_name' => $User->name,
            'app_id' => $inputs['app_id'],
            'link' => route('api.reset_password', [$inputs['app_id'], $token,'mobile']),
            'background_color' => $background_color,
            'image_link' => $image_link
        ];

        Mail::queue('emails.mobile_password', $datas, function ($message) use ($User, $CustomerIdList) {
            if (!isset($CustomerIdList->email) || $CustomerIdList->email == '') {
                $CustomerIdList->email = 'support@convep.com';
            }
            $message->from($CustomerIdList->email, $CustomerIdList->name);
            $message->to($User->email, $User->name)->subject($CustomerIdList->name . ' - Reset Password');
        });

        return $this->returnData([], __FUNCTION__);
    }

    public function resetPassword($app_id, $token,$type)
    {
         $CustomerIdList = CustomerIdList::find($app_id);
        return view('api.reset_password', compact('app_id', 'token','type','CustomerIdList'));
    }

    public function postResetPassword(Request $request, $token,$type)
    {
        $expire = config('auth.password.expire', 60);
        DB::table('password_resets')->where('created_at', '<', Carbon::now()->subMinutes($expire))->delete();
         $CustomerIdList = CustomerIdList::find($request->app_id);

        if (!isset($request->token) || !isset($request->app_id)) {
            return redirect()->back()->with('error-message', 'Invalid or expired token, please click again forgot password');
        }
        $Reset = DB::table('password_resets')->where('token', $request->token)->where('app_id', $request->app_id)->orderBy('created_at', 'desc')->first();
        if (!$Reset) {
            return redirect()->back()->with('error-message', 'Invalid or expired token, please click again fogot password');
        }
        $email = $Reset->email;
        if($type=='mobile'){
            $User = User::where('email',$email)->where('role','mobile')->where('app_id',$request->app_id)->first();
        }else{
            $User = User::where('email',$email)->where('role','!=','mobile')->where('app_id',$request->app_id)->first();
        }
        $User->password =bcrypt($request->new_password);
        $User->save();

        DB::table('password_resets')->where('token', $request->token)->delete();

        if($CustomerIdList && $CustomerIdList->website!=""){
            return Redirect::to($CustomerIdList->website);
        }
        return view('api.reset_password_success',compact('CustomerIdList'));
    }

    public function changePassword(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);

        if (Auth::once(['email' => $inputs['email'],'app_id' => $inputs['app_id'],'password'=>$inputs['password'] ])) {
            $User = auth()->user();
            $User->password = bcrypt($inputs['new_password']);
            $User->save();

            return $this->returnData([], __FUNCTION__);
        } else {
            return $this->returnData([], __FUNCTION__, 'error', 'Your old password is invalid. Please try with the correct information');
        }
    }

    public function logout(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        // delete the user's push notification
        User::where('email', $inputs['email'])->where('role', 'mobile')->where('app_id', $inputs['app_id'])->first()->mobileDevices()->delete();

        return $this->returnData([], __FUNCTION__);
    }
}
