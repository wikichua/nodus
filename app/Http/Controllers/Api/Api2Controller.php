<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Setting;
use App\Device;
use App\User;
use App\Temperatures;
use App\PairingLogs;
use App\Sector;
use App\MobileDevice;
use App\PnLog;
use App\Lib\HouseholdIdGenerator\HouseholdIdGenerator;
use App\Lib\PushNotification\PushNotification;
use App\CustomerIdList;
use App\PowerConsumption;
use App\DeviceAuditTrail;
use App\Firmware;
use Carbon\Carbon;
use URL;


class Api2Controller extends ApiController
{
    use ApiTrait;
    use JwtAuthTrait;

    private function compileUsersInSetting(&$Setting)
    {
        $user_ids = $Setting->user_ids;
        unset($Setting->user_ids,$ids);
        $from =array();
        $from = json_decode($Setting->share_from,true);
        $user_id_keys = array_keys($user_ids);
        foreach ($user_ids as $id => $status) {
            $ids[] = array(
                'id'=>$id,
                'name'=>isset(User::find($id)->name)?User::find($id)->name:'',
                'email'=>isset(User::find($id)->email)?User::find($id)->email:'',
                'status'=>$status,
                'share_from'=> isset($from[$id])?$from[$id] :''
               
                );
        }
        $Setting->user_ids = $ids;
        $admin_ids = $Setting->admin_ids;
        unset($Setting->admin_ids,$ids);
        $Users = User::whereIn('id',$admin_ids)->get();
        foreach ($admin_ids as $id) {
            $ids[] = array(
                'id'=>$id,
                'name'=>User::find($id)->name,
                'email'=>User::find($id)->email,
                );
        }
        $Setting->admin_ids = $ids;
    }
    private function compileSetting(&$Settings)
    {
        foreach ($Settings as $Setting) {
            if(isset($Setting->user_ids)){
                $this->compileUsersInSetting($Setting);
            }
        }
    }

    public function setting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Settings = Setting::with('devices')->where('user_ids', 'like','%"'.$User->id.'":"Accepted"%')->where('app_id',isset($inputs['app_id'])? $inputs['app_id']:0)->orderBy('created_at', 'desc')->get();
        $this->compileSetting($Settings);
        $data['settings'] = $Settings;
        return $this->returnData($data, __FUNCTION__);
    }

    public function create_setting(Request $request, HouseholdIdGenerator $HouseholdIdGenerator)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $household_ids = Setting::lists('household_id','household_id')->toArray();
        $household_id = $HouseholdIdGenerator->generate($User->email);
        while(in_array($household_id, $household_ids)){
            $household_id = $HouseholdIdGenerator->generate($User->email);
        }
        $Setting = Setting::create([
            'user_ids'=> array($User->id => 'Accepted'),
            'admin_ids'=> array($User->id),
            'zone' => isset($inputs['zone'])? $inputs['zone']:'',
            'scene' => isset($inputs['scene'])? $inputs['scene']:'',
            'timer' =>  isset($inputs['timer'])? $inputs['timer']:'',
            'manual_light' =>  isset($inputs['manual_light'])? $inputs['manual_light']:'',
            'setting' => isset($inputs['setting'])? $inputs['setting']:'',
            'location' => isset($inputs['location'])? $inputs['location']:'',
            'location_type' => isset($inputs['location_type'])? $inputs['location_type']:'',
            'app_id' => $inputs['app_id'],
            'sense' => isset($inputs['sense'])? $inputs['sense']:'',
            'household_id' => $household_id,
        ]);
        return $this->setting($request);
    }

    public function update_setting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Setting = Setting::find($inputs['setting_id']);
        if(in_array($User->id,$Setting->admin_ids)){
            $Setting->zone = isset($inputs['zone']) ? $inputs['zone']:$Setting->zone;
            $Setting->scene = isset($inputs['scene']) ? $inputs['scene']:$Setting->scene;
            $Setting->timer = isset($inputs['timer']) ? $inputs['timer']:$Setting->timer;
            $Setting->manual_light = isset($inputs['manual_light']) ? $inputs['manual_light']:$Setting->manual_light;
            $Setting->setting = isset($inputs['setting']) ? $inputs['setting']:$Setting->setting;
            $Setting->location = isset($inputs['location']) ? $inputs['location']:$Setting->location;
            $Setting->location_type =  isset($inputs['location_type']) ? $inputs['location_type']:$Setting->location_type;
            $Setting->sense = isset($inputs['sense']) ? $inputs['sense']:$Setting->location;
            
            $Setting->save();
        }else{
              return $this->returnData([], __FUNCTION__, 'error', 'You can\'t edit this Setting');
        }
        return $this->setting($request);
    }

    public function pairDevice(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Devices = $this->getAuthDevice($request);
        $inputs = json_decode($request->all()['data'], true);

        // if (!$Devices) {
            
        //     return $this->returnData([], __FUNCTION__, 'error', ['Device not found']);
        // }
        foreach ($Devices as $Device) {
            $gps = $Device->gps;
            $lat = trim($inputs['lat']) != ''? $inputs['lat']:$gps->lat;
            $long = trim($inputs['long']) != ''? $inputs['long']:$gps->long;
            $Device->gps = json_encode(['lat' => $lat, 'long' => $long]);
            $Device->setting_id = trim($inputs['setting_id']);
            $Device->save();
            $Setting = $Device->setting()->first();
            $this->compileUsersInSetting($Setting);
            $Device->setting = $Setting;
            $devices[] = $Device;
        }

        return $this->returnData($devices, __FUNCTION__);
    }

    public function unpairDevice(Request $request)
    {
        $Devices = $this->getAuthDevice($request);
        if (!$Devices) {
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        foreach ($Devices as $Device) {
            $gps = $Device->gps;
            $Device->gps = json_encode(['lat' => '', 'long' => '']);
            $Device->setting_id = 0;
            $Device->save();
            $devices[] = $Device->toArray();
        }
        return $this->returnData($devices, __FUNCTION__);
    }

    public function shareSetting(Request $request, PushNotification $PushNotification, PnLog $PnLog)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $User_to_share_with = User::where('email', $inputs['user2_email'])
            ->where('role', 'mobile')
            ->where('app_id', $inputs['app_id'])->first();
        if (!$User_to_share_with) {
            return $this->returnData([], __FUNCTION__, 'error', 'User\'s email not found');
        }
        $Setting = Setting::find($inputs['setting_id']);
        if(isset($Setting)){
            if(in_array($User->id,$Setting->admin_ids)){
                if(!in_array($User_to_share_with->id, array_flip($Setting->user_ids))){
                    $user_ids = $Setting->user_ids;
                    $user_ids[$User_to_share_with->id] = 'Pending';
                    $Setting->user_ids = $user_ids;
                    $record = json_decode($Setting->share_from,true);
                    $record[$User_to_share_with->id] = $User->email;
                    $Setting->share_from =json_encode($record);
                    $Setting->save();
                }else{
                    return $this->returnData([], __FUNCTION__, 'error', 'User already Shared ');
                }
            }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Setting Not Found');
            }
        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'Setting Not Exists');
        }

        $User_to_shared_mobile_devices = $User_to_share_with->mobileDevices()->get()->toArray();

        foreach ($User_to_shared_mobile_devices as $User_to_shared_mobile_device) {
            if(trim($User_to_shared_mobile_device['push_token']) != '')
            {
                $token = $User_to_shared_mobile_device['push_token'];
                $message = "$User->name share a $Setting->location with you";
                $pn_response = $PushNotification->send($token, $message);
            }
        }
        return $this->setting($request);
    }

    public function unshareSetting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User_shared_with = User::where('email', $inputs['user2_email'])
            ->where('role', 'mobile')
            ->where('app_id', $inputs['app_id'])->first();
        $Setting = Setting::find($inputs['setting_id']);
        $user_ids = $Setting->user_ids;
        $admin_ids = $Setting->admin_ids;
        $share_from = json_decode($Setting->share_from,true);
         if(isset($user_ids[$User_shared_with->id])){
            unset($user_ids[$User_shared_with->id]);
            $Setting->user_ids = $user_ids;
         }
         if(isset($share_from[$User_shared_with->id])){
            unset($share_from[$User_shared_with->id]);
            $Setting->share_from = json_encode($share_from);
         }
         if(in_array($User_shared_with->id,$Setting->admin_ids)){
            $admin_ids = array_flip($admin_ids);
            unset($admin_ids[$User_shared_with->id]);
            $admin_ids = array_flip($admin_ids);
            $Setting->admin_ids = array_unique($admin_ids);
         }
        $Setting->save();
        return $this->setting($request);
    }

    public function pending_setting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Settings = Setting::with('devices')->where('user_ids', 'like','%"'.$User->id.'":"Pending"%')->where('app_id',isset($inputs['app_id'])? $inputs['app_id']:0)->orderBy('created_at', 'desc')->get();
        $this->compileSetting($Settings);
        $data['settings'] = $Settings;
        return $this->returnData($data, __FUNCTION__);
    }

    public function acceptSetting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Setting = Setting::find($inputs['setting_id']);
         if(in_array($User->id,array_flip($Setting->user_ids)))
         {
            $user_ids = $Setting->user_ids;
            $user_ids[$User->id] = 'Accepted';
            $Setting->user_ids = $user_ids;
            $Setting->save();
            return $this->setting($request);
        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'Setting not found');
        }
    }

    public function make_admin(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $User_to_be_admin = User::where('email', $inputs['user2_email'])
            ->where('role', 'mobile')
            ->where('app_id', $inputs['app_id'])->first();

        if (!$User_to_be_admin) {
            return $this->returnData([], __FUNCTION__, 'error', 'User\'s email not found');
        }

        $Setting = Setting::find($inputs['setting_id']);
        $user_ids = $Setting->user_ids;
        if(isset($user_ids[$User_to_be_admin->id]) && $user_ids[$User_to_be_admin->id] == 'Accepted')
        {
            $admin_ids = $Setting->admin_ids;
            $admin_ids[] = $User_to_be_admin->id;
            $Setting->admin_ids = array_unique($admin_ids);
            $Setting->save();
        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'User is not invited or has not accept the invitation');
        }
       return $this->setting($request);
    }

    public function unmake_admin(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Admin_to_be_removed = User::where('email', $inputs['user2_email'])
            ->where('role', 'mobile')
            ->where('app_id', $inputs['app_id'])->first();

        if (!$Admin_to_be_removed) {
            return $this->returnData([], __FUNCTION__, 'error', 'User\'s email not found');
        }
        $Setting = Setting::find($inputs['setting_id']);
        $user_ids = $Setting->user_ids;
        if(isset($user_ids[$Admin_to_be_removed->id]) && $user_ids[$Admin_to_be_removed->id] == 'Accepted')
        {
            $admin_ids = $Setting->admin_ids;
            if(in_array($Admin_to_be_removed->id,$admin_ids))
            {
                $admin_ids = array_flip($admin_ids);
                unset($admin_ids[$Admin_to_be_removed->id]);
                $admin_ids = array_flip($admin_ids);
                $Setting->admin_ids = array_unique($admin_ids);
                $Setting->save();
            }else{
                return $this->returnData([], __FUNCTION__, 'error', 'User is not an admin');
            }
        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'User is not invited or has not accept the invitation');
        }
        $this->compileUsersInSetting($Setting);
        return $this->returnData($Setting, __FUNCTION__);
    }

    public function leave_setting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Setting = Setting::find($inputs['setting_id']);
        $user_ids = $Setting->user_ids;
        $admin_ids = $Setting->admin_ids;
        if(isset($user_ids[$User->id]) && $user_ids[$User->id] == 'Accepted')
        {
            unset($user_ids[$User->id]);
            $admin_ids = array_flip($admin_ids);
            unset($admin_ids[$User->id]);
            $admin_ids = array_flip($admin_ids);

            if(count($user_ids) == 0)
            {
                Device::where('setting_id',$Setting->id)->update(array('setting_id'=>''));
                $Setting->delete();
            }else{            
                if(count($admin_ids) == 0)
                {
                    $new_admin_id = array_keys($user_ids)[0];
                    $admin_ids[] = $new_admin_id;
                    $Setting->admin_ids = $admin_ids;
                }
                $Setting->admin_ids = $admin_ids;
                $Setting->user_ids = $user_ids;
                $Setting->save();               
            }  

        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'User is not invited or has not accept the invitation');
        }
        return $this->setting($request);
    }

    public function pairing_logs(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        for ($i = 0; $i < count($inputs['serial_num']); $i++) {
            $Pairinglogs = PairingLogs::updateOrCreate(['serial_num'=>$inputs['serial_num'][$i]],[
            'serial_num'=>$inputs['serial_num'][$i],
            'app_id'=>$inputs['app_id'],
            ]);
        }
        $data=array();
        return $this->returnData($data, __FUNCTION__);
    }

    public function power_consumption(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $data = array();
        $Ddata=array();
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        foreach ($inputs['device'] as $i => $device) {
            // if(Device::find($device['device_id'])){
            if($Device_id = Device::where('customer_device_serial_id',ltrim ($device["device_id"],"0"))->first()){
                $Device_id =$Device_id->id;
            }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            }

           if( ($Ddata=PowerConsumption::where('device_id',$Device_id)->where('batch_id',$device['batch_id'])->get())!=null ){
                foreach ($Ddata as $key => $data) {
                    if($data->setting_id!=$device['setting_id']){
                    $data->deleted_at=Carbon::now();
                    $data->save();
                    }
                }
           }
            PowerConsumption::updateOrCreate(['device_id'=>$Device_id,'setting_id'=>$device['setting_id'],'batch_id'=>$device['batch_id'],'time'=>$device['time']],
                    [
                        'device_id'=>$Device_id,
                        'setting_id'=>isset($device['setting_id'])?$device['setting_id']:"",
                        'batch_id'=>isset($device['batch_id'])?$device['batch_id']:"",
                        'power'=>isset($device['power'])?$device['power']/10:"",
                        'time'=>isset($device['time'])?$device['time']:"",
                    ]);  
            // }else{
            //      return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            // }
        }
        $data = array();
        return $this->returnData($data, __FUNCTION__);
    }  


    public function power_consumption_get_webview(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        $Device_id =$Device_id->id;
        // if($data=$PowerConsumptions=PowerConsumption::where('device_id', $Device_id)->where('setting_id',$inputs["setting_id"])->where('batch_id',$inputs["batch_id"])->first()==null){
        //     // if no data
        //     $data[]= route('webview',array($Device_id,$inputs["setting_id"],$inputs["batch_id"]))."?form_date=".Carbon::now()->format('Y-m-d')."&to_date=24";
        //     return$this->returnData($data, __FUNCTION__); 

        // }else{
        $data[]= route('webview',array($Device_id,$inputs["setting_id"],$inputs["batch_id"]))."?form_date=".Carbon::now()->format('Y-m-d')."&to_date=1";
            return$this->returnData($data, __FUNCTION__); 
        // }
    }  

        public function power_consumption_get_webview2(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        $Device_id =$Device_id->id;
        // if($data=$PowerConsumptions=PowerConsumption::where('device_id', $Device_id)->where('setting_id',$inputs["setting_id"])->where('batch_id',$inputs["batch_id"])->first()==null){
        //     // if no data
        //     $data[]= route('webview',array($Device_id,$inputs["setting_id"],$inputs["batch_id"]))."?form_date=".Carbon::now()->format('Y-m-d')."&to_date=24";
        //     return$this->returnData($data, __FUNCTION__); 

        // }else{
        $data[]= route('webview2',array($Device_id,$inputs["setting_id"],$inputs["batch_id"]))."?form_date=".Carbon::now()->format('Y-m-d')."&to_date=1";
            return$this->returnData($data, __FUNCTION__); 
        // }
    }  

    public function power_consumption_get_Daily(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $data=array();
        $x=array();
        $ad=0;
        $Device_id = 0;
        $Daily_Powerconsumption=array();

        if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){

            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        $Device_id =$Device_id->id;


        if($PowerConsumptions=PowerConsumption::where('device_id',$Device_id)->where('setting_id',$inputs["setting_id"])->get()){
             // dd($PowerConsumptions);
             foreach ($PowerConsumptions as $PowerConsumption) {
                if($PowerConsumption->time>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])&&$PowerConsumption->time<=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addDay()){
                        $Daily_Powerconsumption[]=$PowerConsumption;
                }
             }
             

             for ($i=0; $i<24 ; $i++) { 
                $ad=0;
                $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addHours($i)->toDateTimeString(),"power"=>$ad];

                if(isset($Daily_Powerconsumption)){
                    foreach ($Daily_Powerconsumption as $key => $Daily_powerconsumption) {
                        if(Carbon::createFromFormat('Y-m-d H:i:s',$Daily_powerconsumption->time)>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addHours($i)&&
                            Carbon::createFromFormat('Y-m-d H:i:s',$Daily_powerconsumption->time)<Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addHours($i+1))
                        {

                            $ad+=isset($Daily_powerconsumption)?$Daily_powerconsumption->power:'';
                            $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addHours($i)->toDateTimeString(),"power"=>$ad];
                        }
                    }
                }
             
             }
             
            return $this->returnData($data, __FUNCTION__);

        }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            }
    } 

    public function power_consumption_get_Weekly(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        $data=array();
                if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){

                    return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
                }
        $Device_id =$Device_id->id;

        if($PowerConsumptions=PowerConsumption::where('device_id',$Device_id)->where('setting_id',$inputs["setting_id"])->get()){
             // dd($PowerConsumptions);
             foreach ($PowerConsumptions as $PowerConsumption) {
                if($PowerConsumption->time>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])&&$PowerConsumption->time<=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addWeek()){
                        $Weekly_Powerconsumption[]=$PowerConsumption;
                }
             }
             for ($i=0; $i<7 ; $i++) { 
                $ad=0;
                $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addDays($i)->toDateTimeString(),"power"=>$ad];
                if(isset($Weekly_Powerconsumption)||$ad!=0){
                    foreach ($Weekly_Powerconsumption as $key => $Weekly_powerconsumption) {
                        if(Carbon::createFromFormat('Y-m-d H:i:s',$Weekly_powerconsumption->time)>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addDays($i)&&
                            Carbon::createFromFormat('Y-m-d H:i:s',$Weekly_powerconsumption->time)<Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addDays($i+1))
                        {
                            $ad+=isset($Weekly_powerconsumption)?$Weekly_powerconsumption->power:'';
                            $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addDays($i)->toDateTimeString(),"power"=>$ad];
                        }
                    }
                }
             
             }
             
            return $this->returnData($data, __FUNCTION__);

        }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            }
    } 

    public function power_consumption_get_Monthly(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        $data=array();
                if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){

                    return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
                }
        $Device_id =$Device_id->id;

        if($PowerConsumptions=PowerConsumption::where('device_id',$Device_id)->where('setting_id',$inputs["setting_id"])->get()){
             // dd($PowerConsumptions);
            foreach ($PowerConsumptions as $PowerConsumption) {
                if($PowerConsumption->time>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])&&$PowerConsumption->time<=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYear()){
                        $Monthly_Powerconsumption[]=$PowerConsumption;
                }
            }
            for ($i=0; $i<12 ; $i++) { 
                $ad=0;
                $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addMonths($i)->toDateTimeString(),"power"=>$ad];
                if(isset($Monthly_Powerconsumption)){
                    foreach ($Monthly_Powerconsumption as $key => $Monthly_powerconsumption) {
                        if(Carbon::createFromFormat('Y-m-d H:i:s',$Monthly_powerconsumption->time)>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addMonths($i)&&
                            Carbon::createFromFormat('Y-m-d H:i:s',$Monthly_powerconsumption->time)<Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addMonths($i+1))
                        {
                            $ad+=isset($Monthly_powerconsumption)?$Monthly_powerconsumption->power:'';
                            $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addMonths($i)->toDateTimeString(),"power"=>$ad];
                        }
                    }
                }
                
             
             }
             
            return $this->returnData($data, __FUNCTION__);

        }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            }
    } 

    public function power_consumption_get_Yearly(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device_id = 0;
        $data=array();
                if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){

                    return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
                }
        $Device_id =$Device_id->id;

        if($PowerConsumptions=PowerConsumption::where('device_id',$Device_id)->where('setting_id',$inputs["setting_id"])->get()){
             // dd($PowerConsumptions);
            foreach ($PowerConsumptions as $PowerConsumption) {
                if($PowerConsumption->time>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])&&$PowerConsumption->time<=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYears(5)){
                        $Yearly_Powerconsumption[]=$PowerConsumption;
                }
            }
            for ($i=0; $i<5 ; $i++) { 
                $ad=0;
                $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYears($i)->toDateTimeString(),"power"=>$ad];
                if(isset($Yearly_Powerconsumption)){
                    foreach ($Yearly_Powerconsumption as $key => $Yearly_powerconsumption) {
                        if(Carbon::createFromFormat('Y-m-d H:i:s',$Yearly_powerconsumption->time)>=Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYears($i)&&
                            Carbon::createFromFormat('Y-m-d H:i:s',$Yearly_powerconsumption->time)<Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYears($i+1))
                        {
                            $ad+=isset($Yearly_powerconsumption)?$Yearly_powerconsumption->power:'';
                            $data[$i]=["time"=>Carbon::createFromFormat('Y-m-d H:i:s', $inputs["from"])->addYears($i)->toDateTimeString(),"power"=>$ad];
                        }
                    }
                }
             
             }
             
            return $this->returnData($data, __FUNCTION__);

        }else{
                return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
            }
    }


    public function device_audit_trail(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $data = array();
        $Device_id = 0;
        $User = $this->getUserFromJwtToken($inputs);


        foreach ($inputs['device'] as $i => $device) {
            $Device = Device::where('customer_device_serial_id',ltrim ($device["device_id"],"0"))->first();
                $outdoor=0;
                $indoor=0;
                $temperature=0;

            if($Device!=null){
            $Device_id = $Device->id;
            if($Device_id)
            {
                if( ($Ddata=DeviceAuditTrail::where('device_id',$Device_id)->where('batch_id',$device['batch_id'])->get())!=null ){
                    foreach ($Ddata as $key => $data) {
                        if($data->setting_id!=$device['setting_id']){
                            $data->deleted_at=Carbon::now();
                            $data->save();
                        }
                    }
                
                }
                //indoor/outdoot temperature
            foreach ($device['mode'] as $key => $value) {
               
                if($key=='Indoor_Temp'){
                    $indoor = $value;
                }
                if($key=='Outdoor_Temp'){
                    $outdoor = $value;
                }
                if($key=='Temperature'){
                    $temperature = $value;
                }
            }
            Temperatures::Create([
                    'device_id'=>$Device_id,
                    'setting_id'=>isset($device['setting_id'])?$device['setting_id']:"",
                    'batch_id' =>isset($device['batch_id'])?$device['batch_id']:"",
                    'temperature'=>$temperature,
                    'room_temperature'=>$indoor,
                    'outdoor_temperature'=>$outdoor,
                    'time'=>isset($device['time'])?$device['time']:"",

                ]);
            


                // if(Device::find($device['device_id'])){
                    DeviceAuditTrail::updateOrCreate(['device_id'=>$Device_id,'setting_id'=>$device['setting_id'],'batch_id'=>$device['batch_id'],'time'=>$device['time']],
                        [
                            'device_id'=>$Device_id,
                            'setting_id'=>isset($device['setting_id'])?$device['setting_id']:"",
                            'batch_id' =>isset($device['batch_id'])?$device['batch_id']:"",
                            'mode'=>isset($device['mode'])?json_encode($device['mode'], true):"",
                            'time'=>isset($device['time'])?$device['time']:"",
                        ]);
                //  }else{
                //     return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
                // }                
            }
        }
    }
        $data=array();
        return $this->returnData($data, __FUNCTION__);
    }  

    public function device_audit_trail_get_Report(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $data=array();

        if(!$Device_id = Device::where('customer_device_serial_id',ltrim ($inputs["device_id"],"0"))->where('customer_sub_headerblock_id',$inputs["batch_id"])->first()){

            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        $Device_id =$Device_id->id;

        $DeviceAuditTrails=DeviceAuditTrail::where('device_id',$Device_id)->where('setting_id',$inputs["setting_id"])->orderBy('id','desc')->get();
        foreach ($DeviceAuditTrails as $key => $DeviceAuditTrail) {

                $data[]=array("time"=>$DeviceAuditTrail->time,"mode"=>json_decode($DeviceAuditTrail->mode));
                if(count($data)==50){
                    return $this->returnData($data, __FUNCTION__);
                }
            
        }

        return $this->returnData($data, __FUNCTION__);
    }  

        public function firmware(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $data = array();
        $Firmware = Firmware::where('app_id',$inputs['app_id'])->first();
        $Firmware->file=URL::to('/uploads/'.$Firmware->file);

        $data =  $Firmware;

        return $this->returnData($data, __FUNCTION__);
    }  

}