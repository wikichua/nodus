<?php

namespace App\Http\Controllers\Api;
use App\AppVersion;


trait ApiTrait
{
    /**
     * Return JSON response.
     *
     * @param array  $datas
     * @param string $module
     * @param string $status
     * @param array $messages
     *
     * @return mixed
     */
    private function returnData($datas = [], $module = '', $status = 'success', $messages =[], $no_app_data = false)
    {
        $inputs = json_decode(request()->all()['data'], true);

        $updated_at = date('1970-01-01 00:00:00');
        if($messages!=[])
            $messages=[$messages];
        if (count($datas)) {

            foreach ($datas as $key => $data) {
                if (isset($datas[$key]['updated_at']) && $updated_at < $datas[$key]['updated_at']) {
                    $updated_at = $datas[$key]['updated_at'];
                }
            }
            
        }

        if(isset($inputs['imei']) && $inputs['imei'] != '')
        {
            $os = explode(':',$inputs['imei'])[0];
            $LastAppVersion = AppVersion::where('os',$os)->where('status','Active')->first();
            $LastCriticalAppVersion = AppVersion::where('type','Critical')->where('status','Active')->first();
        }
        
        $datas = $this->convertNullRecursive($datas);
        if($no_app_data) {
            $final_datas =[
                'AppData' => [
                    'module'          => $module,
                    'settingIndex'    => date('Y-m-d H:i:s'),
                    'portal_url'      => route('home'),
                    'download_url'    => route('home'),
                    'web_url'         => route('home'),
                    'totalRecords'    => 0,
                    'last_updated_at' => $updated_at,
                    'version'         => '',
                    'status'          => $status,
                    'message'         => $messages,
                    'play_store_url'=>'',
                    'last_app_update_version' => isset($LastAppVersion) && $LastAppVersion->version? $LastAppVersion->version:0,
                    'last_app_update_description' => isset($LastAppVersion) && $LastAppVersion->description? $LastAppVersion->description:'',
                    'last_critical_app_update_version' => isset($LastCriticalAppVersion) && $LastCriticalAppVersion->version? $LastCriticalAppVersion->version:0,
                    'last_critical_app_update_description' => isset($LastCriticalAppVersion) && $LastCriticalAppVersion->description? $LastCriticalAppVersion->description:'',
                    ],
                    'Data' => $datas,
                ];
        }else{
            $final_datas = [
                'AppData' => [
                    'module'          => $module,
                    'settingIndex'    => date('Y-m-d H:i:s'),
                    'portal_url'      => route('home'),
                    'download_url'    => route('home'),
                    'web_url'         => route('home'),
                    'totalRecords'    => isset($datas['settings'])?count($datas['settings']):count($datas),
                    'last_updated_at' => $updated_at,
                    'version'         => '',
                    'status'          => $status,
                    'message'         => $messages,
                    'play_store_url'=>'',
                    'last_app_update_version' => isset($LastAppVersion) && $LastAppVersion->version? $LastAppVersion->version:0,
                    'last_app_update_description' => isset($LastAppVersion) && $LastAppVersion->description? $LastAppVersion->description:'',
                    'last_critical_app_update_version' => isset($LastCriticalAppVersion) && $LastCriticalAppVersion->version? $LastCriticalAppVersion->version:0,
                    'last_critical_app_update_description' => isset($LastCriticalAppVersion) && $LastCriticalAppVersion->description? $LastCriticalAppVersion->description:'',
                ],
                'Data' => $datas,
            ];
        }

        return \Response::make(str_replace(': null',': ""',json_encode($final_datas, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)))
            ->header('Content-Type', 'application/json');
    }

    /**
     * Convert null value to empty string recursively.
     *
     * @return array
     */
    private function convertNullRecursive($values)
    {
        foreach ($values as &$value) {
            if (is_array($value)) {
                $value = $this->convertNullRecursive($value);
            } elseif (is_null($value)) {
                $value = '';
            }
        }

        return $values;
    }

    /**
     * log request input from mobile
     * @param  string $name
     * @param  array $inputs
     */
    
    // private function log_mobile_request($name = '', $inputs)
    // {
    //     $is_log = env('LOG_MOBILE_REQUEST', false);

    //     if ($is_log && isset($inputs['data'])) {
    //         \App\MobileRequestLog::create([
    //             'name' => $name,
    //             'info' => $inputs['data'],
    //         ]);
    //     }
    // }
}
