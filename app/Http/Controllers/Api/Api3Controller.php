<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\Api2Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Device;
use App\User;
use App\PairingLogs;
use App\Sector;
use App\MobileDevice;
use App\PnLog;
use JWTAuth;
use App\Poc as POC_OffAirRequest;
use App\Lib\HouseholdIdGenerator\HouseholdIdGenerator;
use App\Lib\PushNotification\PushNotification;
use App\CustomerIdList;
use Carbon\Carbon;


class Api3Controller extends ApiController
{
    use ApiTrait;
    use JwtAuthTrait;

    public function poc_issue_requests(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);

        if(count(POC_OffAirRequest::where('setting_id',$inputs['setting_id'])->whereIn('status',array('New','Pending'))->get())<2){
        $data = POC_OffAirRequest::create(
            [
                'imei' => isset($inputs['imei'])?$inputs['imei']:"",
                'app_id'=>$inputs['app_id'],
                'setting_id'=>$inputs['setting_id'],
                'request'=>isset($inputs['request'])?$inputs['request']:"",
                'command'=>isset($inputs['command'])?$inputs['command']:"",
                'status'=>'New',
            ]);
    }else{
         return $this->returnData([], __FUNCTION__, 'error', 'Request is Pending,You can try again later',$no_app_data = true);
    }
            
        return $this->returnData($data, __FUNCTION__,'success','',$no_app_data = true);
    }

    public function poc_followup_requests(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        
        if($data = POC_OffAirRequest::find($inputs['POC_OffAirRequest_id']))
        {
            if($data->status!='Completed'&&Carbon::now()->format('Y-m-d h:i:s')>$data->created_at->addMinute()->format('Y-m-d h:i:s')){
                $data->status = 'Failed';
                $data->save();
            }

        }else if($inputs['POC_OffAirRequest_id']=='all'){// POC_OffAirRequest_id = all to return all.
            $setting_ids = Setting::where('user_ids', 'like','%"'.$User->id.'":"Accepted"%')->where('app_id',$inputs['app_id'])->lists('id')->toArray();
            $data = POC_OffAirRequest::whereIn('setting_id',$setting_ids )->where('app_id',$inputs['app_id'])->get();
            foreach ($data as $POC_OffAir) {
                if($POC_OffAir->status!='Completed'&&Carbon::now()->format('Y-m-d h:i:s')>$POC_OffAir->created_at->addMinute()->format('Y-m-d h:i:s')){
                $POC_OffAir->status = 'Failed';
                $POC_OffAir->save();
            }
            }
        }else{
            return $this->returnData([], __FUNCTION__, 'error', 'Request Not Found',$no_app_data = true);
    }
   
    return $this->returnData($data, __FUNCTION__,'success','',$no_app_data = true);
        }

     public function poc_device_check_requests(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $data =array();
        $setting_ids = Setting::where('user_ids', 'like','%"'.$User->id.'":"Accepted"%')->where('app_id',$inputs['app_id'])->lists('id')->toArray();

        $POC_OffAirRequests = POC_OffAirRequest::whereIn('setting_id',$setting_ids)
                    ->whereIn('status',array('new','Pending'))
                    ->orderBy('created_at','asc')->get();

        $count = count($POC_OffAirRequests);
        if(count($POC_OffAirRequests)>2){
             $count = 2;
        }
        for($i=0;$i<=$count-1;$i++) {

            if($POC_OffAirRequests[$i]->status!='Completed'&&Carbon::now()->format('Y-m-d h:i:s')>$POC_OffAirRequests[$i]->created_at->addMinute()->format('Y-m-d h:i:s')){
                $POC_OffAirRequests[$i]->status = 'Failed';
                $POC_OffAirRequests[$i]->save();
                $data[$i] = $POC_OffAirRequests[$i];
                return $this->returnData($data, __FUNCTION__,'success','',$no_app_data = true);
            }
            $POC_OffAirRequests[$i]->status = 'Pending';
            $POC_OffAirRequests[$i]->save();
            
            $data[$i] = $POC_OffAirRequests[$i];
        }
        
        return $this->returnData($data, __FUNCTION__,'success','',$no_app_data = true);
    }
     public function poc_device_complete_requests(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $POC_OffAirRequest = POC_OffAirRequest::find($inputs['POC_OffAirRequest_id']);
        $POC_OffAirRequest->status = 'Completed';
        $POC_OffAirRequest->info = isset($inputs['info'])?$inputs['info']:"";
        $POC_OffAirRequest->save();
        return $this->returnData(array(), __FUNCTION__,'success','',$no_app_data = true);        
    }

}