<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Device;
use App\User;
use App\Sector;
use App\MobileDevice;
use App\PnLog;
use App\Lib\HouseholdIdGenerator\HouseholdIdGenerator;
use App\Lib\PushNotification\PushNotification;
use App\CustomerIdList;

class ApiController extends Controller
{
    use ApiTrait;
    use JwtAuthTrait;

    public function __construct(Request $request)
    {
        $this->middleware('jwt.auth');
        //$this->log_mobile_request(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'', $request->all());
    }

    public function pt(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);

        try {
            $imei = isset($inputs['imei'])?substr($inputs['imei'], 4, count($inputs['imei']) - 3):'';
            $MobileDevice = MobileDevice::updateOrCreate(['imei' => $imei], [
                'user_id'       => $User->id,
                'width'       => isset($inputs['width'])?$inputs['width']:'',
                'height'      => isset($inputs['height'])?$inputs['height']:'',
                'os'          => isset($inputs['imei'])?substr($inputs['imei'], 0, 3):'',
                'os_version'  => isset($inputs['os_version'])?$inputs['os_version']:'',
                'app_version' => isset($inputs['version'])?$inputs['version']:'',
                'push_token'  => str_replace(' ', '', $inputs['push_token'])
            ]);
        } catch (\Exception $e) {
            return $this->returnData([], __FUNCTION__, 'error', 'could not save mobile push token');
        }

        $datas = $MobileDevice->toArray();

        return $this->returnData($datas, __FUNCTION__);
    }

    public function setting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $datas = Setting::where('user_id', $User->id)->orderBy('created_at', 'desc')->get()->toArray();

        return $this->returnData($datas, __FUNCTION__);
    }

    public function submitSetting(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $settings = array();
        $Setting = Setting::where('user_id', $User->id)->get();

        if (count($Setting) > 0) {
            $settings = $Setting->toArray()[0];
        } else {
            $settings['zone'] = '';
            $settings['scene'] = '';
            $settings['timer'] = '';
            $settings['manual_light'] = '';
            $settings['setting'] = '';
        }
        $Settings = Setting::updateOrCreate(['user_id' => $User->id], [
            'zone' => isset($inputs['zone']) ? $inputs['zone'] : $settings['zone'],
            'scene' => isset($inputs['scene']) ? $inputs['scene'] : $settings['scene'],
            'timer' => isset($inputs['timer']) ? $inputs['timer'] : $settings['timer'],
            'manual_light' => isset($inputs['manual_light']) ? $inputs['manual_light'] : $settings['manual_light'],
            'setting' => isset($inputs['setting']) ? $inputs['setting'] : $settings['setting'],
        ]);

        $datas[] = $Settings->toArray();

        return $this->returnData($datas, __FUNCTION__);
    }

    public function checkDevice(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Setting= Setting::where('user_id',$User->id)->get();
        $Devices = Device::all();
        $devices = array();
        
        foreach ($Devices as $key => $Device) {
            if($Device->setting)
            {
                if ($Device->setting->user->id == $User->id) {
                    $devices[] = $Device;
                    // dd($devices);
                }                
            }

            // foreach ($Device['user2_ids']->id as $user2_id) {
            //     if ($user2_id == $User->id) {
            //         $devices[$key] = $Device;
            //     }
            // }
        }
         // $this->setDeviceWithEmail($devices);
        $settings = \App\Setting::where('user_id', $User->id)->orderBy('created_at', 'desc')->get()->toArray();
        // $devices['settings'] = $settings;

        return $this->returnData($devices, __FUNCTION__);
    }

    public function pairDevice(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Devices = $this->getAuthDevice($request);
        if (!$Devices) {
            return $this->returnData([], __FUNCTION__, 'error', ['Device not found']);
        }
        foreach ($Devices as $Device) {
            $gps = $Device->gps;
            $lat = trim($inputs['lat']) != ''? $inputs['lat']:$gps->lat;
            $long = trim($inputs['long']) != ''? $inputs['long']:$gps->long;
            $Device->gps = json_encode(['lat' => $lat, 'long' => $long]);
            $Device->setting_id = trim($inputs['setting_id']);
            $Device->save();
            $devices[] = $Device->toArray();
            $this->setDeviceWithEmail($devices);
        }

        return $this->returnData($devices, __FUNCTION__);
    }

    public function unpairDevice(Request $request)
    {
        $Devices = $this->getAuthDevice($request);
        if (!$Devices) {
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        foreach ($Devices as $Device) {
            $gps = $Device->gps;
            $Device->gps = json_encode(['lat' => '', 'long' => '']);
            $Device->setting_id = 0;
            $Device->save();
            $devices[] = $Device->toArray();
            $this->setDeviceWithEmail($devices);
        }
        return $this->returnData($devices, __FUNCTION__);
    }

    public function shareDevice(Request $request, PushNotification $PushNotification, PnLog $PnLog)
    {
        $inputs = json_decode($request->all()['data'], true);
        $Devices = $this->getAuthDevice($request);
        if (!$Devices) {
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        $User2 = User::where('email', $inputs['user2_email'])
            ->where('role', 'mobile')
            ->where('app_id', $inputs['app_id'])->first();
        if (!$User2) {
            return $this->returnData([], __FUNCTION__, 'error', 'User2 email not found');
        }
        $devices = array();
        foreach ($Devices as $Device) {
            $user2_ids = $Device->user2_ids->id;
            $is_user2_exist = false;
            foreach ($user2_ids as $user2_id) {
                if ($user2_id == $User2->id) {
                    $is_user2_exist = true;
                    continue;
                }
            }
            if ($is_user2_exist) {
                continue;
            }
            $user2_ids[] = $User2->id;
            $user2_statuses = $Device->user2_ids->status;
            $user2_statuses[] = 0;
            $Device->user2_ids = json_encode(['id' => $user2_ids, 'status' => $user2_statuses]);
            $Device->save();
             if (!$Devices) {
                $devices[] = $Device->toArray();
            $this->setDeviceWithEmail($devices);
        }
            
        }

        $user2_mobile_devices = $User2->mobileDevices()->get()->toArray();
        $User = $this->getUserFromJwtToken($inputs);

        foreach ($user2_mobile_devices as $user2_mobile_device) {
            $token = $user2_mobile_device['push_token'];
            $message = "$User->name share a $Device->name with you";
            $pn_response = $PushNotification->send($token, $message);
        }

        return $this->returnData($devices, __FUNCTION__);
    }

    public function unshareDevice(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $Devices = $this->getAuthDevice($request);
        if (!$Devices) {
            return $this->returnData([], __FUNCTION__, 'error', 'Device not found');
        }
        foreach ($Devices as $Device) {
            $User2 = User::where('email', $inputs['user2_email'])
                ->where('role', 'mobile')
                ->where('app_id', $inputs['app_id'])->first();
            $user2_ids = $Device->user2_ids->id;
            $user2_key = array_search($User2->id, $user2_ids);
            unset($user2_ids[$user2_key]);
            sort($user2_ids);
            $user2_statuses = $Device->user2_ids->status;
            unset($user2_statuses[$user2_key]);
            sort($user2_statuses);
            $Device->user2_ids = json_encode(['id' => $user2_ids, 'status' => $user2_statuses]);
            $Device->save();
            $devices[] = $Device->toArray();
            $this->setDeviceWithEmail($devices);
        }

        return $this->returnData($devices, __FUNCTION__);
    }

    public function acceptDevice(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Device = Device::find($inputs['device_id']);
        $user2_ids = $Device->user2_ids->id;
        $user2_statuses = $Device->user2_ids->status;
        foreach ($user2_ids as $key => $user2_id) {
            if ($user2_id == $User->id) {
                $user2_statuses[$key] = 1;
                continue;
            }
        }
        $Device->user2_ids = json_encode(['id' => $user2_ids, 'status' => $user2_statuses]);
        $Device->save();
        $devices[] = $Device->toArray();
        $this->setDeviceWithEmail($devices);

        return $this->returnData($devices, __FUNCTION__);
    }

    public function feedback(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $Feedbacks = $User->feedback()->create($inputs);

        $CustomerIdList = CustomerIdList::find($inputs['app_id']);
        $background_color = '#71bc37';
        $image_link = url('images/logo-big-black.png');
        if ($CustomerIdList->background_color && $CustomerIdList->background_color != '') {
            $background_color = $CustomerIdList->background_color;
        }
        if ($CustomerIdList->app_icon && $CustomerIdList->app_icon != '') {
            $image_link = url('/uploads/' . $CustomerIdList->app_icon);
        }

        $mail_datas = [
            'user_name' => $User->name,
            'background_color' => $background_color,
            'image_link' => $image_link,
            'Feedbacks' => $Feedbacks,
        ];

        \Mail::queue('emails.thanks_feedback', $mail_datas, function ($message) use ($User, $CustomerIdList) {
            if (!isset($CustomerIdList->email) || $CustomerIdList->email == '') {
                $CustomerIdList->email = 'support@convep.com';
            }
            $message->from($CustomerIdList->email, $CustomerIdList->name);
            $message->to($User->email, $User->name)->subject($CustomerIdList->name . ' - Thank you for giving feedback');
        });

        $datas[] = $Feedbacks;

        return $this->returnData($datas, __FUNCTION__);
    }

    public function updateProfile(Request $request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $User = $this->getUserFromJwtToken($inputs);
        $User->name = isset($inputs['name'])?$inputs['name']:$User->name;
        $User->gender = isset($inputs['gender'])?$inputs['gender']:$User->gender;
        $User->dob = isset($inputs['dob'])?$inputs['dob']:$User->dob;
        $User->save();
        $datas[] = $User;

        return $this->returnData($datas, __FUNCTION__);
    }

    public function getAuthDevice($request)
    {
        $inputs = json_decode($request->all()['data'], true);
        $Devices = array();
        for ($i = 0; $i < count($inputs['device_type']); $i++) {
            $Devices[$i] = Device::where('customer_id', $inputs['customer_id'])
                ->where('device_type', $inputs['device_type'][$i])
                ->where('customer_sub_headerblock_id', $inputs['customer_sub_headerblock_id'][$i])
                ->where('customer_device_serial_id', $inputs['customer_device_serial_id'][$i])
                ->first();
            if (!$Devices[$i]) {
                $Devices[$i]=Device::create([
                'name' =>'device',
                'customer_id'=>$inputs['customer_id'],
                'device_type'=>$inputs['device_type'][$i],
                'customer_device_serial_id'=>$inputs['customer_device_serial_id'][$i],
                'customer_sub_headerblock_id'=>$inputs['customer_sub_headerblock_id'][$i],
                'leadsource'=>'API',
                'status'=>'Pending',
                ]);
            }
        }

        return $Devices;
    }

    private function setDeviceWithEmail(&$devices)
    {
        foreach ($devices as $key => $device) {
            $Setting = Setting::find($device['setting_id']);
            $email = isset($Setting->user->email)?$Setting->user->email:'';
            $household_id = isset($Setting->user->household_id)?$Setting->user->household_id:'';
            try {
                $devices[$key]['user_ids']->email[0] = $email;
                $devices[$key]['user_ids']->household_id[0] = $household_id;                
            } catch (\Exception $e) {
                $devices[$key]['user_ids']->email[0] = '';
                $devices[$key]['user_ids']->household_id[0] = '';
            }

            // foreach ($device['user2_ids']->id as $key2 => $user2_id) {
            //     try {
            //         $devices[$key]['user2_ids']->email[$key2] = User::find($user2_id)->email;
            //     } catch (\Exception $e) {
            //         $devices[$key]['user2_ids']->email[$key2] = '';
            //     }
            // }
        }
    }


    

}
