<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\InfoRequest;
use App\Http\Requests\ImageRequest;
use Auth,Session;
use App\User;
use App\Lib\GoogleApiManager\GoogleApiManager;
use App\CustomerIdList;
use App\PowerConsumption;
use App\DeviceAuditTrail;
use App\Device;
use Gate;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',array('except'=>array('ApiTesting')));
    }

    public function profile()
    {
        $Users = Auth::user();

        return view('home.profile', compact('Users'));
    }

    public function update(InfoRequest $request)
    {
        $User = Auth::user();
        $User->name = $request->get('name', $User->name);
        //$User->email = $request->get('email', $User->email);
        $User->gender = $request->get('gender', $User->gender);
        $User->dob = $request->get('dob', $User->dob);
        $User->save();

        return  redirect()->route('home.profile')->with('success-message', 'Updated successfully');
    }

    public function updateimage(ImageRequest $request)
    {

        $User = Auth::user();
         $User->picture = uploadImage('picture', $User->picture, 'profile');
        $User->save();
        return redirect()->route('home.profile')->with('success-message', 'Updated Successfully');

    }

    public function updatepassword(PasswordRequest $request)
    {
        $User = Auth::user();
        $User->password = bcrypt($request->get('password'));
        $User->save();

        return redirect()->back()->with('success-message', 'Updated Successfully');
    }

    public function switch_user($id = '0')
    {
        if($id != 0)
        {
            // if(Session::has("original_user_id")){

            //     Auth::login(User::find($id));
            //     Session::forget('original_user_id');
            //     return redirect()->route('home')->with('success-message','You have switched to '. Auth::user()->name .'.');
            // }
            if(!Session::has('original_user_id')){
                Session::put('original_user_id', Auth::user()->id);
                 Auth::login(User::find($id));
                return redirect()->route('home')->with('success-message','You have switched to '. Auth::user()->name .'.');

            }
                 Auth::login(User::find(Session::get('original_user_id')));
            Session::forget('original_user_id');
            return redirect()->route('home')->with('success-message','You have switched back to '. Auth::user()->name .'.');
        }else{
            Auth::login(User::find(Session::get('original_user_id')));
            Session::forget('original_user_id');
            return redirect()->route('home')->with('success-message','You have switched back to '. Auth::user()->name .'.');
        }

        return redirect()->route('home');
    }


    public function chartjs(Request $request,$CustomerIdLists_id)
    {
        $CustomerIdLists = CustomerIdList::find($CustomerIdLists_id);
        $Devices=Device::where('customer_id',$CustomerIdLists->code)->get();
        $GA_CHART_START_DATE=array();
        $power=array(0,0,0,0,0,0,0);
        $powers= array();
        $DAT= array();
        $Count_DAT= 0;
        $DeviceAuditTrail= array(0,0,0,0,0,0,0);
        for($i=6 ;$i>=0;$i--){
            $GA_CHART_START_DATE[]=Carbon::now()->subMonths($i)->format('Y-m');
        }
        $t=0;
        $t2=0;
        foreach ($Devices as $key=>$devices) {
            $PC = PowerConsumption::where('device_id',$devices->id)->get();
            foreach ($PC  as $key => $pc) {
                foreach ( $GA_CHART_START_DATE as $key=> $date) {
                     
                    if(str_split($pc->time, 7)[0]==$date){

  
                       $t +=$pc->power;
                        $power[$key]=$t;
                        $s=str_split($pc->time, 7)[0];
                    }else{

                    }
                }
            };
            $DAT = DeviceAuditTrail::where('device_id',$devices->id)->get();
            ;
            foreach ($DAT  as $key => $dat) {
                // dd($dat->mode);
                $Count_DAT++;
                foreach ( $GA_CHART_START_DATE as $key=> $date) {
                     
                    if(str_split($dat->time, 7)[0]==$date){
                        $t2 +=json_decode($dat->mode,JSON_NUMERIC_CHECK)['Temperature'];
                        $DeviceAuditTrail[$key]=$t2/$Count_DAT;
                        $s=str_split($dat->time, 7)[0];
                    }else{

                    }
                }
            };

        }
        
        foreach ($power as $key => $value) {
            $powers[]=['x'=>$key,'y'=>$value];
        }

        $DeviceAuditTrail = json_encode($DeviceAuditTrail,JSON_NUMERIC_CHECK);
        $power = json_encode($power,JSON_NUMERIC_CHECK);
        $GA_CHART_START_DATE = json_encode($GA_CHART_START_DATE,JSON_NUMERIC_CHECK);
        return view('home.chartjs')->with(compact('GA_CHART_START_DATE','power','CustomerIdLists_id','DeviceAuditTrail'));
    }

    public function index(GoogleApiManager $GoogleApiManager)
    {

        // $gapi_access_token = $GoogleApiManager->getAccessToken();
        // $app_id = auth()->user()->app_id;
        // $gapi_ids = array();
        
        // $GA_CHART_START_DATE='30daysAgo';
        //  $GA_CHART_END_DATE='yesterday';
        // if ($app_id) {
        //     $CustomerIdLists[] = CustomerIdList::find($app_id);
        // } else {
        //     $CustomerIdLists = CustomerIdList::where('id','1')->get();
        // }
        // return view('home.index', compact('gapi_access_token', 'CustomerIdLists','GA_CHART_START_DATE','GA_CHART_END_DATE'));
        return $this->ga1($GoogleApiManager,1,request());
    }


    public function ga1(GoogleApiManager $GoogleApiManager, $id, Request $request)
    {
        $gapi_access_token = $GoogleApiManager->getAccessToken();
        if (Gate::denies('admin', auth()->user())) {
            $app_id = auth()->user()->app_id;
             if (Gate::denies('manager', auth()->user())) {
                }else{
                    $first= json_decode(auth()->user()->app_ids,true);
                    $app_id = $first[0];
                }
        }else{
            $app_id = $id;
        }
        $gapi_ids = array();
        $GA_CHART_START_DATE=$request->get('ga_from',Carbon::now()->subDays(30)->format('Y-m-d'));
        $GA_CHART_END_DATE=$request->get('ga_to',Carbon::now()->subDays(1)->format('Y-m-d'));
        $CustomerIdLists = CustomerIdList::where('id',$app_id)->get();
        return view('home.index', compact('gapi_access_token', 'CustomerIdLists','GA_CHART_START_DATE','GA_CHART_END_DATE'));
    }

    public function ga2(GoogleApiManager $GoogleApiManager, $id)
    {

        $gapi_access_token = $GoogleApiManager->getAccessToken();
        $app_id = auth()->user()->app_id;
        $gapi_ids = array();
        if ($app_id) {
            $CustomerIdLists[] = CustomerIdList::find($app_id);
        } else {
            $CustomerIdLists = CustomerIdList::where('id',$id)->get();
            
        }

        return view('home.index', compact('gapi_access_token', 'CustomerIdLists'));
    }


    /**
     * Mobile API simulator input form.
     *
     * @return view
     */
    public function poster()
    {
        return view('api.poster');
    }

    public function poster2()
    {
        return view('api.poster2');
    }

        public function ApiTesting()
    {
        return view('api.ApiTesting');
    }
}
