<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerSubIdListRequest;
use App\Http\Controllers\Controller;
use App\CustomerSubHeaderblockIdList;
use App\CustomerIdList;
use App\IeeeList;
use Gate;
use Auth;

class CustomerSubIdListController extends Controller
{
    protected $CustomerSubHeaderblockIdList;
    protected $CustomerIdList;
    protected $IeeeList;

    public function __construct(CustomerSubHeaderblockIdList $CustomerSubHeaderblockIdList,
        CustomerIdList $CustomerIdList,
        IeeeList $IeeeList)
    {
        $this->middleware('auth');
        $this->CustomerSubHeaderblockIdList = $CustomerSubHeaderblockIdList;
        $this->CustomerIdList = $CustomerIdList;
        $this->IeeeList = $IeeeList;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('admin', 'user')) 
       {
            $CustomerSubHeaderblockIdLists = $this->CustomerSubHeaderblockIdList->all();
        }else{
            $CustomerSubHeaderblockIdLists = $this->CustomerSubHeaderblockIdList->where('customer_id_list_id',auth()->user()->app_id)->get();

        }
        

        return view('cms.customer_sub_id_lists.index', compact('CustomerSubHeaderblockIdLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        $this->getFieldList($customer_id_lists, $ieee_lists);

        return view('cms.customer_sub_id_lists.create', compact('customer_id_lists', 'ieee_lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerSubIdListRequest $request)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->CustomerSubHeaderblockIdList->create($request->all());

        return redirect()->route('cms.customer_sub_id_lists.index')->with('success-message', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $CustomerSubHeaderblockIdList = $this->CustomerSubHeaderblockIdList->find($id);
        $this->getFieldList($customer_id_lists, $ieee_lists);
        return view('cms.customer_sub_id_lists.edit', compact('CustomerSubHeaderblockIdList', 'customer_id_lists', 'ieee_lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerSubIdListRequest $request, $id)
    {
        $this->CustomerSubHeaderblockIdList->updateOrCreate(['id' => $id], $request->all());

        return redirect()->route('cms.customer_sub_id_lists.index')->with('success-message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->CustomerSubHeaderblockIdList->destroy($id);

        session()->flash('success-message', 'Deleted successfully');
    }

    /**
     * get listing from fields
     * @param  array  $customer_id_lists
     * @param  array  $ieee_lists
     */
    private function getFieldList(&$customer_id_lists = array(), &$ieee_lists = array())
    {
        $CustomerIdLists = $this->CustomerIdList->all();
        foreach ($CustomerIdLists as $key => $CustomerIdList) {
            $customer_id_lists[$CustomerIdList->id] = $CustomerIdList->name;
        }

        $IeeeLists = $this->IeeeList->all();
        foreach ($IeeeLists as $key => $IeeeList) {
            $ieee_lists[$IeeeList->id] = $IeeeList->name;
        }
    }
}
