<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DeviceTypeList;
use Gate;

class DeviceTypeListController extends Controller
{
    protected $DeviceTypeList;
    protected $rules;

    public function __construct(DeviceTypeList $DeviceTypeList)
    {
        $this->middleware('auth');
        $this->DeviceTypeList = $DeviceTypeList;
        $this->rules = [
            'name' => 'required',
            'code' => 'required|integer|min:0|max:255',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $DeviceTypeLists = $this->DeviceTypeList->all();

        return view('cms.device_type_lists.index', compact('DeviceTypeLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        return view('cms.device_type_lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->validate($request, $this->rules);

        $this->DeviceTypeList->create($request->all());

        return redirect()->route('cms.device_type_lists.index')->with('success-message', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $DeviceTypeList = $this->DeviceTypeList->find($id);

        return view('cms.device_type_lists.edit', compact('DeviceTypeList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->validate($request, $this->rules);

        $this->DeviceTypeList->updateOrCreate(['id' => $id], $request->all());

        return redirect()->route('cms.device_type_lists.index')->with('success-message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->DeviceTypeList->destroy($id);

        session()->flash('success-message', 'Deleted successfully');
    }
}
