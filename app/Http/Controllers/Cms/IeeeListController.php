<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\IeeeListRequest;
use App\Http\Controllers\Controller;
use App\IeeeList;
use Gate;

class IeeeListController extends Controller
{
    protected $IeeeList;

    public function __construct(IeeeList $IeeeList)
    {
        $this->middleware('auth');
        $this->IeeeList = $IeeeList;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $IeeeLists = $this->IeeeList->all();

        return view('cms.ieee_lists.index', compact('IeeeLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        return view('cms.ieee_lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(IeeeListRequest $request)
    {

        $this->IeeeList->create($request->all());

        return redirect()->route('cms.ieee_lists.index')->with('success-message', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $IeeeList = $this->IeeeList->find($id);

        return view('cms.ieee_lists.edit', compact('IeeeList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(IeeeListRequest $request, $id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        $this->IeeeList->updateOrCreate(['id' => $id], $request->all());

        return redirect()->route('cms.ieee_lists.index')->with('success-message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }
        $this->IeeeList->destroy($id);

        session()->flash('success-message', 'Deleted successfully');
    }
}
