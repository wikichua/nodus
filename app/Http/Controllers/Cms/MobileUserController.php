<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth,Gate;

class MobileUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(User $User)
    {
        if (auth()->user()->app_id!=0) {
            $app_id = auth()->user()->app_id;
            $MobileUsers = $User->where('role', 'mobile')->where('app_id', $app_id)->where('confirmed', 1)->get();
        }else {
            if (Auth::user()->can('manager', 'user')) {
                $MobileUsers = $User->whereIn('app_id',json_decode( Auth::user()->app_ids,true))->where('role', 'mobile')->get();
            }else {
            $MobileUsers = $User->where('role', 'mobile')->get();
        }
        }

        foreach ($MobileUsers as $MobileUser) {
            if ($MobileUser->dob == '1970-01-01') {
                $MobileUser->dob = '';
            }

            if ($MobileUser->confirmed) {
                if ($MobileUser->confirmed_at == '1970-01-01') {
                    $MobileUser->confirmed_at = '';
                }
                $MobileUser->confirmed = $MobileUser->confirmed_at;
            } else {
                $MobileUser->confirmed = 'not activated';
            }
        }

        return view('cms.mobile_users.index', compact('MobileUsers'));
    }
}
