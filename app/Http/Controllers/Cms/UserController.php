<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\CustomerIdList;
use Auth;
use Session;
use Gate;

class UserController extends Controller
{
    protected $User;

    public function __construct(User $User)
    {
        $this->User = $User;

        $this->middleware('role');
    }

    public function index()
    {
        if (Auth::user()->can('admin', 'user')) {
            if (Auth::user()->can('su', 'user')) {
                $Users = User::where('id', '!=', Auth::user()->id)->where('role', '!=', 'mobile')->get();
            }else {
                $Users = User::where('id', '!=', Auth::user()->id)->where('role', '!=', 'su')->where('role', '!=', 'mobile')->get();
            }
        }elseif (Auth::user()->can('customer', 'user')) {
            $Users = User::where('id', '!=', Auth::user()->id)->where('app_id', Auth::user()->app_id)->where('role', 'sub_customer')->where('role', '!=', 'mobile')->get();
        }elseif (Auth::user()->can('manager', 'user')) {
            $Users = User::where('id', '!=', Auth::user()->id)->whereIn('app_id',json_decode( Auth::user()->app_ids,true))->where('role', '!=', 'mobile')->get();
        }

        return view('users.index', compact('Users'));
    }

    public function create()
    {
        if (!Gate::denies('sub_customer', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        $Appid = CustomerIdList::all();

        foreach ($Appid as $key => $appid) {
            $app_id[$appid->id] = $appid->name;
        }

        return view('users.create', compact('app_id'));
    }

    public function edit($id)
    {
        $Users = User::find($id);

        return view('users.edit', compact('Users'));
    }

    public function update(Request $request, $id)
    {
        $Users = User::find($id);
        $Users->name = $request->get('name');
        $Users->save();

        return  redirect()->route('users.index')->with('success-message', 'Updated successfully');
    }

    public function show($id)
    {
        $Users = User::find($id);
        $CustomerIdList = CustomerIdList::find($Users->app_id);

        return view('users.show', compact('Users', 'CustomerIdList'));
    }

    public function store(RegisterRequest $request)
    {

        $ExistingUser = User::where('email', $request->email)->where('role', '!=', 'mobile')->where('app_id', $request->app_id)->first();

        if ($ExistingUser) {
            return redirect()->back()->with('warning-message', 'Email has already been registered.');
        }

        $User = User::create([
            'name'         => $request->name,
            'email'        => $request->email,
            'password'     => bcrypt($request->password),
            'confirmed'    => 1,
            'role'         => $request->get('Role', 'sub_customer'),
            'app_id'       => $request->get('App_id',''),
            'app_ids'      => $request->has('app_ids')? json_encode($request->get('app_ids')):json_encode(array()),
        ]);

        return redirect()->route('users.index')->with('success-message', 'Created successfully');
    }

    public function destroy($id)
    {
        $this->User->destroy($id);

        session()->flash('success-message', 'Deleted successfully');
    }
}
