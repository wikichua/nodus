<?php

namespace App\Http\Controllers\Cms;
use App\Http\Requests\DeviceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Device;
use App\User;
use App\Temperatures;
use App\PowerConsumption;
use App\DeviceAuditTrail;
use App\CustomerIdList;
use App\CustomerSubHeaderblockIdList;
use App\DeviceTypeList;
use App\IeeeList;
use Gate;
use Carbon\Carbon;
use Auth;

class DeviceController extends Controller
{
    protected $Device;
    protected $CustomerIdList;
    protected $CustomerSubHeaderblockIdList;
    protected $DeviceTypeList;
    protected $IeeeList;

    public function __construct(Device $Device,
        CustomerIdList $CustomerIdList,
        CustomerSubHeaderblockIdList $CustomerSubHeaderblockIdList,
        DeviceTypeList $DeviceTypeList,
        IeeeList $IeeeList)
    {
        $this->middleware('auth',array('except'=>array('webview')));

        $this->Device                       = $Device;
        $this->CustomerIdList               = $CustomerIdList;
        $this->CustomerSubHeaderblockIdList = $CustomerSubHeaderblockIdList;
        $this->DeviceTypeList               = $DeviceTypeList;
        $this->IeeeList                     = $IeeeList;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('admin', 'user')) {
            $Devices = Device::all();
        }else{
            $Customer = CustomerIdList::where('id', Auth::user()->app_id)->first();
            $Devices = Device::where("customer_id", $Customer->code)->get();
        }
        foreach ($Devices as $Device) {
            $Device = $this->getSerialNumber($Device);
            $Device->customer_id = isset($Device->customer_id_list->name) ? $Device->customer_id_list->name : '';
        }

        return view('cms.inventory.index', compact('Devices'));
    }

    /**
     * Get Device serial number.
     *
     * @param mixed $Device
     *
     * @return mixed
     */
    private function getSerialNumber($Device)
    {
        $customer_id = str_pad(dechex($Device->customer_id), 2, '0', STR_PAD_LEFT);
        $device_type = str_pad(dechex($Device->device_type), 2, '0', STR_PAD_LEFT);
        $ieee = str_pad(dechex($Device->ieee), 6, '0', STR_PAD_LEFT);
        $customer_sub_headerblock_id = str_pad(dechex($Device->customer_sub_headerblock_id), 2, '0', STR_PAD_LEFT);
        $customer_device_serial_id = str_pad(dechex($Device->customer_device_serial_id), 4, '0', STR_PAD_LEFT);
        $Device->serial_number = '0x' . $customer_id.$device_type.$ieee.$customer_sub_headerblock_id.$customer_device_serial_id;

        return $Device;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('success-message', 'You Are No Permission');
        }
        $this->getFieldList($customer_id_lists, $customer_sub_headerblock_id_lists, $device_type_lists, $ieee_lists);

        return view('cms.inventory.create', compact(
            'customer_id_lists',
            'customer_sub_headerblock_id_lists',
            'device_type_lists',
            'ieee_lists'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DeviceRequest $request)
    {
        $Device = $this->Device->create($request->all());
        $Device->gps = '{"lat":[0],"long":[0]}';
        $Device->leadsource='Portal';
        $Device->status='Approved';
        $Device->save();

        return redirect()->route('cms.inventory.index')->with('success-message', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Device = $this->Device->find($id);

        $Device = $this->getSerialNumber($Device);
        $Device->customer_id = isset($Device->customer_id_list->name) ? $Device->customer_id_list->name : '';
        $Device->device_type = isset($Device->device_type_list->name) ? $Device->device_type_list->name : '';
        $Device->ieee = isset($Device->ieee_list->name) ? $Device->ieee_list->name : '';
        $Device->customer_sub_headerblock_id = isset($Device->customer_sub_headerblock_id_list->name) ? $Device->customer_sub_headerblock_id_list->name : '';
        $Device->customer_device_serial_id = '0x' . str_pad(dechex($Device->customer_device_serial_id), 4, '0', STR_PAD_LEFT);

        return view('cms.inventory.show', compact('id', 'Device'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getFieldList($customer_id_lists, $customer_sub_headerblock_id_lists, $device_type_lists, $ieee_lists);

        $Device = $this->Device->find($id);


        return view('cms.inventory.edit', compact(
            'customer_id_lists',
            'device_type_lists',
            'ieee_lists',
            'customer_sub_headerblock_id_lists',
            'Device'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DeviceRequest $request, $id)
    {
        $Device = $this->Device->updateOrCreate(['id' => $id], $request->all());

        return redirect()->route('cms.inventory.index')->with('success-message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return
     */
    public function destroy($id)
    {
        $this->Device->destroy($id);

        session()->flash('success-message', 'Deleted successfully');
    }

    public function get_chart_data2(Request $request,$id,$setting_id,$batch_id)
    {
        $Power = PowerConsumption::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $time = array();
        $DeviceAuditTrail = DeviceAuditTrail::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $form_date = Carbon::createFromFormat('Y-m-d H:i:s',$request->get('form_date',Carbon::now()->format('Y-m-d')).'00:00:00')->toDateTimeString();
        ($request->get('to_date',1)==1)?$to_date = Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addDays(1):$to_date = Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addMonths(1);
        $Temp=array();
        $Powers= array();
        $chart_Powers= array();
        $unit='hour';
        $ad=0;


        foreach ($Power as $power) {
            $p=json_decode($power->power,JSON_NUMERIC_CHECK);
            if(Carbon::createFromFormat('Y-m-d H:i:s', $power->time)>=substr($form_date,0,8).'01 00:00:00'&&Carbon::createFromFormat('Y-m-d H:i:s', $power->time)<=$to_date){
                $Powers[] = ['x'=>$power->time,'y'=>$p];
            }
        }
        asort($Powers);

        if($request->get('to_date')==1){
            $unit="hour";
            $form=$form_date;
            for($i=0;$i<24;$i++){
                // $ad=0;
                $chart_Powers[$i]=$ad;
                $time[]=($i<10)?"0".$i.":00":$i.":00";
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addHours($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addHours($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=$ad;
                    }
                }
            }

        }

        if($request->get('to_date')==365){
            $unit="month";
            $form=substr($form_date,0,8).'01 00:00:00';
            $to=Carbon::createFromFormat('Y-m-d H:i:s',substr($to_date,0,8).'01 00:00:00');
            for($i=0;$i<Carbon::createFromFormat('Y-m-d H:i:s',$form)->diffInDays($to);$i++){
                 // $ad=0;
                $chart_Powers[$i]=$ad;
                $time[]=substr(Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)->toDateTimeString(),0,10);
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=$ad;
                    }
                }
            }
        $form_date= substr($form_date,0,8).'01 00:00:00';
        } 


        return array('unit'=>$unit,'form_date'=>$form_date,'to_date'=>$request->get('to_date'),'CustomerIdLists_id'=>$id,'Power'=>$chart_Powers,'time'=>$time);

    }

    public function get_chart_data(Request $request,$id,$setting_id,$batch_id)
    {
        $Power = PowerConsumption::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $time = array();
        $DeviceAuditTrail = DeviceAuditTrail::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $form_date = Carbon::createFromFormat('Y-m-d H:i:s',$request->get('form_date',Carbon::now()->format('Y-m-d')).'00:00:00')->toDateTimeString();
        ($request->get('to_date',1)==1)?$to_date = Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addDays(1):$to_date = Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addMonths(1);
        $Temp=array();
        $Powers= array();
        $chart_Powers= array();
        $unit='hour';


        foreach ($Power as $power) {
            $p=json_decode($power->power,JSON_NUMERIC_CHECK);
            if(Carbon::createFromFormat('Y-m-d H:i:s', $power->time)>=substr($form_date,0,8).'01 00:00:00'&&Carbon::createFromFormat('Y-m-d H:i:s', $power->time)<=$to_date){
                $Powers[] = ['x'=>$power->time,'y'=>$p];
            }
        }
        asort($Powers);



        // foreach ($DeviceAuditTrail as$deviceAuditTrail) {
        //     $dat=json_decode($deviceAuditTrail,JSON_NUMERIC_CHECK);
        //     $mode=json_decode($dat['mode'],JSON_NUMERIC_CHECK);
        //     if(Carbon::createFromFormat('Y-m-d H:i:s', $deviceAuditTrail->time)>=$form_date&&Carbon::createFromFormat('Y-m-d H:i:s', $deviceAuditTrail->time)<=$to_date){
        //         $Temp[] = ['x'=>$deviceAuditTrail->time,'y'=>$mode['temp'],'z'=>$mode];
        //     }

        // }
        if($request->get('to_date')==1){
            $unit="hour";
            $form=$form_date;
            for($i=0;$i<24;$i++){
                $ad=0;
                $chart_Powers[$i]=$ad;
                $time[]=($i<10)?"0".$i.":00":$i.":00";
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addHours($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addHours($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=$ad;
                    }
                }
            }

        }

        if($request->get('to_date')==7){
            $unit="day";
            $form=$form_date;
            for($i=0;$i<7;$i++){
                $ad=0;
                $chart_Powers[$i]=$ad;
                $time[]=substr(Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)->toDateTimeString(),0,10);
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=$ad;
                    }
                }
            }
        }  


        if($request->get('to_date')==365){
            $unit="month";
            $form=substr($form_date,0,8).'01 00:00:00';
            $to=Carbon::createFromFormat('Y-m-d H:i:s',substr($to_date,0,8).'01 00:00:00');
            for($i=0;$i<Carbon::createFromFormat('Y-m-d H:i:s',$form)->diffInDays($to);$i++){
                $ad=0;
                $chart_Powers[$i]=$ad;
                $time[]=substr(Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)->toDateTimeString(),0,10);
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addDays($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=$ad;
                    }
                }
            }
        $form_date= substr($form_date,0,8).'01 00:00:00';
        } 

        // if($request->get('to_date')==365){
        //     $unit="month";
        //     $form=substr($form_date,0,8).'01 00:00:00';
        //     for($i=0;$i<12;$i++){
        //         $ad=0;
        //         $chart_Powers[$i]=$ad;
        //         $time[]=substr(Carbon::createFromFormat('Y-m-d H:i:s',$form)->addMonths($i)->toDateTimeString(),0,10);
        //         foreach ($Powers as $key => $power) {
        //             if(Carbon::createFromFormat('Y-m-d H:i:s',substr($power["x"],0,8).'01 00:00:00')>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addMonths($i)&&
        //                 Carbon::createFromFormat('Y-m-d H:i:s',substr($power["x"],0,8).'01 00:00:00')<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addMonths($i+1)){
        //                 $ad+=$power['y'];
        //                 $chart_Powers[$i]=$ad;
        //             }
        //         }
        //     }
        //     $form_date= substr($form_date,0,8).'01 00:00:00';
        // } 
        if($request->get('to_date')==365*5){
            $unit="year";
            $form=$form_date;
            for($i=0;$i<=12;$i++){
                $ad=0;
                $chart_Powers[$i]=['x'=>Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)->toDateTimeString(),'y'=>$ad];
                foreach ($Powers as $key => $power) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$power["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i+1)){
                        $ad+=$power['y'];
                        $chart_Powers[$i]=['x'=>Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)->toDateTimeString(),'y'=>$ad];
                    }
                }
            }
        } 

            
        // dd(array_values($chart_Powers));
        return array('unit'=>$unit,'form_date'=>$form_date,'to_date'=>$request->get('to_date'),'CustomerIdLists_id'=>$id,'Power'=>$chart_Powers,'time'=>$time);
            // ->with('unit',$unit)
            // ->with('form_date',$form_date)
            // ->with('to_date',$request->get('to_date'))
            // ->with('CustomerIdLists_id',$id)
            // ->with('Power',json_encode($chart_Powers,JSON_NUMERIC_CHECK))

            // ->with('time',json_encode(array($form_date,$to_date->toDateTimeString()),JSON_NUMERIC_CHECK))
            // ->with('Temp',json_encode($Temp,JSON_NUMERIC_CHECK));

    }
    public function chartjs(Request $request,$id,$batch_id)
    {
        $Device = Device::find($id);
        $Data = PowerConsumption::where('device_id',$id)->first();
        $setting_id=isset($Data)?$Data->setting_id:0;
        $data = $this->get_chart_data($request,$id,$setting_id,$batch_id);

        return view('cms.inventory.chartjs')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK))
            ->with("batch_id",$batch_id)
            ->with('Device_name',$Device->name);
            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
    }

    public function chartjs2(Request $request,$id,$batch_id)
    {
        $Device = Device::find($id);
        $Data = PowerConsumption::where('device_id',$id)->first();
        $setting_id=isset($Data)?$Data->setting_id:0;
        $data = $this->get_chart_data2($request,$id,$setting_id,$batch_id);

        return view('cms.inventory.chartjs2')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK))
            ->with("batch_id",$batch_id)
            ->with('Device_name',$Device->name);
            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
    }


public function get_temperature_chart_data(Request $request,$id,$setting_id,$batch_id)
    {
        $Temperature = Temperatures::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $time = array();
        $DeviceAuditTrail = DeviceAuditTrail::where('device_id',$id)->where('setting_id',$setting_id)->where('batch_id',$batch_id)->get();
        $form_date = Carbon::createFromFormat('Y-m-d H:i:s',$request->get('form_date',Carbon::now()->format('Y-m-d')).'00:00:00')->toDateTimeString();
        ($request->get('to_date',1)==1)?$to_date = Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addDays(1):$to_date =Carbon::createFromFormat('Y-m-d H:i:s', $form_date)->addDays(1);
        $Temp=array();
        $Temperatures= array();
        $Temperatures2= array();
        $Temperatures3= array();
        $chart_Temperatures= array();
        $unit='hour';
        foreach ($Temperature as $temperature) {
            $p=json_decode($temperature->temperature,JSON_NUMERIC_CHECK);
            // if(Carbon::createFromFormat('Y-m-d H:i:s', $temperature->time)>=substr($form_date,0,8).'01 00:00:00'&&Carbon::createFromFormat('Y-m-d H:i:s', $temperature->time)<=$to_date){
                if(true){
                $Temperatures[] = ['x'=>$temperature->time,'y'=>$p];
                $Temperatures2[] = ['x'=>$temperature->time,'y'=>json_decode($temperature->room_temperature,JSON_NUMERIC_CHECK)];
                $Temperatures3[] = ['x'=>$temperature->time,'y'=>json_decode($temperature->outdoor_temperature,JSON_NUMERIC_CHECK)];
            }
        }

        // asort($Temperatures);




        if($request->get('to_date')==1){
            $unit="hour";
            $form=$form_date;
  
                foreach ($Temperatures as $key => $temperature) {
                        $time[]=$temperature['x'];

                        $chart_Temperatures[]=$temperature['y'];
                    $last=$temperature['x'];
                
            }
            // $time[]=Carbon::createFromFormat('Y-m-d H:i:s',$temperature['x'])->addhours(2)->toDateTimeString();
            // dd($time);
        }

        if($request->get('to_date')==7){
            $unit="day";
            $form=$form_date;
            foreach ($Temperatures2 as $key => $temperature) {
                        $time[]=$temperature['x'];

                        $chart_Temperatures[]=$temperature['y'];
                    $last=$temperature['x'];
        }  
    }


        if($request->get('to_date')==365){
            $unit="month";
            $form=substr($form_date,0,8).'01 00:00:00';
            $to=Carbon::createFromFormat('Y-m-d H:i:s',substr($to_date,0,8).'01 00:00:00');
            foreach ($Temperatures3 as $key => $temperature) {
                        $time[]=$temperature['x'];

                        $chart_Temperatures[]=$temperature['y'];
                    $last=$temperature['x'];
        }  
        $form_date= substr($form_date,0,8).'01 00:00:00';
        } 

        if($request->get('to_date')==365*5){
            $unit="year";
            $form=$form_date;
            for($i=0;$i<=12;$i++){
                $ad=0;
                $chart_Temperatures[$i]=['x'=>Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)->toDateTimeString(),'y'=>$ad];
                foreach ($Temperatures as $key => $temperature) {
                    if(Carbon::createFromFormat('Y-m-d H:i:s',$temperature["x"])>=Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)&&
                        Carbon::createFromFormat('Y-m-d H:i:s',$temperature["x"])<Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i+1)){
                        $ad+=$temperature['y'];
                        $chart_Temperatures[$i]=['x'=>Carbon::createFromFormat('Y-m-d H:i:s',$form)->addYears($i)->toDateTimeString(),'y'=>$ad];
                    }
                }
            }
        } 

            
         // dd(array_values($chart_Temperatures));
        return array('unit'=>$unit,'form_date'=>$form_date,'to_date'=>$request->get('to_date'),'CustomerIdLists_id'=>$id,'Temperature'=>$chart_Temperatures,'time'=>$time);
            // ->with('unit',$unit)
            // ->with('form_date',$form_date)
            // ->with('to_date',$request->get('to_date'))
            // ->with('CustomerIdLists_id',$id)
            // ->with('Power',json_encode($chart_Powers,JSON_NUMERIC_CHECK))

            // ->with('time',json_encode(array($form_date,$to_date->toDateTimeString()),JSON_NUMERIC_CHECK))
            // ->with('Temp',json_encode($Temp,JSON_NUMERIC_CHECK));

    }

    public function temperature_graph(Request $request,$id,$batch_id)
    {
        $Device = Device::find($id);
        $Data = Temperatures::where('device_id',$id)->first();
        $setting_id=isset($Data)?$Data->setting_id:0;
        $data = $this->get_temperature_chart_data($request,$id,$setting_id,$batch_id);
        // dd(json_encode($data['time'],JSON_NUMERIC_CHECK));
        return view('cms.inventory.temperature_graph')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Temperature',json_encode($data['Temperature'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK))
            ->with("batch_id",$batch_id)
            ->with('Device_name',$Device->name);
            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
    }

    public function webview(Request $request,$id,$setting_id,$batch_id)
    {
        if($request->get('chart',1)==1){
            $Device = Device::find($id);
            $Data = PowerConsumption::where('device_id',$id)->first();
            $setting_id=isset($Data)?$Data->setting_id:0;
            $data = $this->get_chart_data($request,$id,$setting_id,$batch_id);

            return view('webview')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('chart',$request->get('chart',1))
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK));


        }else if($request->get('chart',1)==2){
            $Device = Device::find($id);
            $Data = PowerConsumption::where('device_id',$id)->first();
            $setting_id=isset($Data)?$Data->setting_id:0;
            $data = $this->get_chart_data2($request,$id,$setting_id,$batch_id);
            return view('webview2')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('chart',$request->get('chart',1))
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK));
        }else {

            $Device = Device::find($id);
            $Data = Temperatures::where('device_id',$id)->first();
            $setting_id=isset($Data)?$Data->setting_id:0;
            $data = $this->get_temperature_chart_data($request,$id,$setting_id,$batch_id);
            return view('webview3')
                ->with('unit',$data['unit'])
                ->with('form_date',$data['form_date'])
                ->with('to_date',$data['to_date'])
                ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
                ->with('Temperature',json_encode($data['Temperature'],JSON_NUMERIC_CHECK))
                ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK))
                ->with('chart',$request->get('chart',1))
                ->with("batch_id",$batch_id)
                ->with("setting",$setting_id);
        }

        return view('webview')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('chart',$request->get('chart',1))
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK));
            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
        
    }

     public function webview2(Request $request,$id,$setting_id,$batch_id)
    {
        $Device = Device::find($id);
        $Data = PowerConsumption::where('device_id',$id)->first();
        $setting_id=isset($Data)?$Data->setting_id:0;
        $data = $this->get_chart_data2($request,$id,$setting_id,$batch_id);
            
        return view('webview2')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Power',json_encode($data['Power'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK));

            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
        
    }

    public function webview3(Request $request,$id,$setting_id,$batch_id)
    {
        $Device = Device::find($id);
        $Data = PowerConsumption::where('device_id',$id)->first();
        $setting_id=isset($Data)?$Data->setting_id:0;
        $data = $this->get_temperature_chart_data($request,$id,$setting_id,$batch_id);

            
        return view('webview3')
            ->with('unit',$data['unit'])
            ->with('form_date',$data['form_date'])
            ->with('to_date',$data['to_date'])
            ->with('CustomerIdLists_id',$data['CustomerIdLists_id'])
            ->with('Temperature',json_encode($data['Temperature'],JSON_NUMERIC_CHECK))
            ->with('time',json_encode($data['time'],JSON_NUMERIC_CHECK))
            ->with("batch_id",$batch_id)
            ->with("setting",$setting_id);

            
            // ->with('Temp',json_encode($data['Temp'],JSON_NUMERIC_CHECK));
        
    }

    public function grid(Request $request,$id,$batch_id)
    {
        $Device = Device::find($id);
        $DAT = DeviceAuditTrail::where('device_id',$id)->orderBy('id','desc')->get();
        $setting_id=isset($DAT[0])?$DAT[0]->setting_id:0;
        $mode_name=array();
        $mode_values= array();
        $mode_value= array();
        $mode_time=array();
        $i=0;
        $ttt=array();
        foreach ($DAT as $key => $dat) {
            $mode_time[] = $dat->time;
                if( count(json_decode($dat->mode,true))>count( $mode_name)){//get longest mode
                foreach (array_keys(json_decode($dat->mode,true)) as $key => $name) {
                   $mode_name[$key ] = str_replace("_", " ", $name);
                   // $mode_name[$key ] = str_replace(" Temp", " Temperature",  $mode_name[$key ]);
                }
            }
        // rsort($mode_name);
            foreach ((json_decode($dat->mode,true)) as $key => $mode) {
                if(is_bool($mode)){
                    if($mode){
                        $mode = '1';
                    }else{
                        $mode = '0';
                    }
                }
                if(in_array($key,  config('cms.need_chg') )){
                    $mode=config('cms.'.$key.'.'.$mode);
                };
                $mode_value[str_replace("_", " ", $key)] = $mode;
            }
            if($mode_name!=null&&$mode_value!=null){
            krsort($mode_value);
            rsort($mode_name);
            }

            $mode_values[] =  array_values($mode_value);
        }
        


        return view('cms.inventory.grid')
        ->with('CustomerIdLists_id',$id)
        ->with("batch_id",$batch_id)
        ->with('Device_name',$Device->name)
        ->with("TH",$mode_name)
        ->with('time', $mode_time)
        ->with("TD",$mode_values);
            
    }

    /**
     * get listing of the fields
     * @param  array  $customer_id_lists
     * @param  array  $customer_sub_headerblock_id_lists
     * @param  array  $device_type_lists
     * @param  array  $ieee_lists
     */
    private function getFieldList(&$customer_id_lists = array(),
        &$customer_sub_headerblock_id_lists = array(),
        &$device_type_lists = array(),
        &$ieee_lists = array())
    {
        $CustomerIdLists = $this->CustomerIdList->all();
        foreach ($CustomerIdLists as $key => $CustomerIdList) {
            $customer_id_lists[$CustomerIdList->code] = $CustomerIdList->name;
        }

        $CustomerSubHeaderblockIdLists = $this->CustomerSubHeaderblockIdList->all();
        foreach ($CustomerSubHeaderblockIdLists as $key => $CustomerSubHeaderblockIdList) {
            $customer_sub_headerblock_id_lists[$key]['batch_name'] = $CustomerSubHeaderblockIdList->name;
            $customer_sub_headerblock_id_lists[$key]['batch_code'] = $CustomerSubHeaderblockIdList->code;
            try {
                $customer_sub_headerblock_id_lists[$key]['customer_code'] = $CustomerSubHeaderblockIdList->customer()->first()->code;
            } catch (\Exception $e) {
                $customer_sub_headerblock_id_lists[$key]['customer_code'] = '';
            }

        }

        $DeviceTypeLists = $this->DeviceTypeList->all();
        foreach ($DeviceTypeLists as $key => $DeviceTypeList) {
            $device_type_lists[$DeviceTypeList->code] = $DeviceTypeList->name;
        }

        $IeeeLists = $this->IeeeList->all();
        foreach ($IeeeLists as $key => $IeeeList) {
            $Ieee=$this->CustomerSubHeaderblockIdList->where('ieee_list_id',$IeeeList->id)->get();

            // dd($IeeeList->batch()->first()->customer()->first()->code);
            foreach ( $Ieee as $key => $ieee) {
                try {
                $ieee_lists[$key]['ieee_name'] = $IeeeList->name;
                $ieee_lists[$key]['ieee_code'] = $IeeeList->code;
                $ieee_lists[$key]['customer_code'] = $ieee->customer()->first()->code;
                
            } catch (\Exception $e) {
                $ieee_lists[$key]['customer_code'] = '';
            }
            }
            
            // try {
            //     $ieee_lists[$key]['customer_code'] = $IeeeList->batch()->first()->customer()->first()->code;
            // } catch (\Exception $e) {
            //     $ieee_lists[$key]['customer_code'] = '';
            // }
        }
    }



}
