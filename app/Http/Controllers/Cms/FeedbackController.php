<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\FeedbackRequest;
use App\Http\Controllers\Controller;
use App\Feedback;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Feedback $Feedback)
    {
        if (auth()->user()->app_id!=0) {
            $app_id = auth()->user()->app_id;
            $Feedbacks = $Feedback->where('app_id', $app_id)->get();
        } else {
            $Feedbacks = $Feedback->all();
        }

        return view('cms.feedbacks.index', compact('Feedbacks'));
    }


    public function show($id)
    {
        $Feedbacks = Feedback::where('id',$id)->first();
        return  view('cms.feedbacks.show', compact('Feedbacks'));
    }
}
