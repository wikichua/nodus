<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\CustomerIdListRequest;
use App\Http\Controllers\Controller;
use App\CustomerIdList;
use Gate;
use Auth;

class CustomerIdListController extends Controller
{
    protected $CustomerIdList;

    public function __construct(CustomerIdList $CustomerIdList)
    {
        $this->middleware('auth');
        $this->CustomerIdList = $CustomerIdList;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('admin', 'user')) {
            $CustomerIdLists = $this->CustomerIdList->all();
        } else if(Auth::user()->can('admin', 'user')) {
            $CustomerIdLists = $this->CustomerIdList->all();
        } else {
            $CustomerIdLists = $this->CustomerIdList->where('id', auth()->user()->app_id)->get();
        }

        return view('cms.customer_id_lists.index', compact('CustomerIdLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        return view('cms.customer_id_lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerIdListRequest $request)
    {
        if (Gate::denies('admin', auth()->user())) {
            return redirect()->route('home')->with('warning-message', 'You Are No Permission');
        }

        $CustomerIdList = $this->CustomerIdList->create($request->all());
        $CustomerIdList->app_icon = uploadImage('app_icon', '', 'app_icon');
        $CustomerIdList->website = $request->get('website');
        $CustomerIdList->save();

        return redirect()->route('cms.customer_id_lists.index')->with('success-message', 'Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $CustomerIdList = $this->CustomerIdList->find($id);

        return view('cms.customer_id_lists.edit', compact('CustomerIdList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerIdListRequest $request, $id)
    {
        $CustomerIdList = $this->CustomerIdList->updateOrCreate(['id' => $id], $request->all());
        $CustomerIdList->app_icon = uploadImage('app_icon', $CustomerIdList->app_icon, 'app_icon');
        $CustomerIdList->website = $request->get('website');

        $CustomerIdList->save();

        return redirect()->route('cms.customer_id_lists.index')->with('success-message', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // disble delete because app_id is hard linking to other tables
        // $this->CustomerIdList->destroy($id);

        // session()->flash('success-message', 'Deleted successfully');
    }
}
