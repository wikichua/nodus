<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Firmware;
use App\CustomerIdList;
use Auth;


class FirmwareController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $CustomerIdList = CustomerIdList::all();
        $app_ids=array();
        foreach ($CustomerIdList as$value) {
            $app_ids[$value->id]=$value->name;
        }
        $Firmwares = Firmware::orderBy('version','desc')->paginate(25);
        return view('firmware.index')->with(compact('Firmwares','app_ids'));
    }

    public function create()
    {
        $CustomerIdList = CustomerIdList::all();
        $app_ids=array();
        foreach ($CustomerIdList as$value) {
            $app_ids[$value->id]=$value->name;
        }
        return view('firmware.create')->with(compact('app_ids'));
    }
    public function store(Request $request)
    {
        if($request->get('type') == 'Active')
        {
            Firmware::where('os',$request->get('os'))->where('type',$request->get('type'))->update(array('status' => 'Inactive'));
        }
        $Firmware = Firmware::updateOrCreate(['app_id' => $request->get('app_id')],
                array(
                        'app_id' => $request->get('app_id'),
                        'version' => $request->get('version'),
                        'file' => uploadFile('file','','firmware-file'),
                        'description' => $request->get('description'),
                    )
            );

        return redirect()->route('app.firmware')->with('success','Record created.');
    }

    public function edit($id)
    {
        $Firmware = Firmware::find($id);
        $CustomerIdList = CustomerIdList::all();
        $app_ids=array();
        foreach ($CustomerIdList as$value) {
            $app_ids[$value->id]=$value->name;
        }
        return view('firmware.edit')->with(compact('Firmware','app_ids'));
    }

    public function update(Request $request, $id)
    {

        $Firmware = Firmware::find($id);
        $Firmware->version = $request->get('version',$Firmware->version);
        $Firmware->file = uploadFile('file', $Firmware->file ,'firmware-file') ;
        $Firmware->description = $request->get('description',$Firmware->description);
        $Firmware->app_id =  $request->get('app_id',$Firmware->app_id);
        $Firmware->save();

        return back()->with('success-message','Record Updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
         Firmware::destroy($id);

        return redirect()->route('app.firmware');
        
    }
}