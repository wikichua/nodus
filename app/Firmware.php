<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Firmware extends Model
{
    use SoftDeletes;

    protected $table = 'firmware';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
