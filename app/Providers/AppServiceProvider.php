<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator, DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            $this->app->instance('log', new \Illuminate\Log\Writer(
                    (new \Monolog\Logger(
                        $this->app->environment()
                    ))->pushHandler(new \Monolog\Handler\StreamHandler(env('LOG_PATH',storage_path('logs/laravel.log'))))
                )
            );
        
            Validator::extend('check_password', function($attribute, $value, $parameters) {
                if(auth()->once(['id' => auth()->user()->id, 'password' => $value]))
                {
                    return true;
                }
                    return false;
            });

            Validator::extend('is_mobile', function($attribute, $value, $parameters) {
                $table = $parameters[0];
                $field = $parameters[1];
                //dd($table.' '.$field." ".$value.' '.$attribute);
                $Users = DB::table($table)->where($attribute,$value)->get();
                    if(count(DB::table($table)->where($attribute, $value)->get())<2){
                    if($Users->{$field} !="mobile"){
                       return true; 
                    }
                    dd($Users);
                    return true;
                }
                
                return true;
                   
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
