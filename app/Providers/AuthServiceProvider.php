<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
        $gate->define('su',function($users){
        return $users->role =='su';
        });

        $gate->define('admin',function($users){
        return $users->role =='admin'||$users->role =='su';
        });

        $gate->define('manager',function($users){
        return $users->role =='manager';
        });

        $gate->define('customer',function($users){
        return $users->role =='customer'||$users->role =='sub_customer';
        });

        $gate->define('sud_customer',function($users){
        return $users->role =='sub_customer';
        });

        $gate->define('mobile',function($users){
        return $users->role =='mobile';
        });

        //
    }
}
