<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PnLog extends Model
{
    protected $fillable = [
        'push_token',
        'response',
        'os',
        'message'
    ];
}
