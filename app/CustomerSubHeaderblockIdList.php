<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerSubHeaderblockIdList extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'code', 'customer_id_list_id', 'ieee_list_id', 'customer_id_list_id'];

    public function customer()
    {
        return $this->belongsTo('App\CustomerIdList', 'customer_id_list_id');
    }

    public function ieee()
    {
        return $this->belongsTo('App\IeeeList', 'ieee_list_id');
    }
}
