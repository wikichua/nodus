<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function getGpsAttribute($value)
    {
        return json_decode($value)? json_decode($value):null;
    }

    public function customer_id_list()
    {
        return $this->belongsTo('App\CustomerIdList', 'customer_id', 'code');
    }

    public function device_type_list()
    {
        return $this->belongsTo('App\DeviceTypeList', 'device_type', 'code');
    }

    public function ieee_list()
    {
        return $this->belongsTo('App\IeeeList', 'ieee', 'code');
    }

    public function customer_sub_headerblock_id_list()
    {
        return $this->belongsTo('App\CustomerSubHeaderblockIdList', 'customer_sub_headerblock_id', 'code');
    }

    public function setting()
    {
        return $this->hasOne('App\Setting','id','setting_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function licensee()
    {
        return $this->belongsTo('App\CustomerIdList', 'app_id');
    }
}
