<?php

namespace App\Lib\PushNotification;

use App\MobileDevice;
use App\PnLog;

/**
 * Send Push Notification.
 */
class PushNotification
{
    /**
     * Send push notification
     *
     * @param string  $token
     * @param string $message
     * @param array  $custom
     *
     * @return array
     */
    public function send($token = '', $message = '', $custom = [])
    {
        $MobileDevice = MobileDevice::where('push_token', $token)->first();

        if (!$MobileDevice) {
            return 'Mobile device not found';
        }

        if ($MobileDevice->os === 'AND') {
            $response = $this->sendGcm($token, $message, $custom);
        } else if ($MobileDevice->os === 'IOS') {
            $response = $this->sendApns($token, $message, $custom);
        }

        $PnLog = PnLog::create([
            'push_token' => $token,
            'response'   => $response,
            'os'         => $MobileDevice->os,
            'message'    => $message,
        ]);

        return $response;
    }

    /**
     * Send push notifications using GCM.
     *
     * @param array  $tokens
     * @param string $message
     * @param array  $custom
     *
     * @return array
     */
    private function sendGcms($tokens = [], $message = '', $custom = [])
    {
        $results = array();

        while (count($tokens) > 0) {
            $first1k = array_slice($tokens, 0, 1000);
            $tokens = array_slice($tokens, 1000, count($tokens) - 1000);

            $headers = [
                'Content-Type:application/json',
                'Authorization:key='.config('app.pn.android'),
            ];

            $datas = [
                'registration_ids' => $first1k, // registration_ids have max 1000 limit
                'data' => [
                    'message' => $message,
                    'custom' => $custom,
                ],
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datas));
            $results[] = curl_exec($ch);
            curl_close($ch);
        }

        return $results;
    }

    /**
     * Send push notification using GCM.
     *
     * @param string  $token
     * @param string $message
     * @param array  $custom
     *
     * @return array
     */
    private function sendGcm($token = '', $message = '', $custom = [])
    {
        $headers = [
            'Content-Type:application/json',
            'Authorization:key='.config('app.pn.android'),
        ];

        $datas = [
            'to' => $token, // single token
            'data' => [
                'message' => $message,
                'custom' => $custom,
            ],
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datas));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * Send push Notification using APNS.
     * @param  string $token
     * @param  string $message
     * @param  array $custom
     * @return string
     */
    private function sendApns($token = '', $message = '', $custom = [])
    {
        $apnsPort = 2195;
        $apnsPass = 'qazwsx12';

        // development server
        $apnsHost = 'gateway.sandbox.push.apple.com';
        $apnsCert = base_path() . '/files/nodus_apns_dev_cert.pem';

        // production server
        // $apnsHost = 'gateway.push.apple.com';
        // $apnsCert = base_path() . '/files/nodus_apns_prod_cert.pem';

        $payload['aps'] = [
            'alert' => $message,
            'sound' => 'default',
            'custom' => $custom
        ];

        $output = json_encode($payload);
        $token = pack('H*', str_replace(' ', '', $token));
        $apnsMessage = chr(0).chr(0).chr(32).$token.chr(0).chr(strlen($output)).$output;
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
        stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

        $apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
        $result = fwrite($apns, $apnsMessage);
        fclose($apns);

        if ($result) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }
}
