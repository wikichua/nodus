<?php

namespace App\Lib\HouseholdIdGenerator;

class HouseholdIdGenerator
{
    public function generate($email)
    {
        $email = str_replace(array(' ','.'),'',microtime()).$email;
        $count = strlen($email);
        $index = 0;
        $sum1 = 0xff;
        $sum2 = 0xff;

        while ($count !== 0) {
            $tlen = ($count >= 20) ? 20 : $count;
            $count -= $tlen;

            for ($i = 0; $i < $tlen; $i++) {
                $character = substr($email, $index, 1);
                $ascii = ord($character); // convert to ascii number
                $sum2 += $sum1 += $ascii;
                $index++;
            }

            $sum1 = ($sum1 & 0xff) + ($sum1 >> 8);
            $sum2 = ($sum2 & 0xff) + ($sum2 >> 8);
        }

        $sum1 = ($sum1 & 0xff) + ($sum1 >> 8);
        $sum2 = ($sum2 & 0xff) + ($sum2 >> 8);

        $result = ($sum2 << 8) | $sum1;

        return $result;
    }
}
