<?php

namespace App\Lib\GoogleApiManager;

use Google_Client;
use Google_Service_Analytics;
use Google_Auth_AssertionCredentials;

class GoogleApiManager
{
    protected $client;
    protected $analytics;

    public function __construct()
    {
        $service_account_email = config('app.google_api.service_account_email');
        $key_file_location = config('app.google_api.key_file_location');

        $this->client = new Google_Client();
        $this->client->setApplicationName('Nodus_CMS_Analytics');
        $this->analytics = new Google_Service_Analytics($this->client);

        $key = file_get_contents($key_file_location);

        $cred = new Google_Auth_AssertionCredentials(
            $service_account_email,
            array(Google_Service_Analytics::ANALYTICS_READONLY),
            $key
        );

        $this->client->setAssertionCredentials($cred);

        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getAnalytics()
    {
        return $this->analytics;
    }

    public function getAccessToken()
    {
        $AccessToken = $this->client->getAccessToken();

        return json_decode($AccessToken, true)['access_token'];
    }
}
