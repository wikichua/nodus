<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['subject', 'message', 'user_id', 'app_id'];

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function licensee()
    {
        return $this->belongsTo('App\CustomerIdList', 'app_id');
    }
}
